﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tripallmed.Entity;

namespace tripallmed.Business
{
    public class HealthHomeService : DbInstance
    {
        public List<BranchHospital> GetAllBranchHospitals()
        {
            return db.BranchHospitals.ToList();
        }

        public Polyclinic GetPolyclinicId(int id)
        {
            return db.Polyclinics.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
        }

        public List<HealthAdvertisement> GetCategoryAdvertisements(int kategoriId)
        {
            var healthAdvertisements = db.HealthAdvertisements.Where(x => x.MedicalUnitId == kategoriId && x.IsDeleted == false && x.Active == true && x.AdvertisementStatusId == 1).ToList();
            if(healthAdvertisements != null)
            {
                foreach(var healthAdvertisement in healthAdvertisements)
                {
                    var doctors = new List<Doctor>();
                    doctors = healthAdvertisement.HealthcareCompany.Doctors.ToList();
                    foreach(var doctor in doctors)
                    {
                        if(doctor.IsDeleted)
                        {
                            healthAdvertisement.HealthcareCompany.Doctors.Remove(doctor);
                        }
                    }
                }

                foreach(var healthAdvertisement in healthAdvertisements)
                {
                    var Advers = new List<HealthAdvertisement>();
                    Advers = healthAdvertisement.HealthcareCompany.HealthAdvertisements.ToList();
                    foreach(var adver in Advers)
                    {
                        if(adver.IsDeleted)
                        {
                            healthAdvertisement.HealthcareCompany.HealthAdvertisements.Remove(adver);
                        }
                    }
                }

            }

            return healthAdvertisements;
        }

        public List<HealthAdvertisement> GetCategoryServiceAdvertisements(int? kategoriId)
        {
            var healthAdvertisements = db.HealthAdvertisements.Where(x => x.OperationId == kategoriId && x.IsDeleted == false && x.Active == true && x.AdvertisementStatusId == 1).ToList();
            if(healthAdvertisements != null)
            {
                foreach(var healthAdvertisement in healthAdvertisements)
                {
                    var doctors = new List<Doctor>();
                    doctors = healthAdvertisement.HealthcareCompany.Doctors.ToList();
                    foreach(var doctor in doctors)
                    {
                        if(doctor.IsDeleted)
                        {
                            healthAdvertisement.HealthcareCompany.Doctors.Remove(doctor);
                        }
                    }
                }

                foreach(var healthAdvertisement in healthAdvertisements)
                {
                    var Advers = new List<HealthAdvertisement>();
                    Advers = healthAdvertisement.HealthcareCompany.HealthAdvertisements.ToList();
                    foreach(var adver in Advers)
                    {
                        if(adver.IsDeleted)
                        {
                            healthAdvertisement.HealthcareCompany.HealthAdvertisements.Remove(adver);
                        }
                    }
                }
            }

            return healthAdvertisements;
        }


        public HealthAdvertisement GetHealthAdvertisement(int id)
        {
            return db.HealthAdvertisements.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
        }

        public List<City> GetTurkeyCity()
        {
            return db.Cities.Where(x => x.CountryId == 213 && x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<HealthStatus> GetAllHealthStatuses()
        {
            return db.HealthStatuses.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public object GetAllCityTowns(int id)
        {
            return db.Cities.FirstOrDefault(x => x.Id == id).Towns.Select(x => new
            {
                Id = x.Id,
                TownName = x.TownName
            }).ToList();
        }

    }
}