﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using tripallmed.Entity;

namespace tripallmed.Business
{
    public class RegisterService : DbInstance
    {

        /// <summary>
        /// Kullanıcılar arasında bu email varmı kontrolünü yapan fonksiyon, kullanıcı varsa geriye true yoksa false döner
        /// </summary>
        /// <param name="email">Kontrol edilecek email adresi</param>
        /// <returns></returns>
        public bool EmailCheck(string email)
        {

            if(db.Users.Any(x => x.Email == email) || db.HealthUsers.Any(x => x.Email == email) || db.TourismUsers.Any(x => x.Email == email) || db.AdminUsers.Any(x => x.Email == email))
            {
                return true;
            }

            return false;
        }


        /// <summary>
        /// Sağlık sektöründe yeni kullanıcı kaydını veri tabanına ekleyen fonksiyon
        /// </summary>
        /// <param name="model">Yeni sağlık sektörü kullanıcısı</param>
        /// <returns></returns>
        public bool HealthRegisterNewMember(HealthUser model)
        {
            if(model == null) return false;

            db.HealthUsers.Add(model);
            return db.SaveChanges() > 0;
        }

        public bool TourismRegisterNewMember(TourismUser model)
        {
            if(model == null) return false;

            db.TourismUsers.Add(model);
            return db.SaveChanges() > 0;
        }


        /// <summary>
        /// Kullanıcının email ve şifre kontrolunu yaparak varsa hangi sektörde olduğunu ve email adresini geri dönderen fonksiyon, böyle bir kullanıcı yoksa geriye null döner
        /// </summary>
        /// <param name="email">Kullanıcı email</param>
        /// <param name="password">Kullanıcı parola</param>
        /// <returns></returns>
        public KeyValuePair<string, string> UserCheckAndGetEmail(string email, string password)
        {
            if(db.Users.Any(x => x.Email == email && x.Password == password))
            {
                return new KeyValuePair<string, string>(db.Users.FirstOrDefault(x => x.Email == email && x.Password == password).Role, db.Users.FirstOrDefault(x => x.Email == email && x.Password == password).Email);
            }

            if(db.HealthUsers.Any(x => x.Email == email && x.Password == password))
            {
                return new KeyValuePair<string, string>(db.HealthUsers.FirstOrDefault(x => x.Email == email && x.Password == password).Role, db.HealthUsers.FirstOrDefault(x => x.Email == email && x.Password == password).Email);
            }

            if(db.TourismUsers.Any(x => x.Email == email && x.Password == password))
            {
                return new KeyValuePair<string, string>(db.TourismUsers.FirstOrDefault(x => x.Email == email && x.Password == password).Role, db.TourismUsers.FirstOrDefault(x => x.Email == email && x.Password == password).Email);
            }

            if(db.AdminUsers.Any(x => x.Email == email && x.Password == password))
            {
                return new KeyValuePair<string, string>(db.AdminUsers.FirstOrDefault(x => x.Email == email && x.Password == password).Role, db.AdminUsers.FirstOrDefault(x => x.Email == email && x.Password == password).Email);
            }

            return new KeyValuePair<string, string>(null, null);
        }


        /// <summary>
        /// Kullanıcı rolünü geriye dönderen fonksiyon, böyle bir kullanıcı yoksa geriye null döner
        /// </summary>
        /// <param name="email">Kullanıcı email</param>
        /// <returns></returns>
        public string UserCheckAndGetRole(string email)
        {
            if(DbInstance.db.Users.Any(x => x.Email == email))
                return DbInstance.db.Users.FirstOrDefault(x => x.Email == email).Role;

            if(DbInstance.db.HealthUsers.Any(x => x.Email == email))
                return DbInstance.db.HealthUsers.FirstOrDefault(x => x.Email == email).Role;

            if(DbInstance.db.TourismUsers.Any(x => x.Email == email))
                return DbInstance.db.TourismUsers.FirstOrDefault(x => x.Email == email).Role;

            if(DbInstance.db.AdminUsers.Any(x => x.Email == email))
                return DbInstance.db.AdminUsers.FirstOrDefault(x => x.Email == email).Role;

            return null;
        }




        /// <summary>
        /// Kullanıcının ismini ve soyismini ve rolünü key olarak geriye döndüren fonsiyon, kullanıcı yoksa geriye null döner.
        /// </summary>
        /// <param name="email">Kullanıcı email</param>
        /// <returns></returns>
        public KeyValuePair<string, string> GetUserName(string email)
        {
            if(DbInstance.db.Users.Any(x => x.Email == email))
                return new KeyValuePair<string, string>(DbInstance.db.Users.FirstOrDefault(x => x.Email == email).Role, DbInstance.db.Users.FirstOrDefault(x => x.Email == email).Name + " " + DbInstance.db.Users.FirstOrDefault(x => x.Email == email).Surname);

            if(DbInstance.db.HealthUsers.Any(x => x.Email == email))
                return new KeyValuePair<string, string>(DbInstance.db.HealthUsers.FirstOrDefault(x => x.Email == email).Role, DbInstance.db.HealthUsers.FirstOrDefault(x => x.Email == email).HealthcareCompany.Title != null ? db.HealthUsers.FirstOrDefault(x => x.Email == email).HealthcareCompany.Title : DbInstance.db.HealthUsers.FirstOrDefault(x => x.Email == email).Name + " " + db.HealthUsers.FirstOrDefault(x => x.Email == email).Surname);

            if(DbInstance.db.TourismUsers.Any(x => x.Email == email))
                return new KeyValuePair<string, string>(DbInstance.db.TourismUsers.FirstOrDefault(x => x.Email == email).Role, DbInstance.db.TourismUsers.FirstOrDefault(x => x.Email == email).TourismCompany.Title != null ? db.TourismUsers.FirstOrDefault(x => x.Email == email).TourismCompany.Title : DbInstance.db.TourismUsers.FirstOrDefault(x => x.Email == email).Name + " " + db.TourismUsers.FirstOrDefault(x => x.Email == email).Surname);


            if(DbInstance.db.AdminUsers.Any(x => x.Email == email))
                return new KeyValuePair<string, string>(DbInstance.db.AdminUsers.FirstOrDefault(x => x.Email == email).Role, DbInstance.db.AdminUsers.FirstOrDefault(x => x.Email == email).Name + " " + DbInstance.db.AdminUsers.FirstOrDefault(x => x.Email == email).Surname);

            return new KeyValuePair<string, string>(null, null);
        }
    }
}
