﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tripallmed.Data_Access;
using tripallmed.Entity;
using tripallmed.Models.DTO;

namespace tripallmed.Business
{
    public class TourismHomeService : DbInstance
    {
        public List<TourismAdvertisement> GetAllTourismAdvertisements()
        {
            return db.TourismAdvertisements.Where(x => x.IsDeleted == false && x.Active == true && x.AdvertisementStatusId == 1).ToList();
        }

        public List<TourismAmenities> GetAllTourismAmenities()
        {
            return db.TourismAmenities.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<FilterDTO> FilterScopes()
        {
            return db.TourismScopes.Where(x => x.IsDeleted == false && x.Active == true && x.TourismCompanies.Count() > 0).Select(x => new FilterDTO
            {
                Name = x.Scopes,
                Count = x.TourismCompanies.Count()
            }).ToList();
        }

        public List<TourismmCompany> GetAllTourismmCompanies()
        {
            return db.TourismCompanies.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<TourismRule> GetAllRules()
        {
            return db.TourismRules.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<TourismCompanyDTO> SeachFilter(SeacrhDTO model)
        {
            //var TourismCompanyIsActive = db.TourismCompanies.Where(x => x.IsDeleted == false && x.Active == true);
            var TourismCompanyIsActive = new TripallmedContext().TourismCompanies.Where(x => x.IsDeleted == false && x.Active == true);

            var searchText = model.Text.Split(' ');
            var searchTextList = new List<string>();
            foreach(var text in searchText)
            {
                if(text != "") searchTextList.Add(text);
            }

            var checkInDate = Convert.ToDateTime(model.CheckIn);
            var checkOutDate = Convert.ToDateTime(model.CheckOut);
            TimeSpan gunfarki = checkOutDate - checkInDate;
            var gun = gunfarki.Days;
            model.Day = gun;

            List<TourismCompanyDTO> listCompany = new List<TourismCompanyDTO>();
            foreach(var item in searchTextList)
            {
                var companies = TourismCompanyIsActive.Where(x => x.Title.Contains(item) || x.City.CityName.Contains(item) || x.Town.TownName.Contains(item)).ToList();
                foreach(var company in companies)
                {
                    if(!listCompany.Any(x => x.Id == company.Id))
                    {
                        var newCompany = new TourismCompanyDTO()
                        {
                            Id = company.Id,
                            TourismUser = company.TourismUser,
                            CompanyTypeId = company.CompanyTypeId,
                            CompanyType = company.CompanyType,
                            Title = company.Title,
                            TaxAdministration = company.TaxAdministration,
                            TaxNo = company.TaxNo,
                            FacilityName = company.FacilityName,
                            FacilityTypeId = company.FacilityTypeId,
                            FacilityType = company.FacilityType,
                            TourismStatusId = company.TourismStatusId,
                            TourismStatus = company.TourismStatus,
                            EstablishmentDate = company.EstablishmentDate,
                            RenovationDate = company.RenovationDate,
                            AreaM2 = company.AreaM2,
                            RoomCapacity = company.RoomCapacity,
                            BedCapacity = company.BedCapacity,
                            TourismThemaId = company.TourismThemaId,
                            TourismThema = company.TourismThema,
                            TourismScopes = company.TourismScopes.Where(t => t.IsDeleted == false && t.Active == true).ToList(),
                            TourismRules = company.TourismRules.Where(t => t.IsDeleted == false && t.Active == true).ToList(),
                            CheckInTime = company.CheckInTime,
                            CheckOutTime = company.CheckOutTime,
                            ValidLanguages = company.ValidLanguages.Where(t => t.IsDeleted == false && t.Active == true).ToList(),
                            DescriptionTurkish = company.DescriptionTurkish,
                            DescriptionEnglish = company.DescriptionEnglish,
                            DescriptionArabic = company.DescriptionArabic,
                            CountryId = company.CountryId,
                            Country = company.Country,
                            CityId = company.CityId,
                            City = company.City,
                            TownId = company.TownId,
                            Town = company.Town,
                            Address = company.Address,
                            ZipCode = company.ZipCode,
                            AirportDistance = company.AirportDistance,
                            CityCenterDistance = company.CityCenterDistance,
                            Latitude = company.Latitude,
                            Longitude = company.Longitude,
                            Logo = company.Logo,
                            CoverImage = company.CoverImage,
                            PictureGalleries = company.PictureGalleries.Where(t => t.IsDeleted == false && t.Active == true).ToList(),
                            TourismAdvertisements = company.TourismAdvertisements.Where(t => t.IsDeleted == false && t.Active == true && t.AdvertisementStatusId == 1).ToList(),
                        };

                        listCompany.Add(newCompany);
                    }
                }
            }
            listCompany.Distinct();

            foreach(var company in listCompany)
            {
                company.TourismAdvertisements = company.TourismAdvertisements.Where(x => x.GuestCount >= (model.AdultCount + model.ChildCount)).ToList();
            }

            foreach(var company in listCompany)
            {
                var listAdver = new List<TourismAdvertisement>();
                foreach(var item in company.TourismAdvertisements)
                {
                    var adver = new TourismAdvertisement();
                    adver = item;
                    listAdver.Add(adver);
                }
                foreach(var tourismAdvertisement in listAdver)
                {
                    if(tourismAdvertisement.TourismPriceTypeId != 1)
                    {
                        tourismAdvertisement.NightPrice *= gun;
                        continue;
                    }

                    int childCount = 0, babyCount = 0;
                    foreach(var property in model.GetType().GetProperties())
                    {
                        if(property.Name.Contains("ChildAge") & property.GetValue(model, null) != null)
                        {
                            if((short)property.GetValue(model, null) > 6 && (short)property.GetValue(model, null) < 13)
                            {
                                childCount++;
                            }
                            else if((short)property.GetValue(model, null) < 7)
                            {
                                babyCount++;
                            }
                            else if((short)property.GetValue(model, null) > 12)
                            {
                                model.AdultCount++;
                            }
                        }
                    }

                    if(!tourismAdvertisement.BabyDiscount)
                    {
                        model.AdultCount += (short)babyCount;
                        babyCount = 0;
                    }
                    if(!tourismAdvertisement.ChildDiscount)
                    {
                        model.AdultCount += (short)childCount;
                        childCount = 0;
                    }

                    if(tourismAdvertisement.TourismAdvertisedPersonPrices.Any(x => x.Key == (model.AdultCount + childCount) && x.Price == null) || tourismAdvertisement.TourismAdvertisedPersonPrices.Any(x => x.Key == (model.AdultCount) && x.Price == null))
                    {
                        company.TourismAdvertisements.Remove(tourismAdvertisement);
                        continue;
                    }
                    else
                    {

                        var adultPrice = tourismAdvertisement.TourismAdvertisedPersonPrices.FirstOrDefault(x => x.Key == model.AdultCount).Price;
                        var adultChildPrice = tourismAdvertisement.TourismAdvertisedPersonPrices.FirstOrDefault(x => x.Key == (model.AdultCount + childCount)).Price;

                        decimal? childPrice;
                        if(childCount != 0)
                        {
                            childPrice = ((adultChildPrice - adultPrice) / childCount) / 2;
                        }
                        else
                        {
                            childPrice = 0;
                        }

                        foreach(var prices in tourismAdvertisement.TourismAdvertisedPersonPrices)
                        {
                            if(prices.Key == (model.AdultCount + childCount + babyCount))
                            {
                                prices.Price = Math.Round(adultPrice + (childCount * childPrice) ?? 0, 2) * gun;
                            }
                            else
                            {
                                prices.Price *= gun;
                            }
                        }
                    }
                }
            }

            //foreach(var company in listCompany)
            //{
            //    company.TourismAdvertisements = company.TourismAdvertisements.Where(x => x.TourismAdvertisedAccommodation.TourismAdvertisedRoomProperties.Count() >= model.RoomCount).ToList();
            //}

            listCompany = listCompany.Where(x => x.TourismAdvertisements.Count() > 0).ToList();
            return listCompany;
        }
    }
}