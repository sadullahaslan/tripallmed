﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using tripallmed.Data_Access;

namespace tripallmed.Business
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            if(httpContext.User.Identity.IsAuthenticated)
            {
                RegisterService registerService = new RegisterService();

                string userRole = registerService.UserCheckAndGetRole(httpContext.User.Identity.Name);

                if(userRole != null)
                {
                    var roles = Roles.Split(',');
                    if(roles.Contains(userRole))
                    {
                        return true;
                    }
                }

            }
            return base.AuthorizeCore(httpContext);
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if(!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                filterContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Home/Index.cshtml",
                };
            }
        }
    }
}