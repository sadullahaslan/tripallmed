﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tripallmed.Models.DTO;

namespace tripallmed.Business
{
    public class CustomValidation
    {
        public static ValidationMessageDTO ValidationMessageDTO(ModelStateDictionary modelState)
        {
            ValidationMessageDTO validationMessageDTO = new ValidationMessageDTO();
            validationMessageDTO.Message = string.Join(" ", modelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));

            foreach(var modelStateKey in modelState.Keys)
            {
                var modelStateVal = modelState[modelStateKey];
                foreach(var error in modelStateVal.Errors)
                {
                    validationMessageDTO.Keys.Add(modelStateKey);
                }
            }

            return validationMessageDTO;
        }

    }
}