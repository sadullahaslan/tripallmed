﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace tripallmed.Business
{
    /// <summary>
    /// Summary description for ImageUploadHandler
    /// </summary>
    public class ImageUploadHandler : IHttpHandler
    {
        private static Random random = new Random();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                string str_image = "";
                string str_image_100 = "";

                foreach(string s in context.Request.Files)
                {
                    HttpPostedFile file = context.Request.Files[s];
                    string fileName = file.FileName;
                    string fileExtension = file.ContentType;

                    if(!string.IsNullOrEmpty(fileName))
                    {
                        fileExtension = Path.GetExtension(fileName);
                        string randomString = RandomString();
                        str_image = randomString + fileExtension;
                        str_image_100 = randomString + "_100" + fileExtension;
                        string pathToSave = HttpContext.Current.Server.MapPath("~/Data/") + str_image;

                        Bitmap bmp = (Bitmap)Bitmap.FromStream(context.Request.Files[s].InputStream);
                        if(bmp.Width > bmp.Height)//width büyükse
                        {
                            if(bmp.Width > 720)
                            {
                                Bitmap bmp2 = ImageUploadHandler.ScaleImage(bmp, 720, 0);
                                System.Drawing.Image i = bmp2;
                                Bitmap cizimDosyasi = new Bitmap(i, i.Width, i.Height);
                                cizimDosyasi.Save(pathToSave);
                            }
                            else if(bmp.Width < 720)
                            {
                                Bitmap bmp2 = ImageUploadHandler.ScaleImage(bmp, bmp.Width, bmp.Height);
                                System.Drawing.Image i = bmp2;//System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);
                                Bitmap cizimDosyasi = new Bitmap(i, i.Width, i.Height);
                                cizimDosyasi.Save(pathToSave);
                            }
                            else if(bmp.Width == 720)
                            {
                                Bitmap bmp2 = ImageUploadHandler.ScaleImage(bmp, 720, 405);
                                System.Drawing.Image i = bmp2;//System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);
                                Bitmap cizimDosyasi = new Bitmap(i, i.Width, i.Height);
                                cizimDosyasi.Save(pathToSave);
                            }
                        }
                        else if(bmp.Width < bmp.Height)// Height büyükse
                        {
                            if(bmp.Height > 540)
                            {
                                Bitmap bmp2 = ImageUploadHandler.ScaleImage(bmp, 0, 540);
                                System.Drawing.Image i = bmp2;//System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);
                                Bitmap cizimDosyasi = new Bitmap(i, i.Width, i.Height);
                                cizimDosyasi.Save(pathToSave);
                            }
                            else if(bmp.Height < 540)
                            {
                                Bitmap bmp2 = ImageUploadHandler.ScaleImage(bmp, bmp.Width, bmp.Height);
                                System.Drawing.Image i = bmp2;//System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);
                                Bitmap cizimDosyasi = new Bitmap(i, i.Width, i.Height);
                                cizimDosyasi.Save(pathToSave);
                            }
                        }
                        else // Height büyükse
                        {
                            Bitmap bmp2 = ImageUploadHandler.ScaleImage(bmp, 720, 405);
                            System.Drawing.Image i = bmp2;//System.Drawing.Image.FromStream(FileUpload1.PostedFile.InputStream);
                            Bitmap cizimDosyasi = new Bitmap(i, i.Width, i.Height);
                            cizimDosyasi.Save(pathToSave);
                        }


                        //file.SaveAs(pathToSave);
                        /*
                        Image image = Image.FromStream(file.InputStream, true, true);
                        string pathToSave_100 = HttpContext.Current.Server.MapPath("~/Data/") + str_image_100;
                        ResizeAndSave(image, 100, pathToSave_100);*/

                    }
                }
                //  database record update logic here  ()

                context.Response.Write(str_image);
            }
            catch
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ResizeAndSave(Image image, int width, string path)
        {
            int newwidthimg = width;
            float AspectRatio = (float)image.Size.Width / (float)image.Size.Height;
            int newHeight = Convert.ToInt32(newwidthimg / AspectRatio);
            Bitmap thumbnailBitmap = new Bitmap(newwidthimg, newHeight);
            Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            var imageRectangle = new Rectangle(0, 0, newwidthimg, newHeight);
            thumbnailGraph.DrawImage(image, imageRectangle);
            thumbnailBitmap.Save(path, ImageFormat.Jpeg);
            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
            image.Dispose();
        }

        public static string RandomString()
        {
            int length = 32;
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static Bitmap ScaleImage(Bitmap orjImg, int width, int height)
        {
            if(height == 0) height = Convert.ToInt32(width * (double)orjImg.Height / (double)orjImg.Width);
            if(width == 0) width = Convert.ToInt32(height * (double)orjImg.Width / (double)orjImg.Height);

            Bitmap imgDest = new Bitmap(width, height);
            imgDest.SetResolution(72, 72);
            Graphics grDest = Graphics.FromImage(imgDest);
            grDest.SmoothingMode = SmoothingMode.AntiAlias;
            grDest.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grDest.PixelOffsetMode = PixelOffsetMode.HighQuality;
            grDest.DrawImage(orjImg, 0f, 0f, (float)width, (float)height);
            grDest.Dispose();
            return imgDest;
        }

    }
}