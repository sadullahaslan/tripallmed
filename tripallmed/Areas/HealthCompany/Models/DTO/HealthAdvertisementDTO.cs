﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Areas.HealthCompany.Models.DTO
{
    public class HealthAdvertisementDTO
    {
        public int Id { get; set; }
        [MaxLength(100, ErrorMessage = "İlan başlığı 100 karakteri geçemez.")]
        public string AdvertTitle { get; set; }
        [MaxLength(100)]
        public string AdvertTitle_Eng { get; set; }
        [MaxLength(100)]
        public string AdvertTitle_Oth { get; set; }
        public int MedicalCenterId { get; set; }
        public int MedicalUnitId { get; set; }
        public int OperationId { get; set; }
        [MaxLength(300)]
        public string What { get; set; }
        [MaxLength(300)]
        public string What_Eng { get; set; }
        [MaxLength(300)]
        public string What_Oth { get; set; }
        [MaxLength(300)]
        public string Preparation { get; set; }
        [MaxLength(300)]
        public string Preparation_Eng { get; set; }
        [MaxLength(300)]
        public string Preparation_Oth { get; set; }
        [MaxLength(300)]
        public string Time { get; set; }
        [MaxLength(300)]
        public string Time_Eng { get; set; }
        [MaxLength(300)]
        public string Time_Oth { get; set; }
        [MaxLength(300)]
        public string Post { get; set; }
        [MaxLength(300)]
        public string Post_Eng { get; set; }
        [MaxLength(300)]
        public string Post_Oth { get; set; }
        public int[] IncludedPriceIds { get; set; }
        public short? OperationOption { get; set; }
        public decimal? ProcessingFee { get; set; }
        public int CurrencyUnitId { get; set; }
        public int CampaignDiscount { get; set; }
        [MaxLength(50)]
        public string CoverImage { get; set; }
        public DateTime? CampaignStart { get; set; }
        public DateTime? CampaignEnd { get; set; }
        public short? Prepayment { get; set; }
        [MaxLength(20)]
        public string CancellationPolicy { get; set; }
    }
}