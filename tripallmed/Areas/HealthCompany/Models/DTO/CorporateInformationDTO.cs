﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Areas.HealthCompany.Models.DTO
{
    public class CorporateInformationDTO
    {
        [Required(ErrorMessage = "Şirket ünvan boş olamaz.")]
        [MaxLength(150, ErrorMessage = "Şirket ünvanı 150 karakteri geçemez.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Şirket türü boş olamaz.")]
        public int CompanyTypeId { get; set; }

        [Required(ErrorMessage = "Vergi dairesi boş olamaz.")]
        [MaxLength(150, ErrorMessage = "Vergi dairesi 150 karakteri geçemez.")]
        public string TaxAdministration { get; set; }

        [Required(ErrorMessage = "Vergi no boş olamaz.")]
        [MaxLength(100, ErrorMessage = "Vergi no 100 karakteri geçemez.")]
        public string TaxNo { get; set; }

        [Required(ErrorMessage = "Merkez adı boş olamaz.")]
        [MaxLength(200, ErrorMessage = "Merkez adı 200 karakteri geçemez.")]
        public string CentralName { get; set; }

        [Required(ErrorMessage = "Merkez tipi boş olamaz.")]
        public int HealthCenterTypeId { get; set; }

        [Required(ErrorMessage = "Statü boş olamaz.")]
        public int HealthStatusId { get; set; }

        [Required(ErrorMessage = "Kuruluş tarihini girin.")]
        public DateTime? EstablishmentDate { get; set; }

        [Required(ErrorMessage = "Renovasyon tarihini girin.")]
        public DateTime? RenovationDate { get; set; }

        [Required(ErrorMessage = "Alan boş olamaz.")]
        [Range(1, 30000, ErrorMessage = "Alan M2 1 ile 30,000 arasında olmalıdır.")]
        public short AreaM2 { get; set; }

        [Required(ErrorMessage = "Yatak kapasitesi boş olamaz.")]
        [Range(1, 30000, ErrorMessage = "Yatak kapasitesi 1 ile 30,000 arasında olmalıdır.")]
        public short BedCapacity { get; set; }

        [Required(ErrorMessage = "Parsonel sayısı girin.")]
        [Range(1, 30000, ErrorMessage = "Personel sayısı 1 ile 30,000 arasında olmalıdır.")]
        public short PersonCount { get; set; }

        [Required(ErrorMessage = "Geçerli dil seçin.")]
        public int[] ValidLanguagesIds { get; set; }

        [Required(ErrorMessage = "Türkçe açıklama boş olamaz.")]
        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionTurkish { get; set; }

        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionEnglish { get; set; }

        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionArabic { get; set; }
    }
}