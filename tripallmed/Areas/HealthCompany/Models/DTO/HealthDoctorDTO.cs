﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Areas.HealthCompany.Models.DTO
{
    public class HealthDoctorDTO
    {
        [Required(ErrorMessage = "Doktor adı boş olamaz.")]
        [MaxLength(50, ErrorMessage = "Doktor adı 50 karakteri geçemez.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Doktor soyadı boş olamaz.")]
        [MaxLength(50, ErrorMessage = "Doktor soyadı 50 karakteri geçemez.")]
        public string Surname { get; set; }

        public int DoctorTitleid { get; set; }
        public int PolyclinicId { get; set; }

    }
}