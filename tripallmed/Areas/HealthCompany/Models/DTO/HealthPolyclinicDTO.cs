﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Areas.HealthCompany.Models.DTO
{
    public class HealthPolyclinicDTO
    {
        [Required(ErrorMessage = "Poliklinik seçiniz.")]
        public int[] PolyclinicIds { get; set; }

        [Required(ErrorMessage = "Anlaşmalı kurum seçiniz.")]
        public int[] ContractedInstitutionIds { get; set; }

    }
}