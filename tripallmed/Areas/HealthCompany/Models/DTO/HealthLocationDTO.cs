﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Areas.HealthCompany.Models.DTO
{
    public class HealthLocationDTO
    {
        [Required(ErrorMessage = "Ülke seçiniz")]
        public int CountryId { get; set; }

        [Required(ErrorMessage = "Şehir seçiniz")]
        public int CityId { get; set; }

        [Required(ErrorMessage = "İlçe seçiniz.")]
        public int TownId { get; set; }

        [Required(ErrorMessage = "Adres boş olamaz.")]
        [MaxLength(200, ErrorMessage = "Adres 200 karakteri geçemez.")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Posta kodu boş olamaz.")]
        [MaxLength(50, ErrorMessage = "Posta kodu 50 karakteri geçemez.")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Havaalanı mesafesi girin.")]
        [Range(1, 30000, ErrorMessage = "Havaalanı mesafesi 1 ile 30,000 arasında olmalıdır.")]
        public short AirportDistance { get; set; }

        [Required(ErrorMessage = "Şehir merkezi mesafesi girin.")]
        [Range(1, 30000, ErrorMessage = "Şehir merkezi uzaklığı 1 ile 30,000 arasında olmalıdır.")]
        public short CityCenterDistance { get; set; }

        [Required(ErrorMessage ="Konum seçin. ")]
        public string Latitude { get; set; }

        public string Longitude { get; set; }


    }
}