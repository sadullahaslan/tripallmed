﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tripallmed.Entity;

namespace tripallmed.Areas.HealthCompany.Models.DTO
{
    public class MyProfileDTO
    {
        public string Title { get; set; }

        public string TaxAdministration { get; set; }

        public string TaxNo { get; set; }

        public string CentralName { get; set; }

        public DateTime? EstablishmentDate { get; set; }

        public DateTime? RenovationDate { get; set; }

        public short? AreaM2 { get; set; }

        public short? BedCapacity { get; set; }

        public short? PersonCount { get; set; }

        public string DescriptionTurkish { get; set; }

        public string DescriptionEnglish { get; set; }

        public string DescriptionArabic { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public short? AirportDistance { get; set; }

        public short? CityCenterDistance { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Logo { get; set; }

        public string CoverImage { get; set; }

        public List<PictureGallery> PictureGalleries { get; set; }
    }
}