﻿using System.Web.Mvc;

namespace tripallmed.Areas.HealthCompany
{
    public class HealthCompanyAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "HealthCompany";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "HealthCompany_default",
                "HealthCompany/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}