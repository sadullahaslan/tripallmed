﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tripallmed.Areas.HealthCompany.Business;
using tripallmed.Areas.HealthCompany.Models.DTO;
using tripallmed.Business;
using tripallmed.Entity;

namespace tripallmed.Areas.HealthCompany.Controllers
{
    [CustomAuthorizeAttribute(Roles = "health")]
    public class PanelController : Controller
    {
        MyProfileService MyProfileService = new MyProfileService();
        AdvertisementService AdvertisementService = new AdvertisementService();

        public ActionResult MyProfile()
        {
            HealthcareCompany healthCompany = MyProfileService.GetHealthcareCompany(User.Identity.Name);

            MyProfileDTO myProfileDTO = new MyProfileDTO();
            myProfileDTO.Title = healthCompany.Title;
            myProfileDTO.TaxAdministration = healthCompany.TaxAdministration;
            myProfileDTO.TaxNo = healthCompany.TaxNo;
            myProfileDTO.EstablishmentDate = healthCompany.EstablishmentDate;
            myProfileDTO.RenovationDate = healthCompany.RenovationDate;
            myProfileDTO.AreaM2 = healthCompany.AreaM2;
            myProfileDTO.BedCapacity = healthCompany.BedCapacity;
            myProfileDTO.PersonCount = healthCompany.PersonCount;
            myProfileDTO.DescriptionTurkish = healthCompany.DescriptionTurkish;
            myProfileDTO.DescriptionEnglish = healthCompany.DescriptionEnglish;
            myProfileDTO.DescriptionArabic = healthCompany.DescriptionArabic;
            myProfileDTO.Address = healthCompany.Address;
            myProfileDTO.ZipCode = healthCompany.ZipCode;
            myProfileDTO.AirportDistance = healthCompany.AirportDistance;
            myProfileDTO.CityCenterDistance = healthCompany.CityCenterDistance;
            myProfileDTO.Latitude = healthCompany.Latitude;
            myProfileDTO.Longitude = healthCompany.Longitude;
            myProfileDTO.Logo = healthCompany.Logo;
            myProfileDTO.CoverImage = healthCompany.CoverImage;
            myProfileDTO.CentralName = healthCompany.CentralName;
            myProfileDTO.PictureGalleries = healthCompany.PictureGalleries.Where(x => x.IsDeleted == false && x.Active == true).ToList();

            ViewBag.CompanyType = new SelectList(MyProfileService.GetCompaniesType(), "Id", "Type", MyProfileService.GetCompanyTypeId(User.Identity.Name));

            ViewBag.CentralType = new SelectList(MyProfileService.GetAllHealthCenterTypes(), "Id", "Type", MyProfileService.GetHealthCenterTypeId(User.Identity.Name));

            ViewBag.CompanyStatus = new SelectList(MyProfileService.GetAllHealthStatuses(), "Id", "Status", MyProfileService.GetHealthStatusId(User.Identity.Name));

            ViewBag.UsesLanguage = new MultiSelectList(MyProfileService.GetAllValidLanguages(), "Id", "LanguageName", MyProfileService.GetValidLanguagesIds(User.Identity.Name));

            ViewBag.SelectPolyclinic = new MultiSelectList(MyProfileService.GetAllPolyclinics(), "Id", "PolyclinicName", MyProfileService.GetPolyclinicIds(User.Identity.Name));

            ViewBag.ContractedInstitution = new MultiSelectList(MyProfileService.GetAllContractedInstitutions(), "Id", "Institution", MyProfileService.GetContractedInstitutionIds(User.Identity.Name));

            ViewBag.SelectCountry = new SelectList(MyProfileService.GetAllCountries(), "Id", "CountryName", MyProfileService.GetCountryId(User.Identity.Name));

            ViewBag.SelectCities = new SelectList(MyProfileService.GetAllCountrieCities(User.Identity.Name), "Id", "CityName", MyProfileService.GetCityId(User.Identity.Name));

            ViewBag.SelectTowns = new SelectList(MyProfileService.GetAllTowns(User.Identity.Name), "Id", "TownName", MyProfileService.GetTownId(User.Identity.Name));

            ViewBag.DoctorTitle = new SelectList(MyProfileService.GetAllDoctorTitles(), "Id", "Title");

            ViewBag.SelectDoctorPolyclinic = new SelectList(MyProfileService.GetAllPolyclinics(), "Id", "PolyclinicName");

            return View(myProfileDTO);
        }


        [HttpPost]
        public JsonResult CorporateInformationUpdate(CorporateInformationDTO model)
        {
            if(ModelState.IsValid)
            {
                var user = MyProfileService.GetHealthUser(User.Identity.Name);

                if(user == null)
                {
                    ModelState.AddModelError("", "Kaydedilirken hata oluştu lütfen sayfayı yeniden yükleyin.");
                    return Json("Hata", JsonRequestBehavior.AllowGet);
                }

                user.HealthcareCompany.Title = model.Title != null ? model.Title.Trim() : null;
                user.HealthcareCompany.TaxAdministration = model.TaxAdministration != null ? model.TaxAdministration.Trim() : null;
                user.HealthcareCompany.TaxNo = model.TaxNo != null ? model.TaxNo.Trim() : null;
                user.HealthcareCompany.CentralName = model.CentralName != null ? model.CentralName.Trim() : null;
                user.HealthcareCompany.EstablishmentDate = model.EstablishmentDate;
                user.HealthcareCompany.RenovationDate = model.RenovationDate;
                user.HealthcareCompany.AreaM2 = model.AreaM2;
                user.HealthcareCompany.BedCapacity = model.BedCapacity;
                user.HealthcareCompany.PersonCount = model.PersonCount;
                MyProfileService.ValidLanguagesUpdate(model.ValidLanguagesIds, user);
                user.HealthcareCompany.DescriptionTurkish = model.DescriptionTurkish != null ? model.DescriptionTurkish.Trim() : null;
                user.HealthcareCompany.DescriptionEnglish = model.DescriptionEnglish != null ? model.DescriptionEnglish.Trim() : null;
                user.HealthcareCompany.DescriptionArabic = model.DescriptionArabic != null ? model.DescriptionArabic.Trim() : null;
                user.HealthcareCompany.CompanyTypeId = model.CompanyTypeId;
                user.HealthcareCompany.HealthCenterTypeId = model.HealthCenterTypeId;
                user.HealthcareCompany.HealthStatusId = model.HealthStatusId;

                MyProfileService.HealthcareCompanyUpdate(user);

                return Json("Kaydedildi", JsonRequestBehavior.AllowGet);
            }

            return Json(CustomValidation.ValidationMessageDTO(ModelState), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult PolyclinickUpdate(HealthPolyclinicDTO model)
        {
            if(ModelState.IsValid)
            {
                var user = MyProfileService.GetHealthUser(User.Identity.Name);

                user.HealthcareCompany.Polyclinics.Clear();

                foreach(var id in model.PolyclinicIds)
                {
                    user.HealthcareCompany.Polyclinics.Add(MyProfileService.GetPolyclinic(id));
                }

                user.HealthcareCompany.ContractedInstitutions.Clear();

                foreach(var id in model.ContractedInstitutionIds)
                {
                    user.HealthcareCompany.ContractedInstitutions.Add(MyProfileService.GetContractedInstitution(id));
                }

                MyProfileService.HealthcareCompanyUpdate(user);

                return Json("Kaydedildi", JsonRequestBehavior.AllowGet);
            }

            return Json(CustomValidation.ValidationMessageDTO(ModelState), JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult DoktorAddOrUpdate(HealthDoctorDTO model)
        {
            if(ModelState.IsValid)
            {
                var user = MyProfileService.GetHealthUser(User.Identity.Name);

                var doctor = new Doctor();
                doctor.Name = model.Name != null ? model.Name.Trim() : null;
                doctor.Surname = model.Surname != null ? model.Surname.Trim() : null;
                doctor.DoctorTitleId = model.DoctorTitleid;
                doctor.PolyclinicId = model.PolyclinicId;

                user.HealthcareCompany.Doctors.Add(doctor);

                MyProfileService.HealthcareCompanyUpdate(user);

                string[] response = new string[2];

                response[0] = "Kaydedildi";
                response[1] = doctor.Id.ToString();

                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json(CustomValidation.ValidationMessageDTO(ModelState), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetAllCompanyDoctors()
        {
            var doctors = MyProfileService.GetCompanyDoctors(User.Identity.Name);

            return Json(doctors, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult CompanyDeleteDoctor(int id)
        {
            var user = MyProfileService.GetHealthUser(User.Identity.Name);

            user.HealthcareCompany.Doctors.FirstOrDefault(x => x.Id == id).IsDeleted = true;

            MyProfileService.HealthcareCompanyUpdate(user);

            return Json("Silindi", JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetAllCountryCities(int id)
        {
            var cities = MyProfileService.GetCountryCities(id);
            return Json(cities, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetAllCityesTown(int id)
        {
            var Towns = MyProfileService.GetCityTowns(id);
            return Json(Towns, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult LocationUpdate(HealthLocationDTO model)
        {
            if(ModelState.IsValid)
            {
                var user = MyProfileService.GetHealthUser(User.Identity.Name);

                user.HealthcareCompany.CountryId = model.CountryId;
                user.HealthcareCompany.CityId = model.CityId;
                user.HealthcareCompany.TownId = model.TownId;
                user.HealthcareCompany.Address = model.Address != null ? model.Address.Trim() : null;
                user.HealthcareCompany.ZipCode = model.ZipCode != null ? model.ZipCode.Trim() : null;
                user.HealthcareCompany.AirportDistance = model.AirportDistance;
                user.HealthcareCompany.CityCenterDistance = model.CityCenterDistance;
                user.HealthcareCompany.Latitude = model.Latitude;
                user.HealthcareCompany.Longitude = model.Longitude;

                MyProfileService.HealthcareCompanyUpdate(user);

                return Json("Kaydedildi", JsonRequestBehavior.AllowGet);
            }

            return Json(CustomValidation.ValidationMessageDTO(ModelState), JsonRequestBehavior.AllowGet);

        }



        [HttpPost]
        public JsonResult ImageSave(string logo, string coverImage)
        {
            if(logo == null && coverImage == null)
                return Json("Farklı bir resim seçin.", JsonRequestBehavior.AllowGet);

            if(MyProfileService.ImageSave(logo, coverImage, MyProfileService.GetHealthUser(User.Identity.Name)))
            {
                return Json("Kaydedildi", JsonRequestBehavior.AllowGet);
            }
            return Json("Hata oldu tekrar deneyin veya farklı bir resim seçin.", JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult GalleyImageSave(string galleryImage)
        {
            if(galleryImage != "")
            {
                var imageId = MyProfileService.GalleyImageSave(galleryImage, MyProfileService.GetHealthUser(User.Identity.Name));
                if(imageId != 0)
                {
                    var response = new KeyValuePair<int, string>(imageId, "Kaydedildi");
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Hata oldu tekrar deneyin veya farklı bir resim seçin.", JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult GalleyImageDelete(int Id)
        {
            if(MyProfileService.GalleyImageDelete(Id, MyProfileService.GetHealthUser(User.Identity.Name)))
            {
                return Json("Silindi", JsonRequestBehavior.AllowGet);
            }
            return Json("Hata oldu tekrar deneyin veya farklı bir resim seçin.", JsonRequestBehavior.AllowGet);
        }


        public ActionResult AdvertisementList()
        {
            var AdvertisementList = AdvertisementService.GetAllHealthAdvertisements(User.Identity.Name);
            AdvertisementList.Reverse();
            ViewBag.AdvertisementList = AdvertisementList;
            return View();
        }


        public ActionResult AdvertisementAdd()
        {
            if(AdvertisementService.HealthCompanyMyPfofileCheck(User.Identity.Name))
            {
                TempData["AdverNo"] = "İlan yayınlayabilmek için lütfen profilim sekmesinde istenen tüm bilgileri giriniz.";
                return RedirectToAction("AdvertisementList");
            }

            ViewBag.MedicalCenter = new SelectList(AdvertisementService.GetBranchHospitals(), "Id", "BranchName");
            ViewBag.IncludePrice = new MultiSelectList(AdvertisementService.GetAllIncludedPrices(), "Id", "Service");
            ViewBag.CurrnetUnit = new SelectList(AdvertisementService.GetCurrencyUnits(), "Id", "Unit");

            return View();
        }


        [HttpPost]
        public ActionResult AdvertisementAdd(HealthAdvertisementDTO model)
        {
            if(ModelState.IsValid)
            {
                var user = AdvertisementService.GetHealthUser(User.Identity.Name);

                HealthAdvertisement healthAdvertisement = new HealthAdvertisement();

                healthAdvertisement.AdvertTitle = model.AdvertTitle != null ? model.AdvertTitle.Trim() : null;
                healthAdvertisement.AdvertTitle_Eng = model.AdvertTitle_Eng != null ? model.AdvertTitle_Eng.Trim() : null;
                healthAdvertisement.AdvertTitle_Oth = model.AdvertTitle_Oth != null ? model.AdvertTitle_Oth.Trim() : null;
                healthAdvertisement.MedicalCenterId = model.MedicalCenterId;
                healthAdvertisement.MedicalUnitId = model.MedicalUnitId;
                healthAdvertisement.OperationId = model.OperationId;
                healthAdvertisement.What = model.What != null ? model.What.Trim() : null;
                healthAdvertisement.What_Eng = model.What_Eng != null ? model.What_Eng.Trim() : null;
                healthAdvertisement.What_Oth = model.What_Oth != null ? model.What_Oth.Trim() : null;
                healthAdvertisement.Preparation = model.Preparation != null ? model.Preparation.Trim() : null;
                healthAdvertisement.Preparation_Eng = model.Preparation_Eng != null ? model.Preparation_Eng.Trim() : null;
                healthAdvertisement.Preparation_Oth = model.Preparation_Oth != null ? model.Preparation_Oth.Trim() : null;
                healthAdvertisement.Time = model.Time != null ? model.Time.Trim() : null;
                healthAdvertisement.Time_Eng = model.Time_Eng != null ? model.Time_Eng.Trim() : null;
                healthAdvertisement.Time_Oth = model.Time_Oth != null ? model.Time_Oth.Trim() : null;
                healthAdvertisement.Post = model.Post != null ? model.Post.Trim() : null;
                healthAdvertisement.Post_Eng = model.Post_Eng != null ? model.Post_Eng.Trim() : null;
                healthAdvertisement.Post_Oth = model.Post_Oth != null ? model.Post_Oth.Trim() : null;
                healthAdvertisement.IncludedPrice = AdvertisementService.GetIncludedPriceIds(model.IncludedPriceIds);
                healthAdvertisement.OperationOption = model.OperationOption;
                healthAdvertisement.ProcessingFee = model.ProcessingFee;
                healthAdvertisement.CurrencyUnitId = model.CurrencyUnitId;
                healthAdvertisement.CampaignDiscount = model.CampaignDiscount;
                healthAdvertisement.CampaignStart = model.CampaignStart;
                healthAdvertisement.CampaignEnd = model.CampaignEnd;
                healthAdvertisement.Prepayment = model.Prepayment;
                healthAdvertisement.CancellationPolicy = model.CancellationPolicy;
                healthAdvertisement.CoverImage = model.CoverImage;
                healthAdvertisement.CompletionRate = AdvertisementService.HealthAdvertisementCompletionRate(model);

                healthAdvertisement.AdvertisementStatusId = AdvertisementService.SetStatus(healthAdvertisement.CompletionRate);

                user.HealthcareCompany.HealthAdvertisements.Add(healthAdvertisement);

                AdvertisementService.HealthcareCompanyUpdate(user);

                return RedirectToAction("AdvertisementList");
            }

            ViewBag.MedicalCenter = new SelectList(AdvertisementService.GetBranchHospitals(), "Id", "BranchName");
            ViewBag.IncludePrice = new MultiSelectList(AdvertisementService.GetAllIncludedPrices(), "Id", "Service");
            ViewBag.CurrnetUnit = new SelectList(AdvertisementService.GetCurrencyUnits(), "Id", "Unit");

            return View(model);
        }


        public ActionResult AdvertisementUpdate(int id)
        {
            var healthAdvertisement = AdvertisementService.GetHealthAdvertisement(id);

            if(healthAdvertisement == null) return RedirectToAction("AdvertisementList");

            var model = new HealthAdvertisementDTO();

            model.Id = healthAdvertisement.Id;
            model.AdvertTitle = healthAdvertisement.AdvertTitle;
            model.AdvertTitle_Eng = healthAdvertisement.AdvertTitle_Eng;
            model.AdvertTitle_Oth = healthAdvertisement.AdvertTitle_Oth;
            model.MedicalCenterId = healthAdvertisement.MedicalCenterId;
            model.MedicalUnitId = healthAdvertisement.MedicalUnitId;
            model.OperationId = healthAdvertisement.OperationId;

            ViewBag.MedicalCenter = new SelectList(AdvertisementService.GetBranchHospitals(), "Id", "BranchName", healthAdvertisement.MedicalCenterId);

            ViewBag.MedicalUnit = new SelectList(AdvertisementService.GetAllBrancPolyclinicName(healthAdvertisement.MedicalCenterId), "Id", "PolyclinicName", healthAdvertisement.MedicalUnitId);

            ViewBag.Operation = new SelectList(AdvertisementService.GetAllPolyclinicServiceName(healthAdvertisement.MedicalUnitId), "Id", "ServiceName", healthAdvertisement.OperationId);

            model.What = healthAdvertisement.What;
            model.What_Eng = healthAdvertisement.What_Eng;
            model.What_Oth = healthAdvertisement.What_Oth;
            model.Preparation = healthAdvertisement.Preparation;
            model.Preparation_Eng = healthAdvertisement.Preparation_Eng;
            model.Preparation_Oth = healthAdvertisement.Preparation_Oth;
            model.Time = healthAdvertisement.Time;
            model.Time_Eng = healthAdvertisement.Time_Eng;
            model.Time_Oth = healthAdvertisement.Time_Oth;
            model.Post = healthAdvertisement.Post;
            model.Post_Eng = healthAdvertisement.Post_Eng;
            model.Post_Oth = healthAdvertisement.Post_Oth;

            if(healthAdvertisement.IncludedPrice != null)
            {
                int[] IncludedPriceIds = new int[healthAdvertisement.IncludedPrice.Count()];
                int number = 0;
                foreach(var includedPrice in healthAdvertisement.IncludedPrice)
                {
                    IncludedPriceIds[number] = includedPrice.Id;
                    number++;
                }

                ViewBag.IncludedPrice = new MultiSelectList(AdvertisementService.GetAllIncludedPrices(), "Id", "Service", IncludedPriceIds);
            }
            else
            {
                ViewBag.IncludedPrice = new MultiSelectList(AdvertisementService.GetAllIncludedPrices(), "Id", "Service");
            }


            model.OperationOption = healthAdvertisement.OperationOption;
            model.ProcessingFee = healthAdvertisement.ProcessingFee;

            ViewBag.CurrnetUnit = new SelectList(AdvertisementService.GetCurrencyUnits(), "Id", "Unit", healthAdvertisement.CurrencyUnitId);

            model.ProcessingFee = healthAdvertisement.ProcessingFee;
            model.Prepayment = healthAdvertisement.Prepayment;
            model.CampaignDiscount = healthAdvertisement.CampaignDiscount;
            model.CampaignStart = healthAdvertisement.CampaignStart;
            model.CampaignEnd = healthAdvertisement.CampaignEnd;
            model.Prepayment = healthAdvertisement.Prepayment;
            model.CancellationPolicy = healthAdvertisement.CancellationPolicy;
            model.CoverImage = healthAdvertisement.CoverImage;

            return View(model);
        }


        public ActionResult DeleteAdvertisement(int id)
        {
            AdvertisementService.DeleteAdvertisement(id);

            return RedirectToAction("AdvertisementList");
        }


        [HttpPost]
        public ActionResult AdvertisementUpdate(HealthAdvertisementDTO model)
        {
            if(ModelState.IsValid)
            {
                var healthAdvertisement = AdvertisementService.GetHealthAdvertisement(model.Id);

                if(healthAdvertisement == null) RedirectToAction("AdvertisementList");

                healthAdvertisement.AdvertTitle = model.AdvertTitle != null ? model.AdvertTitle.Trim() : null;
                healthAdvertisement.AdvertTitle_Eng = model.AdvertTitle_Eng != null ? model.AdvertTitle_Eng.Trim() : null;
                healthAdvertisement.AdvertTitle_Oth = model.AdvertTitle_Oth != null ? model.AdvertTitle_Oth.Trim() : null;
                healthAdvertisement.MedicalCenterId = model.MedicalCenterId;
                healthAdvertisement.MedicalUnitId = model.MedicalUnitId;
                healthAdvertisement.OperationId = model.OperationId;
                healthAdvertisement.What = model.What != null ? model.What.Trim() : null;
                healthAdvertisement.What_Eng = model.What_Eng != null ? model.What_Eng.Trim() : null;
                healthAdvertisement.What_Oth = model.What_Oth != null ? model.What_Oth.Trim() : null;
                healthAdvertisement.Preparation = model.Preparation != null ? model.Preparation.Trim() : null;
                healthAdvertisement.Preparation_Eng = model.Preparation_Eng != null ? model.Preparation_Eng.Trim() : null;
                healthAdvertisement.Preparation_Oth = model.Preparation_Oth != null ? model.Preparation_Oth.Trim() : null;
                healthAdvertisement.Time = model.Time != null ? model.Time.Trim() : null;
                healthAdvertisement.Time_Eng = model.Time_Eng != null ? model.Time_Eng.Trim() : null;
                healthAdvertisement.Time_Oth = model.Time_Oth != null ? model.Time_Oth.Trim() : null;
                healthAdvertisement.Post = model.Post != null ? model.Post.Trim() : null;
                healthAdvertisement.Post_Eng = model.Post_Eng != null ? model.Post_Eng.Trim() : null;
                healthAdvertisement.Post_Oth = model.Post_Oth != null ? model.Post_Oth.Trim() : null;
                if(healthAdvertisement.IncludedPrice != null) healthAdvertisement.IncludedPrice.Clear();
                healthAdvertisement.IncludedPrice = AdvertisementService.GetIncludedPriceIds(model.IncludedPriceIds);
                healthAdvertisement.OperationOption = model.OperationOption;
                healthAdvertisement.ProcessingFee = model.ProcessingFee;
                healthAdvertisement.CurrencyUnitId = model.CurrencyUnitId;
                healthAdvertisement.Prepayment = model.Prepayment;
                healthAdvertisement.CampaignDiscount = model.CampaignDiscount;
                healthAdvertisement.CampaignStart = model.CampaignStart;
                healthAdvertisement.CampaignEnd = model.CampaignEnd;
                healthAdvertisement.CancellationPolicy = model.CancellationPolicy;
                if(model.CoverImage != null)
                {
                    string pathToDelete = System.Web.HttpContext.Current.Server.MapPath("~/Data/") + healthAdvertisement.CoverImage;
                    if(System.IO.File.Exists(pathToDelete))
                    {
                        System.IO.File.Delete(pathToDelete);
                    }
                }
                healthAdvertisement.CoverImage = model.CoverImage != null ? model.CoverImage : healthAdvertisement.CoverImage;
                model.CoverImage = healthAdvertisement.CoverImage;
                healthAdvertisement.CompletionRate = AdvertisementService.HealthAdvertisementCompletionRate(model);

                healthAdvertisement.AdvertisementStatusId = AdvertisementService.SetStatus(healthAdvertisement.CompletionRate);

                AdvertisementService.HealthAdvertisementUpdate(healthAdvertisement);

                return RedirectToAction("AdvertisementList");
            }

            ViewBag.MedicalCenter = new SelectList(AdvertisementService.GetBranchHospitals(), "Id", "BranchName", model.MedicalCenterId);

            ViewBag.MedicalUnit = new SelectList(AdvertisementService.GetAllBrancPolyclinicName(model.MedicalCenterId), "Id", "PolyclinicName", model.MedicalUnitId);

            ViewBag.Operation = new SelectList(AdvertisementService.GetAllPolyclinicServiceName(model.MedicalUnitId), "Id", "ServiceName", model.OperationId);

            ViewBag.IncludedPrice = new MultiSelectList(AdvertisementService.GetAllIncludedPrices(), "Id", "Service", model.IncludedPriceIds);

            ViewBag.CurrnetUnit = new SelectList(AdvertisementService.GetCurrencyUnits(), "Id", "Unit", model.CurrencyUnitId);

            return View(model);
        }


        [HttpPost]
        public JsonResult GetAllBrancPolyclinicName(int id)
        {
            return Json(AdvertisementService.GetAllBrancPolyclinicName(id), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetAllPolyclinicServiceName(int id)
        {
            return Json(AdvertisementService.GetAllPolyclinicServiceName(id), JsonRequestBehavior.AllowGet);
        }
    }
}