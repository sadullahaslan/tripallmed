﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using tripallmed.Areas.HealthCompany.Models.DTO;
using tripallmed.Business;
using tripallmed.Entity;

namespace tripallmed.Areas.HealthCompany.Business
{
    public class AdvertisementService : DbInstance
    {


        /// <summary>
        /// Firmanın ilanlarını gerye dönen fonksiyon
        /// </summary>
        /// <param name="healthcareCompany">Firma entity</param>
        /// <returns></returns>
        public List<HealthAdvertisement> GetAllHealthAdvertisements(HealthcareCompany healthcareCompany)
        {
            if(healthcareCompany == null) return null;

            if(healthcareCompany.HealthAdvertisements == null) return new List<HealthAdvertisement>();

            return healthcareCompany.HealthAdvertisements.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Firmanın ilanlarını gerye dönen fonksiyon
        /// </summary>
        /// <param name="healthcareCompany">Firma entity</param>
        /// <returns></returns>
        public List<HealthAdvertisement> GetAllHealthAdvertisements(string email)
        {
            return db.HealthUsers.FirstOrDefault(x => x.Email == email).HealthcareCompany.HealthAdvertisements.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }





        /// <summary>
        /// Seçilmiş sağlık firmasını geriye döndürür
        /// </summary>
        /// <param name="healthEmail">kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public HealthcareCompany GetHealthcareCompany(string healthEmail)
        {
            var aaaa = db.HealthAdvertisements.FirstOrDefault();




            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail);
            if(healthUser != null)
            {
                return healthUser.HealthcareCompany;
            }
            return null;
        }


        /// <summary>
        /// Tüm dal hastane kategorisini getiren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<BranchHospital> GetBranchHospitals()
        {
            return db.BranchHospitals.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }



        /// <summary>
        /// Fiyata datil hizmetleri getiren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<IncludedPrice> GetAllIncludedPrices()
        {
            return db.IncludedPrices.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Tüm para birimlerini getiren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<CurrencyUnit> GetCurrencyUnits()
        {
            return db.CurrencyUnits.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        public List<Polyclinic> GetAllBrancPolyclinicName(int id)
        {
            var branch = db.BranchHospitals.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
            if(branch != null)
            {
                return db.BranchHospitals.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true).Polyclinics.Where(x => x.IsDeleted == false && x.Active == true).Select(x => new Polyclinic()
                {
                    Id = x.Id,
                    PolyclinicName = x.PolyclinicName
                }).ToList();

            }
            return null;
        }


        public List<PolyclinicService> GetAllPolyclinicServiceName(int id)
        {
            var polyclinic = db.Polyclinics.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
            if(polyclinic != null)
            {
                return db.Polyclinics.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true).PolyclinicServices.Where(x => x.IsDeleted == false && x.Active == true).Select(x => new PolyclinicService
                {
                    Id = x.Id,
                    ServiceName = x.ServiceName
                }).ToList();

            }
            return null;
        }


        /// <summary>
        /// Sağlık firmasının bilgilerini veri tabanına kaydeden fonksiyon
        /// </summary>
        /// <param name="healthUser">Kullanıcı</param>
        /// <returns></returns>
        public bool HealthcareCompanyUpdate(HealthUser healthUser)
        {
            if(healthUser == null) return false;

            db.HealthUsers.AddOrUpdate(healthUser);
            return db.SaveChanges() > 0;
        }


        /// <summary>
        /// Sağlık kullanıcısını geri döndüren fonksiyon
        /// </summary>
        /// <param name="email">Kullanıcı email</param>
        /// <returns></returns>
        public HealthUser GetHealthUser(string email)
        {
            return db.HealthUsers.FirstOrDefault(x => x.Email == email);
        }


        public List<IncludedPrice> GetIncludedPriceIds(int[] ids)
        {
            List<IncludedPrice> ıncludedPrices = new List<IncludedPrice>();
            if(ids != null)
            {
                foreach(var includedPrice in db.IncludedPrices.ToList())
                {
                    foreach(var id in ids)
                    {
                        if(includedPrice.Id == id)
                        {
                            ıncludedPrices.Add(includedPrice);
                        }
                    }
                }
                return ıncludedPrices;
            }
            return null;
        }

        public HealthAdvertisement GetHealthAdvertisement(int id)
        {
            return db.HealthAdvertisements.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
        }


        public bool DeleteAdvertisement(int id)
        {
            var adver = db.HealthAdvertisements.Find(id);
            if(adver != null)
            {
                adver.IsDeleted = true;
                return db.SaveChanges() > 0;
            }
            return false;
        }


        public bool HealthCompanyMyPfofileCheck(string healthEmail)
        {
            var HealthCompany = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true).HealthcareCompany;

            Type t = HealthCompany.GetType();

            var properties = t.GetProperties();

            foreach(var prop in properties)
            {
                if(prop.Name == "DescriptionEnglish") continue;
                if(prop.Name == "DescriptionArabic") continue;
                if(prop.Name == "Logo") continue;

                if(prop.PropertyType.Name == "String" || prop.PropertyType.Name == "Nullable`1")
                {
                    var value = prop.GetValue(HealthCompany, null);

                    if(value == null)
                    {
                        return true;
                    }
                }
            }
            if(HealthCompany.PictureGalleries.Where(x => x.IsDeleted == false).Count() == 0)
                return true;
            if(HealthCompany.ValidLanguages.Where(x => x.IsDeleted == false).Count() == 0)
                return true;
            if(HealthCompany.Polyclinics.Where(x => x.IsDeleted == false).Count() == 0)
                return true;
            if(HealthCompany.ContractedInstitutions.Where(x => x.IsDeleted == false).Count() == 0)
                return true;
            if(HealthCompany.Doctors.Where(x => x.IsDeleted == false).Count() == 0)
                return true;

            return false;

        }

        public bool HealthAdvertisementUpdate(HealthAdvertisement model)
        {
            if(model == null) return false;

            db.HealthAdvertisements.AddOrUpdate(model);
            return db.SaveChanges() > 0;
        }

        public short HealthAdvertisementCompletionRate(HealthAdvertisementDTO model)
        {
            Type t = model.GetType();

            var properties = t.GetProperties();

            int b = 0;

            foreach(var prop in properties)
            {
                if(prop.Name == "AdvertTitle_Eng" || prop.Name == "AdvertTitle_Oth" || prop.Name == "What_Eng" || prop.Name == "What_Oth" || prop.Name == "Preparation_Eng" || prop.Name == "Preparation_Oth" || prop.Name == "Time_Eng" || prop.Name == "Time_Oth" || prop.Name == "Post_Eng" || prop.Name == "Post_Oth" || prop.Name == "IncludedPriceIds" || prop.Name == "CampaignStart" || prop.Name == "CampaignEnd") continue;

                var value = prop.GetValue(model, null);

                if(value != null)
                {
                    b++;
                }
            }

            int totalProp = (properties.Length - 13);

            var rate = ((b * 100) / totalProp);

            return Convert.ToInt16(rate);

        }

        public string ImageSave(string coverImageNew, string coverImage)
        {
            if(coverImageNew == null || coverImageNew == coverImage) return coverImage;

            string pathToDelete = HttpContext.Current.Server.MapPath("~/Data/") + coverImage;
            if(File.Exists(pathToDelete))
            {
                File.Delete(pathToDelete);
            }

            return coverImageNew;
        }

        public int SetStatus(short rate)
        {
            if(rate == 100) return 1;
            return 2;
        }

    }
}