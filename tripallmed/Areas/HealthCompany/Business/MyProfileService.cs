﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using tripallmed.Business;
using tripallmed.Entity;

namespace tripallmed.Areas.HealthCompany.Business
{
    public class MyProfileService : DbInstance
    {
        /// <summary>
        /// Firma tiplerinin tümünü geriye döndürür.
        /// </summary>
        /// <returns></returns>
        public List<CompanyType> GetCompaniesType()
        {
            return db.CompanyTypes.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Firmanın seçilmiş olan firma tipinin idisini geriye döndürür.
        /// </summary>
        /// <param name="healthEmail">Firmanın kayıtlı email adresi</param>
        /// <returns></returns>
        public int? GetCompanyTypeId(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                return healthUser.HealthcareCompany.CompanyTypeId;
            }
            return null;
        }


        /// <summary>
        /// Seçilmiş sağlık firmasını geriye döndürür
        /// </summary>
        /// <param name="healthEmail">kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public HealthcareCompany GetHealthcareCompany(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                return healthUser.HealthcareCompany;
            }
            return null;
        }


        /// <summary>
        /// Sağlık merkez tipinin tümünü geriye döndüren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<HealthCenterType> GetAllHealthCenterTypes()
        {
            return db.HealthCenterTypes.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Firmanın Sağlık merkez tipinin idsini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public int? GetHealthCenterTypeId(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                return healthUser.HealthcareCompany.HealthCenterTypeId;
            }
            return null;
        }


        /// <summary>
        /// Sağlık statüsünün tümünü geriye döndüren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<HealthStatus> GetAllHealthStatuses()
        {
            return db.HealthStatuses.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Firmanın sağlık statüsü idsini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public int? GetHealthStatusId(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                return healthUser.HealthcareCompany.HealthStatusId;
            }
            return null;
        }


        /// <summary>
        /// Geçerli dillerin tümünü geriye döndüren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<ValidLanguages> GetAllValidLanguages()
        {
            return db.ValidLanguages.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Firmanın Geçerli dillerinin idsini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public int[] GetValidLanguagesIds(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                int[] ids = new int[healthUser.HealthcareCompany.ValidLanguages.Count()];
                int number = 0;
                foreach(ValidLanguages validLanguages in healthUser.HealthcareCompany.ValidLanguages)
                {
                    ids[number] = validLanguages.Id;
                    number++;
                }
                return ids;
            }
            return null;
        }


        /// <summary>
        /// Polikliniklerin tümünü geriye döndüren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<Polyclinic> GetAllPolyclinics()
        {
            return db.Polyclinics.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Firmanın seçilmiş olan polikliniklerinin idsini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public int[] GetPolyclinicIds(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                int[] ids = new int[healthUser.HealthcareCompany.Polyclinics.Count()];
                int number = 0;
                foreach(Polyclinic polyclinic in healthUser.HealthcareCompany.Polyclinics)
                {
                    ids[number] = polyclinic.Id;
                    number++;
                }
                return ids;
            }
            return null;
        }



        /// <summary>
        /// Anlaşmalı kurumların tümünü geriye döndüren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<ContractedInstitution> GetAllContractedInstitutions()
        {
            return db.ContractedInstitutions.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Firmanın seçilmiş olan anlaşmalı kurumların idsini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public int[] GetContractedInstitutionIds(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                int[] ids = new int[healthUser.HealthcareCompany.ContractedInstitutions.Count()];
                int number = 0;
                foreach(ContractedInstitution contractedInstitution in healthUser.HealthcareCompany.ContractedInstitutions)
                {
                    ids[number] = contractedInstitution.Id;
                    number++;
                }
                return ids;
            }
            return null;
        }


        /// <summary>
        /// Ülkelerin tümünü geriye döndüren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<Country> GetAllCountries()
        {
            return db.Countries.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Firmanın tercih edilen ülkesinin idisini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public int? GetCountryId(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                return healthUser.HealthcareCompany.CountryId;
            }
            return null;

        }


        /// <summary>
        /// Firmanın seçili olan ülkenin şehirlerini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public List<City> GetAllCountrieCities(string healthEmail)
        {
            int? id = GetCountryId(healthEmail);
            if(id != null)
            {
                return db.Cities.Where(x => x.CountryId == id && x.IsDeleted == false && x.Active == true).ToList();
            }
            return new List<City>();
        }


        /// <summary>
        /// Firmanın tercih edilen şehrinin idisini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public int? GetCityId(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                return healthUser.HealthcareCompany.CityId;
            }
            return null;
        }


        /// <summary>
        /// Firmanın seçili olan şehrinin ilçelerini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public List<Town> GetAllTowns(string healthEmail)
        {
            int? id = GetCityId(healthEmail);
            if(id != null)
            {
                return db.Towns.Where(x => x.CityId == id && x.IsDeleted == false && x.Active == true).ToList();
            }
            return new List<Town>();
        }


        /// <summary>
        /// Firmanın tercih edilen ilçesinin idisini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public int? GetTownId(string healthEmail)
        {
            var healthUser = db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true);
            if(healthUser != null)
            {
                return healthUser.HealthcareCompany.TownId;
            }
            return null;
        }


        /// <summary>
        /// Doktor ünvanlarının tümünü geriye döndüren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<DoctorTitle> GetAllDoctorTitles()
        {
            return db.DoctorTitles.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Sağlık kullanıcısını geri döndüren fonksiyon
        /// </summary>
        /// <param name="email">Kullanıcı email</param>
        /// <returns></returns>
        public HealthUser GetHealthUser(string email)
        {
            return db.HealthUsers.FirstOrDefault(x => x.Email == email && x.IsDeleted == false && x.Active == true);
        }


        /// <summary>
        /// Firmanın tercih edilen dillerini güncelleyen fonksiyon
        /// </summary>
        /// <param name="ids">Tercih edilen dillerin id leri</param>
        /// <param name="healthUser">Kullanıcı</param>
        public void ValidLanguagesUpdate(int[] ids, HealthUser healthUser)
        {
            healthUser.HealthcareCompany.ValidLanguages.Clear();
            for(int i = 0; i < ids.Count(); i++)
            {
                healthUser.HealthcareCompany.ValidLanguages.Add(GetValidLanguages(ids[i]));
            }
        }


        /// <summary>
        /// Geçerli dil geriye döndüren fonksiyon
        /// </summary>
        /// <param name="id">Geçerli dil id si</param>
        /// <returns></returns>
        public ValidLanguages GetValidLanguages(int id)
        {
            return db.ValidLanguages.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
        }


        /// <summary>
        /// Sağlık firmasının bilgilerini veri tabanına kaydeden fonksiyon
        /// </summary>
        /// <param name="healthUser">Kullanıcı</param>
        /// <returns></returns>
        public bool HealthcareCompanyUpdate(HealthUser healthUser)
        {
            if(healthUser == null) return false;

            db.HealthUsers.AddOrUpdate(healthUser);
            return db.SaveChanges() > 0;
        }


        /// <summary>
        /// İd ye göre poliklinik dönen fonksiyon
        /// </summary>
        /// <param name="id">Poliklinik id</param>
        /// <returns></returns>
        public Polyclinic GetPolyclinic(int id)
        {
            return db.Polyclinics.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
        }


        /// <summary>
        /// İd ye göre anlaşmalı kurum dönen fonksiyon
        /// </summary>
        /// <param name="id">Poliklinik id</param>
        /// <returns></returns>
        public ContractedInstitution GetContractedInstitution(int id)
        {
            return db.ContractedInstitutions.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
        }



        /// <summary>
        /// Kullanıcı maili verilen firmanın doktorlarını geriye dönen fonksiyon.
        /// </summary>
        /// <param name="healthEmail">Kullanıcı email</param>
        /// <returns></returns>
        public object GetCompanyDoctors(string healthEmail)
        {
            if(db.HealthUsers.Any(x => x.Email == healthEmail))
            {
                return db.HealthUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true).HealthcareCompany.Doctors.Where(x => x.IsDeleted == false && x.Active == true).Select(x => new
                {
                    id = x.Id,
                    doktorType = x.DoctorTitle.Title,
                    doctorName = x.Name,
                    doctorSurname = x.Surname,
                    polyclinicName = x.Polyclinic.PolyclinicName
                });
            }
            return null;
        }


        /// <summary>
        /// İdsi verilen ülkenin şehirlerini object olarak dönen fonksiyon
        /// </summary>
        /// <param name="id">Ülke id</param>
        /// <returns></returns>
        public object GetCountryCities(int id)
        {
            return db.Countries.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true).Cities.Where(x => x.IsDeleted == false && x.Active == true).Select(x => new
            {
                Id = x.Id,
                CityName = x.CityName
            }).ToList();
        }


        /// <summary>
        /// İdsi verilen şehrin ilçelerini object olarak dönen fonksiyon
        /// </summary>
        /// <param name="id">şehir id</param>
        /// <returns></returns>

        public object GetCityTowns(int id)
        {
            return db.Cities.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true).Towns.Where(x => x.IsDeleted == false && x.Active == true).Select(x => new
            {
                Id = x.Id,
                TownName = x.TownName
            }).ToList();
        }



        /// <summary>
        /// Firma Logo ve kapak resmini veri tabanına katdeden fonksiyon
        /// </summary>
        /// <param name="logo">logo adı</param>
        /// <param name="coverImage">kapak resmi adı</param>
        /// <param name="healthUser">kullanıcı</param>
        /// <returns></returns>
        public bool ImageSave(string logo, string coverImage, HealthUser healthUser)
        {
            if(logo != null && logo != "")
            {
                string pathToDelete = HttpContext.Current.Server.MapPath("~/Data/") + healthUser.HealthcareCompany.Logo;
                if(File.Exists(pathToDelete))
                {
                    File.Delete(pathToDelete);
                }
                healthUser.HealthcareCompany.Logo = logo;
            }

            if(coverImage != null && coverImage != "")
            {
                string pathToDelete = HttpContext.Current.Server.MapPath("~/Data/") + healthUser.HealthcareCompany.CoverImage;
                if(File.Exists(pathToDelete))
                {
                    File.Delete(pathToDelete);
                }
                healthUser.HealthcareCompany.CoverImage = coverImage;
            }
            return HealthcareCompanyUpdate(healthUser);
        }



        public int GalleyImageSave(string image, HealthUser healthUser)
        {
            var img = new PictureGallery() { Name = image };
            healthUser.HealthcareCompany.PictureGalleries.Add(img);
            HealthcareCompanyUpdate(healthUser);
            return img.Id;
        }

        public bool GalleyImageDelete(int Id, HealthUser healthUser)
        {
            string pathToDelete = HttpContext.Current.Server.MapPath("~/Data/") + healthUser.HealthcareCompany.PictureGalleries.FirstOrDefault(x => x.Id == Id).Name;
            if(File.Exists(pathToDelete))
            {
                File.Delete(pathToDelete);
            }

            healthUser.HealthcareCompany.PictureGalleries.FirstOrDefault(x => x.Id == Id).IsDeleted = true;
            return HealthcareCompanyUpdate(healthUser);
        }
    }
}
