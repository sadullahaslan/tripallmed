﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tripallmed.Areas.TourismCompany.Business;
using tripallmed.Areas.TourismCompany.Models;
using tripallmed.Business;
using tripallmed.Entity;

namespace tripallmed.Areas.TourismCompany.Controllers
{
    [CustomAuthorizeAttribute(Roles = "tourism")]
    public class PanelController : Controller
    {
        MyProfileService MyProfileService = new MyProfileService();
        AdvertisementService AdvertisementService = new AdvertisementService();

        public ActionResult MyProfile()
        {
            TourismmCompany tourismCompany = MyProfileService.GetTourismCompany(User.Identity.Name);

            MyProfileDTO myProfileDTO = new MyProfileDTO();

            myProfileDTO.Title = tourismCompany.Title;
            myProfileDTO.TaxAdministration = tourismCompany.TaxAdministration;
            myProfileDTO.TaxNo = tourismCompany.TaxNo;
            myProfileDTO.EstablishmentDate = tourismCompany.EstablishmentDate;
            myProfileDTO.RenovationDate = tourismCompany.RenovationDate;
            myProfileDTO.AreaM2 = tourismCompany.AreaM2;
            myProfileDTO.RoomCapacity = tourismCompany.RoomCapacity;
            myProfileDTO.BedCapacity = tourismCompany.BedCapacity;
            myProfileDTO.DescriptionTurkish = tourismCompany.DescriptionTurkish;
            myProfileDTO.DescriptionEnglish = tourismCompany.DescriptionEnglish;
            myProfileDTO.DescriptionArabic = tourismCompany.DescriptionArabic;
            myProfileDTO.Address = tourismCompany.Address;
            myProfileDTO.ZipCode = tourismCompany.ZipCode;
            myProfileDTO.AirportDistance = tourismCompany.AirportDistance;
            myProfileDTO.CityCenterDistance = tourismCompany.CityCenterDistance;
            myProfileDTO.Latitude = tourismCompany.Latitude;
            myProfileDTO.Longitude = tourismCompany.Longitude;
            myProfileDTO.Logo = tourismCompany.Logo;
            myProfileDTO.CoverImage = tourismCompany.CoverImage;
            myProfileDTO.FacilityName = tourismCompany.FacilityName;
            myProfileDTO.CheckInTime = tourismCompany.CheckInTime;
            myProfileDTO.CheckOutTime = tourismCompany.CheckOutTime;
            myProfileDTO.PictureGalleries = tourismCompany.PictureGalleries.Where(x => x.IsDeleted == false && x.Active == true).ToList();


            ViewBag.CompanyType = new SelectList(MyProfileService.GetCompaniesType(), "Id", "Type", MyProfileService.GetCompanyTypeId(User.Identity.Name));


            ViewBag.FacilityType = new SelectList(MyProfileService.GetAllFacilityTypes(), "Id", "Type", MyProfileService.GetFacilityTypesId(User.Identity.Name));

            ViewBag.TourismStatus = new SelectList(MyProfileService.GetAllTourismStatuses(), "Id", "Status", MyProfileService.GetTourismStatusesId(User.Identity.Name));

            ViewBag.TourismTheme = new SelectList(MyProfileService.GetAllTourismThemas(), "Id", "ThemeName", MyProfileService.GetTourismThemaId(User.Identity.Name));

            ViewBag.TourismScopes = new MultiSelectList(MyProfileService.GetAllTourismScopes(), "Id", "Scopes", MyProfileService.GetTourismScopesIds(User.Identity.Name));

            ViewBag.UsesLanguage = new MultiSelectList(MyProfileService.GetAllValidLanguages(), "Id", "LanguageName", MyProfileService.GetValidLanguagesIds(User.Identity.Name));

            ViewBag.Rule = new MultiSelectList(MyProfileService.GetAllTourismRules(), "Id", "Name", MyProfileService.GetTourismRulesIds(User.Identity.Name));

            ViewBag.SelectCountry = new SelectList(MyProfileService.GetAllCountries(), "Id", "CountryName", MyProfileService.GetCountryId(User.Identity.Name));

            ViewBag.SelectCities = new SelectList(MyProfileService.GetAllCountrieCities(User.Identity.Name), "Id", "CityName", MyProfileService.GetCityId(User.Identity.Name));

            ViewBag.SelectTowns = new SelectList(MyProfileService.GetAllTowns(User.Identity.Name), "Id", "TownName", MyProfileService.GetTownId(User.Identity.Name));


            return View(myProfileDTO);
        }


        [HttpPost]
        public JsonResult CorporateInformationUpdate(CorporateInformationDTO model)
        {
            if(ModelState.IsValid)
            {
                var user = MyProfileService.GetTourismhUser(User.Identity.Name);

                if(user == null)
                {
                    ModelState.AddModelError("", "Kaydedilirken hata oluştu lütfen sayfayı yeniden yükleyin.");
                    return Json("Hata", JsonRequestBehavior.AllowGet);
                }

                user.TourismCompany.Title = model.Title != null ? model.Title.Trim() : null;
                user.TourismCompany.CompanyTypeId = model.CompanyTypeId;
                user.TourismCompany.TaxAdministration = model.TaxAdministration != null ? model.TaxAdministration.Trim() : null;
                user.TourismCompany.TaxNo = model.TaxNo != null ? model.TaxNo.Trim() : null;
                user.TourismCompany.FacilityName = model.FacilityName != null ? model.FacilityName.Trim() : null;
                user.TourismCompany.FacilityTypeId = model.FacilityTypeId;
                user.TourismCompany.TourismStatusId = model.TourismStatusId;
                user.TourismCompany.TourismThemaId = model.ThemeId;
                user.TourismCompany.EstablishmentDate = model.EstablishmentDate;
                user.TourismCompany.RenovationDate = model.RenovationDate;
                user.TourismCompany.AreaM2 = model.AreaM2;
                user.TourismCompany.RoomCapacity = model.RoomCapacity;
                user.TourismCompany.BedCapacity = model.BedCapacity;
                user.TourismCompany.DescriptionTurkish = model.DescriptionTurkish != null ? model.DescriptionTurkish.Trim() : null;
                user.TourismCompany.DescriptionEnglish = model.DescriptionEnglish != null ? model.DescriptionEnglish.Trim() : null;
                user.TourismCompany.DescriptionArabic = model.DescriptionArabic != null ? model.DescriptionArabic.Trim() : null;
                MyProfileService.ValidLanguagesUpdate(model.ValidLanguagesIds, user);


                MyProfileService.TourismCompanyUpdate(user);

                return Json("Kaydedildi", JsonRequestBehavior.AllowGet);
            }

            return Json(CustomValidation.ValidationMessageDTO(ModelState), JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult ScopeUpdate(TourismScopesDTO model)
        {
            if(ModelState.IsValid)
            {
                var user = MyProfileService.GetTourismhUser(User.Identity.Name);

                user.TourismCompany.TourismScopes.Clear();

                foreach(var id in model.ScopesIds)
                {
                    user.TourismCompany.TourismScopes.Add(MyProfileService.GetScopes(id));
                }

                user.TourismCompany.TourismRules.Clear();

                foreach(var id in model.TourismRulesIds)
                {
                    user.TourismCompany.TourismRules.Add(MyProfileService.GetRules(id));
                }

                user.TourismCompany.CheckInTime = model.CheckInTime;
                user.TourismCompany.CheckOutTime = model.CheckOutTime;

                MyProfileService.TourismCompanyUpdate(user);

                return Json("Kaydedildi", JsonRequestBehavior.AllowGet);
            }

            return Json(CustomValidation.ValidationMessageDTO(ModelState), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetAllCountryCities(int id)
        {
            var cities = MyProfileService.GetCountryCities(id);
            return Json(cities, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetAllCityesTown(int id)
        {
            var Towns = MyProfileService.GetCityTowns(id);
            return Json(Towns, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult LocationUpdate(TourismLocationDTO model)
        {
            if(ModelState.IsValid)
            {
                var user = MyProfileService.GetTourismhUser(User.Identity.Name);

                user.TourismCompany.CountryId = model.CountryId;
                user.TourismCompany.CityId = model.CityId;
                user.TourismCompany.TownId = model.TownId;
                user.TourismCompany.Address = model.Address != null ? model.Address.Trim() : null;
                user.TourismCompany.ZipCode = model.ZipCode != null ? model.ZipCode.Trim() : null;
                user.TourismCompany.AirportDistance = model.AirportDistance;
                user.TourismCompany.CityCenterDistance = model.CityCenterDistance;
                user.TourismCompany.Latitude = model.Latitude;
                user.TourismCompany.Longitude = model.Longitude;

                MyProfileService.TourismCompanyUpdate(user);

                return Json("Kaydedildi", JsonRequestBehavior.AllowGet);
            }

            return Json(CustomValidation.ValidationMessageDTO(ModelState), JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult ImageSave(string logo, string coverImage)
        {
            if(logo == null && coverImage == null)
                return Json("Farklı bir resim seçin.", JsonRequestBehavior.AllowGet);

            if(MyProfileService.ImageSave(logo, coverImage, MyProfileService.GetTourismhUser(User.Identity.Name)))
            {
                return Json("Kaydedildi", JsonRequestBehavior.AllowGet);
            }
            return Json("Hata oldu tekrar deneyin veya farklı bir resim seçin.", JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GalleyImageSave(string galleryImage)
        {
            if(galleryImage != "")
            {
                var imageId = MyProfileService.GalleyImageSave(galleryImage, MyProfileService.GetTourismhUser(User.Identity.Name));
                if(imageId != 0)
                {
                    var response = new KeyValuePair<int, string>(imageId, "Kaydedildi");
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Hata oldu tekrar deneyin veya farklı bir resim seçin.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GalleyImageDelete(int Id)
        {
            if(MyProfileService.GalleyImageDelete(Id, MyProfileService.GetTourismhUser(User.Identity.Name)))
            {
                return Json("Silindi", JsonRequestBehavior.AllowGet);
            }
            return Json("Hata oldu tekrar deneyin veya farklı bir resim seçin.", JsonRequestBehavior.AllowGet);
        }


        public ActionResult AdvertisementList()
        {
            var AdvertisementList = AdvertisementService.GetAllTourismAdvertisements(User.Identity.Name);
            AdvertisementList.Reverse();
            ViewBag.AdvertisementList = AdvertisementList;
            return View();
        }

        public ActionResult AdvertisementAdd()
        {
            if(AdvertisementService.TourismCompanyMyPfofileCheck(User.Identity.Name))
            {
                TempData["AdverNo"] = "İlan yayınlayabilmek için lütfen profilim sekmesinde istenen tüm bilgileri giriniz.";
                return RedirectToAction("AdvertisementList");
            }

            ViewBag.AccommodationType = new SelectList(AdvertisementService.GetAllTourismAccommodationTypes(), "Id", "Type");
            ViewBag.Amenities = new MultiSelectList(AdvertisementService.GetAllTourismAmenities(), "Id", "Name");
            ViewBag.Security = new MultiSelectList(AdvertisementService.GetAllTourismSecurities(), "Id", "Name");
            ViewBag.Rule = new MultiSelectList(AdvertisementService.GetAllTourismRules(), "Id", "Name");
            ViewBag.PriceType = new SelectList(AdvertisementService.GetAllTourismPriceTypes(), "Id", "Type");
            ViewBag.CurrentUnit = new SelectList(AdvertisementService.GetAllCurrencyUnits(), "Id", "Unit");
            ViewBag.MealTypes = new SelectList(AdvertisementService.GetAllTourismMealTypes(), "Id", "Type");

            return View();
        }

        [HttpPost]
        public ActionResult AdvertisementAdd(TourismAdvertisementDTO model)
        {
            if(ModelState.IsValid)
            {
                var user = AdvertisementService.GetTourismUser(User.Identity.Name);

                TourismAdvertisement tourismAdvertisement = new TourismAdvertisement();

                tourismAdvertisement.AdvertTitle = model.AdvertTitle != null ? model.AdvertTitle.Trim() : null;
                tourismAdvertisement.AdvertTitle_Eng = model.AdvertTitle_Eng != null ? model.AdvertTitle_Eng.Trim() : null;
                tourismAdvertisement.AdvertTitle_Oth = model.AdvertTitle_Oth != null ? model.AdvertTitle_Oth.Trim() : null;
                tourismAdvertisement.TourismAdvertisedAccommodation = model.TourismAdvertisedAccommodation;
                tourismAdvertisement.TourismMealTypeId = model.TourismMealTypeId;
                tourismAdvertisement.TourismAmenities = AdvertisementService.GetTourismAmenities(model.TourismAmenitiesIds);
                tourismAdvertisement.TourismSecurities = AdvertisementService.GetTourismSecurities(model.TourismSecuritiesIds);
                tourismAdvertisement.CoverImage = model.CoverImage;
                tourismAdvertisement.TourismPriceTypeId = model.TourismPriceTypeId;
                tourismAdvertisement.CurrencyUnitId = model.CurrencyUnitId;
                tourismAdvertisement.GuestCount = model.GuestCount;
                tourismAdvertisement.TourismAdvertisedPersonPrices = model.TourismAdvertisedPersonPrices;
                tourismAdvertisement.Bathroom = model.Bathroom;
                tourismAdvertisement.BabyDiscount = model.BabyDiscount;
                tourismAdvertisement.ChildDiscount = model.ChildDiscount;
                tourismAdvertisement.NightPrice = model.NightPrice;
                tourismAdvertisement.CampaignDiscount = model.CampaignDiscount;
                tourismAdvertisement.CampaignStart = model.CampaignStart;
                tourismAdvertisement.CampaignEnd = model.CampaignEnd;
                tourismAdvertisement.Deposit = model.Deposit;
                tourismAdvertisement.CleaningFee = model.CleaningFee;
                tourismAdvertisement.InstantReservation = model.InstantReservation;
                tourismAdvertisement.Prepayment = model.Prepayment;
                tourismAdvertisement.CancellationPolicy = model.CancellationPolicy;
                tourismAdvertisement.CompletionRate = AdvertisementService.TourismAdvertisementCompletionRate(model);

                tourismAdvertisement.AdvertisementStatusId = AdvertisementService.SetStatus(tourismAdvertisement.CompletionRate);

                user.TourismCompany.TourismAdvertisements.Add(tourismAdvertisement);

                AdvertisementService.TourismCompanyUpdate(user);

                return RedirectToAction("AdvertisementList");
            }

            ViewBag.AccommodationType = new SelectList(AdvertisementService.GetAllTourismAccommodationTypes(), "Id", "Type");
            ViewBag.Amenities = new MultiSelectList(AdvertisementService.GetAllTourismAmenities(), "Id", "Name");
            ViewBag.Security = new MultiSelectList(AdvertisementService.GetAllTourismSecurities(), "Id", "Name");
            ViewBag.Rule = new MultiSelectList(AdvertisementService.GetAllTourismRules(), "Id", "Name");
            ViewBag.PriceType = new SelectList(AdvertisementService.GetAllTourismPriceTypes(), "Id", "Type");
            ViewBag.CurrentUnit = new SelectList(AdvertisementService.GetAllCurrencyUnits(), "Id", "Unit");
            ViewBag.MealTypes = new SelectList(AdvertisementService.GetAllTourismMealTypes(), "Id", "Type");


            return View(model);
        }

        public ActionResult AdvertisementUpdate(int id)
        {
            var tourismAdvertisement = AdvertisementService.GetTourismAdvertisement(id);

            if(tourismAdvertisement == null) return RedirectToAction("AdvertisementList");

            var model = new TourismAdvertisementDTO();

            model.Id = tourismAdvertisement.Id;
            model.AdvertTitle = tourismAdvertisement.AdvertTitle;
            model.AdvertTitle_Eng = tourismAdvertisement.AdvertTitle_Eng;
            model.AdvertTitle_Oth = tourismAdvertisement.AdvertTitle_Oth;

            ViewBag.AccommodationType = new SelectList(AdvertisementService.GetAllTourismAccommodationTypes(), "Id", "Type", tourismAdvertisement.TourismAdvertisedAccommodation.TourismAccommodationTypeId);
            ViewBag.RoomType = AdvertisementService.GetAllTourismRoomTypes().Where(x => x.TourismAccommodationTypeId == tourismAdvertisement.TourismAdvertisedAccommodation.TourismAccommodationTypeId).ToList();
            ViewBag.BedType = AdvertisementService.GetAllTourismBedTypes();
            model.TourismAdvertisedAccommodation = tourismAdvertisement.TourismAdvertisedAccommodation;

            ViewBag.MealTypes = new SelectList(AdvertisementService.GetAllTourismMealTypes(), "Id", "Type", tourismAdvertisement.TourismMealTypeId);

            if(tourismAdvertisement.TourismAmenities != null)
            {
                int[] TourismAmenitiesIds = new int[tourismAdvertisement.TourismAmenities.Count()];
                int number = 0;
                foreach(var tourismAmenities in tourismAdvertisement.TourismAmenities)
                {
                    TourismAmenitiesIds[number] = tourismAmenities.Id;
                    number++;
                }

                ViewBag.TourismAmenities = new MultiSelectList(AdvertisementService.GetAllTourismAmenities(), "Id", "Name", TourismAmenitiesIds);
            }
            else
            {
                ViewBag.TourismAmenities = new MultiSelectList(AdvertisementService.GetAllTourismAmenities(), "Id", "Name");
            }

            if(tourismAdvertisement.TourismSecurities != null)
            {
                int[] TourismSecuritiesIds = new int[tourismAdvertisement.TourismSecurities.Count()];
                int number = 0;
                foreach(var tourismSecurity in tourismAdvertisement.TourismSecurities)
                {
                    TourismSecuritiesIds[number] = tourismSecurity.Id;
                    number++;
                }

                ViewBag.TourismSecurities = new MultiSelectList(AdvertisementService.GetAllTourismSecurities(), "Id", "Name", TourismSecuritiesIds);
            }
            else
            {
                ViewBag.TourismSecurities = new MultiSelectList(AdvertisementService.GetAllTourismSecurities(), "Id", "Name");
            }

            model.CoverImage = tourismAdvertisement.CoverImage;

            ViewBag.TourismPriceType = new SelectList(AdvertisementService.GetAllTourismPriceTypes(), "Id", "Type", tourismAdvertisement.TourismPriceTypeId);

            model.GuestCount = tourismAdvertisement.GuestCount;
            model.TourismPriceTypeId = tourismAdvertisement.TourismPriceTypeId;
            model.TourismAdvertisedPersonPrices = tourismAdvertisement.TourismAdvertisedPersonPrices;
            model.BabyDiscount = tourismAdvertisement.BabyDiscount;
            model.ChildDiscount = tourismAdvertisement.ChildDiscount;
            model.NightPrice = tourismAdvertisement.NightPrice;

            ViewBag.CurrencyUnit = new SelectList(AdvertisementService.GetAllCurrencyUnits(), "Id", "Unit", tourismAdvertisement.CurrencyUnitId);

            model.Bathroom = tourismAdvertisement.Bathroom;

            model.CampaignDiscount = tourismAdvertisement.CampaignDiscount;
            model.CampaignStart = tourismAdvertisement.CampaignStart;
            model.CampaignEnd = tourismAdvertisement.CampaignEnd;

            model.Deposit = tourismAdvertisement.Deposit;

            model.CleaningFee = tourismAdvertisement.CleaningFee;

            model.InstantReservation = tourismAdvertisement.InstantReservation;
            model.Prepayment = tourismAdvertisement.Prepayment;
            model.CancellationPolicy = tourismAdvertisement.CancellationPolicy;

            return View(model);
        }

        [HttpPost]
        public ActionResult AdvertisementUpdate(TourismAdvertisementDTO model)
        {
            if(ModelState.IsValid)
            {
                var tourismAdvertisement = AdvertisementService.GetTourismAdvertisement(model.Id);

                if(tourismAdvertisement == null) RedirectToAction("AdvertisementList");

                tourismAdvertisement.AdvertTitle = model.AdvertTitle != null ? model.AdvertTitle.Trim() : null;
                tourismAdvertisement.AdvertTitle_Eng = model.AdvertTitle_Eng != null ? model.AdvertTitle_Eng.Trim() : null;
                tourismAdvertisement.AdvertTitle_Oth = model.AdvertTitle_Oth != null ? model.AdvertTitle_Oth.Trim() : null;
                AdvertisementService.DeleteTourismAdvertisedAccommodation(tourismAdvertisement.TourismAdvertisedAccommodation);
                tourismAdvertisement.TourismAdvertisedAccommodation = model.TourismAdvertisedAccommodation = model.TourismAdvertisedAccommodation;
                tourismAdvertisement.TourismMealTypeId = model.TourismMealTypeId;
                if(tourismAdvertisement.TourismAmenities != null) tourismAdvertisement.TourismAmenities.Clear();
                tourismAdvertisement.TourismAmenities = AdvertisementService.GetTourismAmenities(model.TourismAmenitiesIds);
                if(tourismAdvertisement.TourismSecurities != null) tourismAdvertisement.TourismSecurities.Clear();
                tourismAdvertisement.TourismSecurities = AdvertisementService.GetTourismSecurities(model.TourismSecuritiesIds);
                tourismAdvertisement.CoverImage = AdvertisementService.ImageSave(model.CoverImage, tourismAdvertisement.CoverImage);
                tourismAdvertisement.TourismPriceTypeId = model.TourismPriceTypeId;
                tourismAdvertisement.CurrencyUnitId = model.CurrencyUnitId;
                tourismAdvertisement.GuestCount = model.GuestCount;
                AdvertisementService.DeleteTourismAdvertisedPersonPrices(tourismAdvertisement.Id);
                tourismAdvertisement.TourismAdvertisedPersonPrices = model.TourismAdvertisedPersonPrices;
                tourismAdvertisement.BabyDiscount = model.BabyDiscount;
                tourismAdvertisement.ChildDiscount = model.ChildDiscount;
                tourismAdvertisement.NightPrice = model.NightPrice;
                tourismAdvertisement.Bathroom = model.Bathroom;
                tourismAdvertisement.CampaignDiscount = model.CampaignDiscount;
                tourismAdvertisement.CampaignStart = model.CampaignStart;
                tourismAdvertisement.CampaignEnd = model.CampaignEnd;
                tourismAdvertisement.Deposit = model.Deposit;
                tourismAdvertisement.CleaningFee = model.CleaningFee;
                tourismAdvertisement.InstantReservation = model.InstantReservation;
                tourismAdvertisement.Prepayment = model.Prepayment;
                tourismAdvertisement.CancellationPolicy = model.CancellationPolicy;
                tourismAdvertisement.CompletionRate = AdvertisementService.TourismAdvertisementCompletionRate(model);

                tourismAdvertisement.AdvertisementStatusId = AdvertisementService.SetStatus(tourismAdvertisement.CompletionRate);

                AdvertisementService.TourismAdvertisementUpdate(tourismAdvertisement);

                return RedirectToAction("AdvertisementList");
            }

            return View(model);
        }

        public ActionResult DeleteAdvertisement(int id)
        {
            AdvertisementService.DeleteAdvertisement(id);

            return RedirectToAction("AdvertisementList");
        }

        public JsonResult GetAllBedTypes()
        {
            return Json(AdvertisementService.GetAllTourismBedTypes(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRoomTypes(int id)
        {
            return Json(AdvertisementService.GetTourismRoomTypes(id).Select(x => new { Id = x.Id, Type = x.Type }), JsonRequestBehavior.AllowGet);
        }
    }
}