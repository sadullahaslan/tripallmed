﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using tripallmed.Business;
using tripallmed.Entity;

namespace tripallmed.Areas.TourismCompany.Business
{
    public class MyProfileService : DbInstance
    {
        public TourismmCompany GetTourismCompany(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                return tourismUser.TourismCompany;
            }
            return null;
        }


        public List<CompanyType> GetCompaniesType()
        {
            return db.CompanyTypes.ToList();
        }


        public int? GetCompanyTypeId(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                return tourismUser.TourismCompany.CompanyTypeId;
            }
            return null;
        }


        public List<FacilityType> GetAllFacilityTypes()
        {
            return db.FacilityTypes.ToList();
        }


        public int? GetFacilityTypesId(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                return tourismUser.TourismCompany.FacilityTypeId;
            }
            return null;
        }



        public List<TourismStatus> GetAllTourismStatuses()
        {
            return db.TourismStatuses.ToList();
        }


        public int? GetTourismStatusesId(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                return tourismUser.TourismCompany.TourismStatusId;
            }
            return null;
        }


        public List<TourismThema> GetAllTourismThemas()
        {
            return db.TourismThemas.ToList();
        }


        public int? GetTourismThemaId(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                return tourismUser.TourismCompany.TourismThemaId;
            }
            return null;
        }



        public List<TourismScopes> GetAllTourismScopes()
        {
            return db.TourismScopes.ToList();
        }


        public int[] GetTourismScopesIds(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                int[] ids = new int[tourismUser.TourismCompany.TourismScopes.Count()];
                int number = 0;
                foreach(TourismScopes tourismScopes in tourismUser.TourismCompany.TourismScopes)
                {
                    ids[number] = tourismScopes.Id;
                    number++;
                }
                return ids;
            }
            return null;
        }

        public int[] GetTourismRulesIds(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                int[] ids = new int[tourismUser.TourismCompany.TourismRules.Count()];
                int number = 0;
                foreach(TourismRule tourismRule in tourismUser.TourismCompany.TourismRules)
                {
                    ids[number] = tourismRule.Id;
                    number++;
                }
                return ids;
            }
            return null;
        }


        public List<Country> GetAllCountries()
        {
            return db.Countries.ToList();
        }


        public int? GetCountryId(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                return tourismUser.TourismCompany.CountryId;
            }
            return null;


        }


        public List<City> GetAllCountrieCities(string tourismEmail)
        {
            int? id = GetCountryId(tourismEmail);
            if(id != null)
            {
                return db.Cities.Where(x => x.CountryId == id).ToList();
            }
            return new List<City>();
        }


        public int? GetCityId(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                return tourismUser.TourismCompany.CityId;
            }
            return null;
        }


        public List<Town> GetAllTowns(string tourismEmail)
        {
            int? id = GetCityId(tourismEmail);
            if(id != null)
            {
                return db.Towns.Where(x => x.CityId == id).ToList();
            }
            return new List<Town>();
        }



        public int? GetTownId(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail);
            if(tourismUser != null)
            {
                return tourismUser.TourismCompany.TownId;
            }
            return null;
        }

        public TourismUser GetTourismhUser(string email)
        {
            return db.TourismUsers.FirstOrDefault(x => x.Email == email);
        }


        public bool TourismCompanyUpdate(TourismUser tourismUser)
        {
            if(tourismUser == null) return false;

            db.TourismUsers.AddOrUpdate(tourismUser);
            return db.SaveChanges() > 0;
        }

        public TourismScopes GetScopes(int id)
        {
            return db.TourismScopes.FirstOrDefault(x => x.Id == id);
        }

        public TourismRule GetRules(int id)
        {
            return db.TourismRules.FirstOrDefault(x => x.Id == id);
        }


        public object GetCountryCities(int id)
        {
            return db.Countries.FirstOrDefault(x => x.Id == id).Cities.Select(x => new
            {
                Id = x.Id,
                CityName = x.CityName
            }).ToList();
        }



        public object GetCityTowns(int id)
        {
            return db.Cities.FirstOrDefault(x => x.Id == id).Towns.Select(x => new
            {
                Id = x.Id,
                TownName = x.TownName
            }).ToList();
        }


        public bool ImageSave(string logo, string coverImage, TourismUser tourismUser)
        {
            if(logo != null && logo != "")
            {
                string pathToDelete = HttpContext.Current.Server.MapPath("~/Data/") + tourismUser.TourismCompany.Logo;
                if(File.Exists(pathToDelete))
                {
                    File.Delete(pathToDelete);
                }
                tourismUser.TourismCompany.Logo = logo;
            }

            if(coverImage != null && coverImage != "")
            {
                string pathToDelete = HttpContext.Current.Server.MapPath("~/Data/") + tourismUser.TourismCompany.CoverImage;
                if(File.Exists(pathToDelete))
                {
                    File.Delete(pathToDelete);
                }
                tourismUser.TourismCompany.CoverImage = coverImage;
            }
            return TourismCompanyUpdate(tourismUser);
        }


        public int GalleyImageSave(string image, TourismUser tourismUser)
        {
            var img = new PictureGallery() { Name = image };
            tourismUser.TourismCompany.PictureGalleries.Add(img);
            TourismCompanyUpdate(tourismUser);
            return img.Id;

        }

        public bool GalleyImageDelete(int Id, TourismUser tourismUser)
        {
            string pathToDelete = HttpContext.Current.Server.MapPath("~/Data/") + tourismUser.TourismCompany.PictureGalleries.FirstOrDefault(x => x.Id == Id).Name;
            if(File.Exists(pathToDelete))
            {
                File.Delete(pathToDelete);
            }

            tourismUser.TourismCompany.PictureGalleries.FirstOrDefault(x => x.Id == Id).IsDeleted = true;
            return TourismCompanyUpdate(tourismUser);
        }


        /// <summary>
        /// Geçerli dillerin tümünü geriye döndüren fonksiyon
        /// </summary>
        /// <returns></returns>
        public List<ValidLanguages> GetAllValidLanguages()
        {
            return db.ValidLanguages.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }


        /// <summary>
        /// Firmanın Geçerli dillerinin idsini geriye döndüren fonksiyon
        /// </summary>
        /// <param name="healthEmail">Kayıtlı kullanıcı email</param>
        /// <returns></returns>
        public int[] GetValidLanguagesIds(string tourismEmail)
        {
            var tourismUser = db.TourismUsers.FirstOrDefault(x => x.Email == tourismEmail && x.IsDeleted == false && x.Active == true);
            if(tourismUser != null)
            {
                int[] ids = new int[tourismUser.TourismCompany.ValidLanguages.Count()];
                int number = 0;
                foreach(ValidLanguages validLanguages in tourismUser.TourismCompany.ValidLanguages)
                {
                    ids[number] = validLanguages.Id;
                    number++;
                }
                return ids;
            }
            return null;
        }


        public void ValidLanguagesUpdate(int[] ids, TourismUser tourismUser)
        {
            tourismUser.TourismCompany.ValidLanguages.Clear();
            for(int i = 0; i < ids.Count(); i++)
            {
                tourismUser.TourismCompany.ValidLanguages.Add(GetValidLanguages(ids[i]));
            }
        }

        public ValidLanguages GetValidLanguages(int id)
        {
            return db.ValidLanguages.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
        }


        public List<TourismRule> GetAllTourismRules()
        {
            return db.TourismRules.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

    }
}