﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using tripallmed.Areas.TourismCompany.Models;
using tripallmed.Business;
using tripallmed.Entity;

namespace tripallmed.Areas.TourismCompany.Business
{
    public class AdvertisementService : DbInstance
    {
        public List<TourismAccommodationType> GetAllTourismAccommodationTypes()
        {
            return db.TourismAccommodationTypes.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<TourismRoomType> GetAllTourismRoomTypes()
        {
            return db.TourismRoomTypes.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<TourismBedType> GetAllTourismBedTypes()
        {
            return db.TourismBedTypes.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<TourismAmenities> GetAllTourismAmenities()
        {
            return db.TourismAmenities.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<TourismSecurity> GetAllTourismSecurities()
        {
            return db.TourismSecurities.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<TourismRule> GetAllTourismRules()
        {
            return db.TourismRules.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<TourismPriceType> GetAllTourismPriceTypes()
        {
            return db.TourismPriceTypes.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public List<CurrencyUnit> GetAllCurrencyUnits()
        {
            return db.CurrencyUnits.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public TourismUser GetTourismUser(string email)
        {
            return db.TourismUsers.FirstOrDefault(x => x.Email == email && x.IsDeleted == false && x.Active == true);
        }

        public List<TourismAmenities> GetTourismAmenities(int[] ids)
        {
            List<TourismAmenities> tourismAmenitieses = new List<TourismAmenities>();
            if(ids != null)
            {
                foreach(var tourismAmenities in db.TourismAmenities.Where(x => x.IsDeleted == false && x.Active == true).ToList())
                {
                    foreach(var id in ids)
                    {
                        if(tourismAmenities.Id == id)
                        {
                            tourismAmenitieses.Add(tourismAmenities);
                        }
                    }
                }
                return tourismAmenitieses;
            }
            return null;
        }

        public List<TourismSecurity> GetTourismSecurities(int[] ids)
        {
            List<TourismSecurity> tourismSecurities = new List<TourismSecurity>();
            if(ids != null)
            {
                foreach(var tourismSecurity in db.TourismSecurities.Where(x => x.IsDeleted == false && x.Active == true).ToList())
                {
                    foreach(var id in ids)
                    {
                        if(tourismSecurity.Id == id)
                        {
                            tourismSecurities.Add(tourismSecurity);
                        }
                    }
                }
                return tourismSecurities;
            }
            return null;
        }

        public bool TourismCompanyUpdate(TourismUser user)
        {
            if(user != null)
            {
                db.TourismUsers.AddOrUpdate(user);
                return db.SaveChanges() > 0;
            }
            return false;
        }

        public List<TourismAdvertisement> GetAllTourismAdvertisements(string email)
        {
            return db.TourismUsers.FirstOrDefault(x => x.Email == email).TourismCompany.TourismAdvertisements.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public bool TourismCompanyMyPfofileCheck(string healthEmail)
        {
            var tourismmCompany = db.TourismUsers.FirstOrDefault(x => x.Email == healthEmail && x.IsDeleted == false && x.Active == true).TourismCompany;

            Type t = tourismmCompany.GetType();

            var properties = t.GetProperties();

            foreach(var prop in properties)
            {
                if(prop.Name == "DescriptionEnglish") continue;
                if(prop.Name == "DescriptionArabic") continue;
                if(prop.Name == "Logo") continue;

                if(prop.PropertyType.Name == "String" || prop.PropertyType.Name == "Nullable`1")
                {
                    var value = prop.GetValue(tourismmCompany, null);

                    if(value == null)
                    {
                        return true;
                    }
                }
            }
            if(tourismmCompany.PictureGalleries.Where(x => x.IsDeleted == false).Count() == 0)
                return true;
            if(tourismmCompany.ValidLanguages.Where(x => x.IsDeleted == false).Count() == 0)
                return true;
            if(tourismmCompany.TourismScopes.Where(x => x.IsDeleted == false).Count() == 0)
                return true;

            return false;
        }

        public TourismAdvertisement GetTourismAdvertisement(int id)
        {
            return db.TourismAdvertisements.FirstOrDefault(x => x.Id == id && x.IsDeleted == false && x.Active == true);
        }

        public bool TourismAdvertisementUpdate(TourismAdvertisement model)
        {
            if(model == null) return false;

            db.TourismAdvertisements.AddOrUpdate(model);
            return db.SaveChanges() > 0;
        }

        public bool DeleteAdvertisement(int id)
        {
            var adver = db.TourismAdvertisements.Find(id);
            if(adver != null)
            {
                adver.IsDeleted = true;
                return db.SaveChanges() > 0;
            }
            return false;
        }

        public List<TourismMealType> GetAllTourismMealTypes()
        {
            return db.TourismMealTypes.Where(x => x.IsDeleted == false && x.Active == true).ToList();
        }

        public short TourismAdvertisementCompletionRate(TourismAdvertisementDTO model)
        {
            Type t = model.GetType();

            var properties = t.GetProperties();

            int b = 0;

            foreach(var prop in properties)
            {
                if(prop.Name == "TourismAdvertisedAccommodation" || prop.Name == "NightPrice" || prop.Name == "TourismAdvertisedPersonPrices" || prop.Name == "CampaignStart" || prop.Name == "CampaignEnd" || prop.Name == "Bathroom") continue;

                var value = prop.GetValue(model, null);
                if(value != null)
                {
                    b++;
                }
            }

            var totalProperties = (properties.Length - 3);//CampaignStart, CampaignEnd, Bathroom çıkartıldı.

            if(!(model.TourismAdvertisedAccommodation.TourismAdvertisedRoomProperties.Where(x => x.TourismAdvertisedBed.Count() == 0).Count() > 0) && model.TourismAdvertisedAccommodation.TourismAdvertisedRoomProperties.Count() > 0) b++;

            if(model.TourismPriceTypeId == 1)
            {
                if(!(model.TourismAdvertisedPersonPrices.Where(x => x.Price != null && x.Price == 0).Count() > 0)) b++;
                totalProperties = totalProperties - 1;
            }
            else
            {
                if(model.NightPrice != null && model.NightPrice != 0) b++;
                totalProperties = totalProperties - 1;
            }


            var rate = ((b * 100) / totalProperties);

            return Convert.ToInt16(rate);

        }

        public List<TourismRoomType> GetTourismRoomTypes(int id)
        {
            return db.TourismRoomTypes.Where(x => x.TourismAccommodationTypeId == id && x.IsDeleted == false && x.Active == true).ToList();
        }

        public string ImageSave(string coverImageNew, string coverImage)
        {
            if(coverImageNew == null || coverImageNew == coverImage) return coverImage;

            string pathToDelete = HttpContext.Current.Server.MapPath("~/Data/") + coverImage;
            if(File.Exists(pathToDelete))
            {
                File.Delete(pathToDelete);
            }

            return coverImageNew;
        }

        public int SetStatus(short rate)
        {
            if(rate == 100) return 1;
            return 2;
        }

        public void DeleteTourismAdvertisedAccommodation(TourismAdvertisedAccommodation model)
        {
            if(db.TourismAdvertisedAccommodations.FirstOrDefault(x => x.Id == model.Id) != null)
            {
                foreach(var tourismAdvertisedBeds in model.TourismAdvertisedRoomProperties.Select(x=>x.TourismAdvertisedBed))
                {
                    db.TourismAdvertisedBeds.RemoveRange(tourismAdvertisedBeds);
                }

                var roomProperties = new List<TourismAdvertisedRoomProperty>();
                roomProperties = model.TourismAdvertisedRoomProperties.ToList();

                foreach(var tourismAdvertisedRoomProperty in roomProperties)
                {
                    db.TourismAdvertisedRoomProperties.Remove(tourismAdvertisedRoomProperty);
                }

                db.TourismAdvertisedAccommodations.Remove(model);

            }
        }

        public void DeleteTourismAdvertisedPersonPrices(int adverId)
        {
            db.TourismAdvertisedPersonPrices.RemoveRange(db.TourismAdvertisedPersonPrices.Where(x => x.TourismAdvertisementId == adverId));
        }
    }
}