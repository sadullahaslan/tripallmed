﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using tripallmed.Entity;

namespace tripallmed.Areas.TourismCompany.Models
{
    public class TourismAdvertisementDTO
    {
        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = "İlan başlığı 100 karakteri geçemez.")]
        public string AdvertTitle { get; set; }


        [MaxLength(100)]
        public string AdvertTitle_Eng { get; set; }


        [MaxLength(100)]
        public string AdvertTitle_Oth { get; set; }


        public TourismAdvertisedAccommodation TourismAdvertisedAccommodation { get; set; }


        public int? TourismMealTypeId { get; set; }


        public int[] TourismAmenitiesIds { get; set; }


        public int[] TourismSecuritiesIds { get; set; }


        [MaxLength(50)]
        public string CoverImage { get; set; }


        public int? TourismPriceTypeId { get; set; }


        public int? CurrencyUnitId { get; set; }


        public short? GuestCount { get; set; }


        public decimal? NightPrice { get; set; }


        public ICollection<TourismAdvertisedPersonPrices> TourismAdvertisedPersonPrices { get; set; }


        public bool BabyDiscount { get; set; }


        public bool ChildDiscount { get; set; }

        public string Bathroom { get; set; }

        public int? CampaignDiscount { get; set; }

        public DateTime? CampaignStart { get; set; }


        public DateTime? CampaignEnd { get; set; }


        public decimal? Deposit { get; set; }


        public decimal? CleaningFee { get; set; }


        public short? InstantReservation { get; set; }


        public short? Prepayment { get; set; }


        [MaxLength(20)]
        public string CancellationPolicy { get; set; }

    }
}