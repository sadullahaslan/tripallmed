﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Areas.TourismCompany.Models
{
    public class TourismScopesDTO
    {
        [Required(ErrorMessage = "Olanak seçiniz.")]
        public int[] ScopesIds { get; set; }


        [Required(ErrorMessage = "Kural seçiniz.")]
        public int[] TourismRulesIds { get; set; }


        [Required(ErrorMessage = "Giriş saati seçiniz.")]
        public string CheckInTime { get; set; }


        [Required(ErrorMessage = "Çıkış saati seçiniz.")]
        public string CheckOutTime { get; set; }

    }
}