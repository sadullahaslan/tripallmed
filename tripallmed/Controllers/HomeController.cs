﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using tripallmed.Business;
using tripallmed.Entity;
using tripallmed.Models.DTO;
using tripallmed.Models.VM;

namespace tripallmed.Controllers
{
    public class HomeController : Controller
    {
        HealthHomeService HealthHomeService = new HealthHomeService();
        TourismHomeService TourismHomeService = new TourismHomeService();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Health()
        {
            return View(HealthHomeService.GetAllBranchHospitals());
        }

        public ActionResult HealthCategory(int id, int? serviceid)
        {
            HealthCategoryVM healthCategoryVM = new HealthCategoryVM();
            healthCategoryVM.Polyclinic = HealthHomeService.GetPolyclinicId(id);
            if(serviceid != null)
            {
                healthCategoryVM.HealthAdvertisements = HealthHomeService.GetCategoryServiceAdvertisements(serviceid);
            }
            else
            {
                healthCategoryVM.HealthAdvertisements = HealthHomeService.GetCategoryAdvertisements(id);
            }
            return View(healthCategoryVM);
        }

        public ActionResult HealthDetail(int id)
        {
            var model = HealthHomeService.GetHealthAdvertisement(id);
            ViewBag.Category = model.MedicalUnit;
            return View(model);
        }

        public ActionResult Tourism()
        {
            var tourismCompanies = TourismHomeService.GetAllTourismmCompanies();
            var model = new List<TourismmCompany>();
            foreach(var tourismmCompany in tourismCompanies)
            {
                var newtourismmCompany = new TourismmCompany()
                {
                    Id = tourismmCompany.Id,
                    TourismUser = tourismmCompany.TourismUser,
                    CompanyTypeId = tourismmCompany.CompanyTypeId,
                    CompanyType = tourismmCompany.CompanyType,
                    Title = tourismmCompany.Title,
                    TaxAdministration = tourismmCompany.TaxAdministration,
                    TaxNo = tourismmCompany.TaxNo,
                    FacilityName = tourismmCompany.FacilityName,
                    FacilityTypeId = tourismmCompany.FacilityTypeId,
                    FacilityType = tourismmCompany.FacilityType,
                    TourismStatusId = tourismmCompany.TourismStatusId,
                    TourismStatus = tourismmCompany.TourismStatus,
                    EstablishmentDate = tourismmCompany.EstablishmentDate,
                    RenovationDate = tourismmCompany.RenovationDate,
                    AreaM2 = tourismmCompany.AreaM2,
                    RoomCapacity = tourismmCompany.RoomCapacity,
                    BedCapacity = tourismmCompany.BedCapacity,
                    TourismThemaId = tourismmCompany.TourismThemaId,
                    TourismThema = tourismmCompany.TourismThema,
                    TourismScopes = tourismmCompany.TourismScopes.Where(t => t.IsDeleted == false && t.Active == true).ToList(),
                    TourismRules = tourismmCompany.TourismRules.Where(t => t.IsDeleted == false && t.Active == true).ToList(),
                    CheckInTime = tourismmCompany.CheckInTime,
                    CheckOutTime = tourismmCompany.CheckOutTime,
                    ValidLanguages = tourismmCompany.ValidLanguages.Where(t => t.IsDeleted == false && t.Active == true).ToList(),
                    DescriptionTurkish = tourismmCompany.DescriptionTurkish,
                    DescriptionEnglish = tourismmCompany.DescriptionEnglish,
                    DescriptionArabic = tourismmCompany.DescriptionArabic,
                    CountryId = tourismmCompany.CountryId,
                    Country = tourismmCompany.Country,
                    CityId = tourismmCompany.CityId,
                    City = tourismmCompany.City,
                    TownId = tourismmCompany.TownId,
                    Town = tourismmCompany.Town,
                    Address = tourismmCompany.Address,
                    ZipCode = tourismmCompany.ZipCode,
                    AirportDistance = tourismmCompany.AirportDistance,
                    CityCenterDistance = tourismmCompany.CityCenterDistance,
                    Latitude = tourismmCompany.Latitude,
                    Longitude = tourismmCompany.Longitude,
                    Logo = tourismmCompany.Logo,
                    CoverImage = tourismmCompany.CoverImage,
                    PictureGalleries = tourismmCompany.PictureGalleries.Where(t => t.IsDeleted == false && t.Active == true).ToList(),
                    TourismAdvertisements = tourismmCompany.TourismAdvertisements.Where(t => t.IsDeleted == false && t.Active == true && t.AdvertisementStatusId == 1).ToList(),
                };
                model.Add(newtourismmCompany);
            }

            ViewBag.Rules = TourismHomeService.GetAllRules();

            return View(model);
        }

        public PartialViewResult _TourismFilter()
        {
            if(TempData["SeachTDO"] != null)
                ViewBag.SearchDTO = TempData["SeachTDO"];

            var tourismAdvertisements = TourismHomeService.GetAllTourismAdvertisements();

            ViewBag.FacilityType = tourismAdvertisements.GroupBy(x => x.TourismmCompany.FacilityType.Type).Select(x => new FilterDTO
            {
                Name = x.Key,
                Count = x.Count()
            }).ToList();

            ViewBag.MealType = tourismAdvertisements.GroupBy(x => x.TourismMealType.Type).Select(x => new FilterDTO
            {
                Name = x.Key,
                Count = x.Count()
            }).ToList();

            ViewBag.CancellationPolicy = tourismAdvertisements.GroupBy(x => x.CancellationPolicy).Select(x => new FilterDTO
            {
                Name = x.Key,
                Count = x.Count()
            }).ToList();

            ViewBag.Thema = tourismAdvertisements.GroupBy(x => x.TourismmCompany.TourismThema.ThemeName).Select(x => new FilterDTO
            {
                Name = x.Key,
                Count = x.Count()
            }).ToList();

            ViewBag.Scopes = TourismHomeService.FilterScopes();

            ViewBag.Amenities = TourismHomeService.GetAllTourismAmenities();

            return PartialView();
        }

        public ActionResult TourismFilterSearch(SeacrhDTO model)
        {
            if(ModelState.IsValid)
            {            
                ViewBag.Rules = TourismHomeService.GetAllRules();

                TempData["SeachTDO"] = model;

                return View(TourismHomeService.SeachFilter(model));
            }
            return RedirectToAction("Tourism");
        }

        public ActionResult Room()
        {
            return View();
        }

        public PartialViewResult _HealthFilter()
        {
            ViewBag.City = new SelectList(HealthHomeService.GetTurkeyCity(), "Id", "CityName");
            ViewBag.Center = new SelectList(HealthHomeService.GetAllHealthStatuses(), "Id", "Status");
            return PartialView();
        }


        [HttpPost]
        public JsonResult GetCityTowns(int id)
        {
            var cities = HealthHomeService.GetAllCityTowns(id);
            return Json(cities, JsonRequestBehavior.AllowGet);
        }

    }
}