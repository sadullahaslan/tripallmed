﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tripallmed.Models.DTO;
using tripallmed.Entity;
using tripallmed.Business;
using System.Web.Security;

namespace tripallmed.Controllers
{
    public class AccountController : Controller
    {
        RegisterService RegisterService = new RegisterService();

        public ActionResult Login()
        {
            return View(new LoginDTO());
        }

        [HttpPost]
        public ActionResult Login(LoginDTO model)
        {
            if(ModelState.IsValid)
            {
                var UserEmail = RegisterService.UserCheckAndGetEmail(model.Email, model.Password);

                if(UserEmail.Value == null)
                {
                    ViewBag.LoginError = 1;
                    return View(model);
                }

                FormsAuthentication.SetAuthCookie(UserEmail.Value, model.RememberMe);

                if(UserEmail.Key == "user") return RedirectToAction("Home", "Index");
                if(UserEmail.Key == "health") return RedirectToAction("MyProfile", "Panel", new { area = "HealthCompany" });
                if(UserEmail.Key == "tourism") return RedirectToAction("MyProfile", "Panel", new { area = "TourismCompany" });
            }
            return View(model);
        }


        public ActionResult Register()
        {
            return View(new RegisterDTO());
        }


        [HttpPost]
        public ActionResult Register(RegisterDTO model)
        {
            if(ModelState.IsValid)
            {
                if(RegisterService.EmailCheck(model.Email.Trim()))
                {
                    ModelState.AddModelError("Email", "Bu email ile kayıt olunmuş.");
                    return View(model);
                }

                if(model.Sector == "health")
                {
                    HealthUser newUser = new HealthUser() { HealthcareCompany = new HealthcareCompany() };

                    newUser.Name = model.Name.Trim();
                    newUser.Surname = model.Surname.Trim();
                    newUser.Email = model.Email.Trim();
                    newUser.Phone = model.Phone.Trim();
                    newUser.Password = model.Password.Trim();
                    newUser.MembershipAgreement = model.MembershipAgreement;
                    newUser.ShareInformation = model.ShareInformation;
                    newUser.Role = "health";

                    RegisterService.HealthRegisterNewMember(newUser);

                    FormsAuthentication.SetAuthCookie(newUser.Email, false);

                    return RedirectToAction("MyProfile", "Panel", new { area = "HealthCompany" });
                }

                if(model.Sector == "tourism")
                {
                    TourismUser newUser = new TourismUser() { TourismCompany = new TourismmCompany() };

                    newUser.Name = model.Name.Trim();
                    newUser.Surname = model.Surname.Trim();
                    newUser.Email = model.Email.Trim();
                    newUser.Phone = model.Phone.Trim();
                    newUser.Password = model.Password.Trim();
                    newUser.MembershipAgreement = model.MembershipAgreement;
                    newUser.ShareInformation = model.ShareInformation;
                    newUser.Role = "tourism";

                    RegisterService.TourismRegisterNewMember(newUser);

                    FormsAuthentication.SetAuthCookie(newUser.Email, false);

                    return RedirectToAction("MyProfile", "Panel", new { area = "TourismCompany" });
                }
            }

            return View(model);
        }


        public ActionResult ResetPassword()
        {
            return View();
        }


        public ActionResult _LoginButton()
        {

            ViewBag.UserName = RegisterService.GetUserName(User.Identity.Name);

            return PartialView();
        }
    }
}