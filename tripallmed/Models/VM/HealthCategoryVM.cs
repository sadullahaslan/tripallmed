﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tripallmed.Entity;

namespace tripallmed.Models.VM
{
    public class HealthCategoryVM
    {
        public Polyclinic Polyclinic { get; set; }
        public List<HealthAdvertisement> HealthAdvertisements { get; set; }
    }
}