﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Models.DTO
{
    public class RegisterDTO
    {
        [Required(ErrorMessage = "İsim boş olamaz.")]
        [MaxLength(50, ErrorMessage = "İsim 50 karakteri geçemez.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Soyad boş olamaz.")]
        [MaxLength(50, ErrorMessage = "Soyad 50 karakteri geçemez.")]
        public string Surname { get; set; }

        [EmailAddress(ErrorMessage = "Mail adresi uygun değil.")]
        [Required(ErrorMessage = "Email boş olamaz.")]
        [MaxLength(100, ErrorMessage = "Email 100 karakteri geçemez.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Telefon no boş olamaz.")]
        [MaxLength(15, ErrorMessage = "Telefon no 15 karakteri geçemez.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Sektörünüzü seçin.")]
        public string Sector { get; set; }

        [Required(ErrorMessage = "Parola boş olamaz.")]
        [MinLength(5, ErrorMessage = "Parola 6 karakterden az olamaz.")]
        [MaxLength(100, ErrorMessage = "Parola 100 karakteri geçemez.")]
        public string Password { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Lütfen üyelik sözleşmesini işaretleyin.")]
        public bool MembershipAgreement { get; set; }

        public bool ShareInformation { get; set; }

    }
}