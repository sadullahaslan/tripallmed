﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripallmed.Models.DTO
{
    public class FilterDTO
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}