﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Models.DTO
{
    public class LoginDTO
    {
        [EmailAddress(ErrorMessage = "Mail adresi uygun değil.")]
        [Required(ErrorMessage = "Email boş olamaz.")]
        [MaxLength(100, ErrorMessage = "Email 100 karakteri geçemez.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Parola boş olamaz.")]
        [MaxLength(100, ErrorMessage = "Parola 100 karakteri geçemez.")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }

    }
}