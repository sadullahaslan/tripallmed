﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripallmed.Models.DTO
{
    public class ValidationMessageDTO
    {
        public ValidationMessageDTO()
        {
            Keys = new List<string>();
        }
        public string Message { get; set; }
        public List<string> Keys { get; set; }
    }
}