﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using tripallmed.Entity;

namespace tripallmed.Models.DTO
{
    public class TourismCompanyDTO
    {
        public TourismCompanyDTO()
        {
            TourismScopes = new List<TourismScopes>();
            PictureGalleries = new List<PictureGallery>();
            TourismAdvertisements = new List<TourismAdvertisement>();
            ValidLanguages = new List<ValidLanguages>();
            TourismRules = new List<TourismRule>();
        }


        public int Id { get; set; }
        public  TourismUser TourismUser { get; set; }

        public int? CompanyTypeId { get; set; }
        public  CompanyType CompanyType { get; set; }


        [MaxLength(150, ErrorMessage = "Şirket ünvanı 150 karakteri geçemez.")]
        public string Title { get; set; }


        [MaxLength(150, ErrorMessage = "Vergi dairesi 150 karakteri geçemez.")]
        public string TaxAdministration { get; set; }


        [MaxLength(100, ErrorMessage = "Vergi no 100 karakteri geçemez.")]
        public string TaxNo { get; set; }


        [MaxLength(200, ErrorMessage = "Tesis adı 200 karakteri geçemez.")]
        public string FacilityName { get; set; }

        //public int? PreferredLanguageId { get; set; }
        //public int? CurrencyUnitId { get; set; }

        public int? FacilityTypeId { get; set; }
        public  FacilityType FacilityType { get; set; }


        public int? TourismStatusId { get; set; }
        public  TourismStatus TourismStatus { get; set; }


        public DateTime? EstablishmentDate { get; set; }


        public DateTime? RenovationDate { get; set; }


        public short? AreaM2 { get; set; }


        public short? RoomCapacity { get; set; }


        public short? BedCapacity { get; set; }


        public int? TourismThemaId { get; set; }
        public  TourismThema TourismThema { get; set; }


        public  List<TourismScopes> TourismScopes { get; set; }


        public  List<TourismRule> TourismRules { get; set; }


        [MaxLength(10)]
        public string CheckInTime { get; set; }



        [MaxLength(10)]
        public string CheckOutTime { get; set; }


        public  List<ValidLanguages> ValidLanguages { get; set; }


        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionTurkish { get; set; }


        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionEnglish { get; set; }


        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionArabic { get; set; }


        public int? CountryId { get; set; }
        public  Country Country { get; set; }


        public int? CityId { get; set; }
        public  City City { get; set; }


        public int? TownId { get; set; }
        public  Town Town { get; set; }


        [MaxLength(300, ErrorMessage = "Adres 300 karakteri geçemez.")]
        public string Address { get; set; }



        [MaxLength(50, ErrorMessage = "Posta kodu 50 karakteri geçemez.")]
        public string ZipCode { get; set; }


        public short? AirportDistance { get; set; }


        public short? CityCenterDistance { get; set; }

        [MaxLength(50)]
        public string Latitude { get; set; }

        [MaxLength(50)]
        public string Longitude { get; set; }


        [MaxLength(500, ErrorMessage = "Logo adı 500 karakteri geçemez.")]
        public string Logo { get; set; }



        [MaxLength(500, ErrorMessage = "Kapak resmi adı 500 karakteri geçemez.")]
        public string CoverImage { get; set; }


        public  List<PictureGallery> PictureGalleries { get; set; }

        public  List<TourismAdvertisement> TourismAdvertisements { get; set; }

    }
}