﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Models.DTO
{
    public class SeacrhDTO
    {
        [Required]
        public string Text { get; set; }
        [Required]
        public string CheckIn { get; set; }
        [Required]
        public string CheckOut { get; set; }
        [Required]
        public short AdultCount { get; set; }
        [Required]
        public short ChildCount { get; set; }
        public short? ChildAge_1 { get; set; }
        public short? ChildAge_2 { get; set; }
        public short? ChildAge_3 { get; set; }
        public short? ChildAge_4 { get; set; }
        public short? ChildAge_5 { get; set; }
        public short? ChildAge_6 { get; set; }
        public short? ChildAge_7 { get; set; }
        public short? ChildAge_8 { get; set; }
        public short? ChildAge_9 { get; set; }
        public short? ChildAge_10 { get; set; }
        [Required]
        public short RoomCount { get; set; }

        public int Day { get; set; }
    }
}