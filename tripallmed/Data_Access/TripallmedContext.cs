﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using tripallmed.Entity;

namespace tripallmed.Data_Access
{
    public class TripallmedContext : DbContext
    {
        public TripallmedContext() : base("Data Source=89.19.29.231;Initial Catalog=u6614476_TripAll; User Id=u6614476_Trip5F3;Password=VThp81R9HKkv90K;Pooling=True;Max Pool Size=100;MultipleActiveResultSets=True")
        {

        }

        public DbSet<AdminUser> AdminUsers { get; set; }
        public DbSet<BranchHospital> BranchHospitals { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<CompanyType> CompanyTypes { get; set; }
        public DbSet<ContractedInstitution> ContractedInstitutions { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<CurrencyUnit> CurrencyUnits { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<DoctorTitle> DoctorTitles { get; set; }
        public DbSet<HealthcareCompany> HealthcareCompanies { get; set; }
        public DbSet<HealthCenterType> HealthCenterTypes { get; set; }
        public DbSet<HealthStatus> HealthStatuses { get; set; }
        public DbSet<HealthUser> HealthUsers { get; set; }
        public DbSet<PictureGallery> PictureGalleries { get; set; }
        public DbSet<Polyclinic> Polyclinics { get; set; }
        public DbSet<PolyclinicService> PolyclinicServices { get; set; }
        public DbSet<PreferredLanguage> PreferredLanguages { get; set; }
        public DbSet<TourismUser> TourismUsers { get; set; }
        public DbSet<Town> Towns { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ValidLanguages> ValidLanguages { get; set; }
        public DbSet<HealthAdvertisement> HealthAdvertisements { get; set; }
        public DbSet<IncludedPrice> IncludedPrices { get; set; }
        public DbSet<AdvertisementStatus> AdvertisementStatuses { get; set; }
        public DbSet<TourismmCompany> TourismCompanies { get; set; }
        public DbSet<TourismScopes> TourismScopes { get; set; }
        public DbSet<TourismThema> TourismThemas { get; set; }
        public DbSet<TourismStatus> TourismStatuses { get; set; }
        public DbSet<FacilityType> FacilityTypes { get; set; }
        public DbSet<TourismAdvertisement> TourismAdvertisements { get; set; }
        public DbSet<TourismAmenities> TourismAmenities { get; set; }
        public DbSet<TourismSecurity> TourismSecurities { get; set; }
        public DbSet<TourismRule> TourismRules { get; set; }
        public DbSet<TourismPriceType> TourismPriceTypes { get; set; }
        public DbSet<TourismRoomType> TourismRoomTypes { get; set; }
        public DbSet<TourismBedType> TourismBedTypes { get; set; }
        public DbSet<TourismMealType> TourismMealTypes { get; set; }
        public DbSet<TourismAccommodationType> TourismAccommodationTypes { get; set; }
        public DbSet<TourismAdvertisedRoomProperty> TourismAdvertisedRoomProperties { get; set; }
        public DbSet<TourismAdvertisedBed> TourismAdvertisedBeds { get; set; }
        public DbSet<TourismAdvertisedAccommodation> TourismAdvertisedAccommodations { get; set; }
        public DbSet<TourismAdvertisedPersonPrices> TourismAdvertisedPersonPrices { get; set; }
    }
}