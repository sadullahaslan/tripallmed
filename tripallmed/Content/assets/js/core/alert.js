﻿function confirmDeleteItem(callback, parameters) {
    Swal.fire({
        title: "Silme işlemi",
        text: "Silme işlemini Onaylıyor musunuz?",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "İptal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Evet",
        closeOnConfirm: true
    }).then((result) => {
        if (result.value) {
            callback(parameters);
        }
    });
}

function alertSuccess(title, message, callback) {
    Swal.fire({
        title: title,
        text: message,
        type: "success",
        showCancelButton: false,
        cancelButtonText: "İptal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Tamam",
        closeOnConfirm: true
    }).then((result) => {
        if (result.value) {
            if (callback) {
                callback();
            }
        }
    });
}

function alertWarning(title, message, callback) {
    Swal.fire({
        title: title,
        text: message,
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "İptal",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Evet",
        closeOnConfirm: true
    }).then((result) => {
        if (result.value) {
            if (callback) {
                callback();
            }
        }
    });
}

function alertFail(title, message) {
    Swal.fire(title, message, "error");
}

function alertSaveItem(callback) {
    Swal.fire({
        title: "Ekleme/Güncelleme İşlemi!",
        text: "İşleminiz başarılı tamamlanmıştır.",
        type: "success",
        timer: 2000
    }).then((result) => {
        if (result.value) {
            callback();
        }
    });
}

function alertSaveItemCallback(callback, parameters) {
    Swal.fire({
        title: "Ekleme/Güncelleme İşlemi!",
        text: "İşleminiz başarılı tamamlanmıştır.",
        type: "success",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Tamam",
    }).then((result) => {
        if (result.value) {
            callback(parameters);
        }
    });
}

function alertDeleteItem(callback) {
    setTimeout(function () {
        Swal.fire({
            title: "Silme İşlemi!",
            text: "İşleminiz başarılı tamamlanmıştır.",
            type: "success",
        }).then((result) => {
            if (result.value) {
                callback();
            }
        });
    }, 500);
}

function alertAddItem() {
    setTimeout(function () { Swal.fire("Ekleme İşlemi", "İşleminiz başarılı tamamlanmıştır", "success"); }, 500);
}

function alertEditItem() {
    setTimeout(function () { Swal.fire("Güncelleme İşlemi", "İşleminiz başarılı tamamlanmıştır", "success"); }, 500);
}

function alertValidationFail(field) {
    Swal.fire("Hata", "'" + field + "'" + " alanını kontrol ediniz!", "error");
}

function alertLoginOperation() {
    Swal.fire("Başarısız Login", "Kullanıcı adı ve şifrenizi kontrol ediniz!", "error");
}