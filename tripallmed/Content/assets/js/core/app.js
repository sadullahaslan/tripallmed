﻿var activeRequestCount = 0;
var CACHE_TIME = 10; // in minute

function Application() { // constructor function
    this.localizationManager = new LocalizationManager();  // Public variable 
    this.currencyManager = new CurrencyManager();
    this.serviceManager = new ServiceManager
    this.localStorageManager = new LocalStorageManager();
}

//ServiceManager
function ServiceManager() {
    this.callService = function (url, data, success_handler, fail_handler, synch) {
        var asynch = true;
        if (synch) {
            asynch = false;
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: url,
            data: data,
            processData: true,
            dataType: "json",
            async: asynch,
            beforeSend: function () {
                showLoading();
            },
            complete: function () {
                controlLoading();
            },
            success: function (response) {
                var data = eval(response.d);
                success_handler(data);
            },
            error: function (errormsg) {
                fail_handler(errormsg);
            }
        });
    }

    this.callServiceWithoutEval = function (url, data, success_handler, fail_handler, synch) {
        var asynch = true;
        if (synch) {
            asynch = false;
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: url,
            data: data,
            processData: true,
            dataType: "json",
            async: asynch,
            beforeSend: function () {
                showLoading();
            },
            complete: function () {
                controlLoading();
            },
            success: function (response) {
                var data = (response.d);
                success_handler(data);
            },
            error: function (errormsg) {
                fail_handler(errormsg);
            }
        });
    }

    this.uploadImage = function (input) {
        var formData = new FormData();
        formData.append('file1', input);
        var savedFileName = "";
        $.ajax({
            type: 'POST',
            url: '/Service/ImageUploadHandler.ashx',
            data: formData,
            success: function (fileName) {
                savedFileName = fileName;
            },
            processData: false,
            contentType: false,
            error: function (error) {
                savedFileName = "";
            },
            async: false
        });
        return savedFileName;
    }

    this.uploadFile = function (input) {
        var formData = new FormData();
        formData.append('file1', input);
        var savedFileName = "";
        $.ajax({
            type: 'POST',
            url: '/Service/FileUploadHandler.ashx',
            data: formData,
            success: function (fileName) {
                savedFileName = fileName;
            },
            processData: false,
            contentType: false,
            error: function (error) {
                savedFileName = "";
            },
            async: false
        });
        return savedFileName;
    }

    this.downloadFile = function (filePath) {
        var file_path = filePath;
        var a = document.createElement('A');
        a.href = file_path;
        a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }
}

//CurrencyManager
function CurrencyManager() {
    this.getMoney = function (money, currency) {
        if (currency == "USD" || currency == 1) {
            return "$ " + money.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
        } else if (currency == "EUR" || currency == 2) {
            return "€ " + money.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
        } else if (currency == "PND" || currency == 3) {
            return "£ " + money.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
        } else {
            return money.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,") + " TL";
        }
    }
}

//LocalizationManager
function LocalizationManager() {
    var that = this;
    this.localizationMap;
    this.initLocalization = function (success_handler) {
        var lang = "tr";
        if (localStorage.getItem("lang") != null) {
            lang = localStorage.getItem("lang");
        }

        i18n.init({
            "lng": lang,
            "resStore": resources,
            "fallbackLng": 'en'
        }, function (t) {
            that.localizationMap = t;
            success_handler(t);
        });
    };

    this.setLocalization = function (lang, success_handler) {
        localStorage.setItem("lang", lang);
        i18n.init({
            "lng": lang
        }, function (t) {
            that.localizationMap = t;
            success_handler(t);
        });
    };

    this.getLocalization = function () {
        if (localStorage.getItem("lang") == null || localStorage.getItem("lang") == undefined) {
            localStorage.setItem("lang", "tr");
        }
        return localStorage.getItem("lang");
    };

    this.getLocaliziedValue = function (key) {
        return that.localizationMap(key);
    };

    this.isLocalizationTr = function () {
        if (localStorage.getItem("lang") == null || localStorage.getItem("lang") == undefined || localStorage.getItem("lang") == 'tr') {
            return true;
        }
        return false;
    };

    this.isLocalizationEn = function () {
        if (localStorage.getItem("lang") == 'en') {
            return true;
        }
        return false;
    };
}

//LocalStorageManager
function LocalStorageManager() {
    this.save = function (key, jsonData, expirationMinute) {
        if (typeof (Storage) == "undefined") { return false; }
        if (expirationMinute == null || expirationMinute == "undefined" || expirationMinute == 0) { expirationMinute = CACHE_TIME; }
        var expirationMS = expirationMinute * 60 * 1000;
        var record = { value: JSON.stringify(jsonData), timestamp: new Date().getTime() + expirationMS }
        localStorage.setItem(key, JSON.stringify(record));
        return jsonData;
    }

    this.load = function (key) {
        if (typeof (Storage) == "undefined") { return false; }
        var record = JSON.parse(localStorage.getItem(key));
        if (!record) { return false; }
        return (new Date().getTime() < record.timestamp && JSON.parse(record.value));
    }
}

function localizePage() {
    $(document).i18n();
}

function controlLoading() {
    activeRequestCount--;
    if (activeRequestCount == 0) {
        setTimeout(function () { $('#loading').hide(); }, 500)
    }
}

function showLoading() {
    activeRequestCount++;
    $('#loading').show();
}
