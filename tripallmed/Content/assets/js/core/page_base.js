﻿var app = new Application();
var prevRow = 0;
var nextRow = 0;
var table;
var columns;
var buttons;
var tableData;

$(document).ready(function () {

    initBase();

    try { initpage(); } catch (exc) { }

    try { Validator.init(); } catch (exc) { }

    try { afterPageLoad(); } catch (exc) { }

    try { eventListener(); } catch (exc) { }

    /*ACTIONS*/
    $('.lang_tr').on('click', function (e) {
        e.preventDefault();
        setLocalization("tr", function () {
            localizationChanged("tr");
        });
    });

    $('.lang_eng').on('click', function (e) {
        e.preventDefault();
        setLocalization("en", function () {
            localizationChanged("en");
        });
    });

    $('#_form_action_cancel').on('click', function (e) {
        e.preventDefault();
        var actionHandler = $(this).attr("action-handler");
        if (actionHandler == null || actionHandler == undefined) {
            $('#_modalForm').modal('hide');
        } else {
            try { window[actionHandler](); } catch (exc) { }
        }

    });

    $('#_form_action_clear').on('click', function (e) {
        e.preventDefault();
        var actionHandler = $(this).attr("action-handler");
        if (actionHandler == null || actionHandler == undefined) {
            clearAndHideForm();
        } else {
            try { window[actionHandler](); } catch (exc) { }
        }
    });

});

function initBase() {
    app.localizationManager.initLocalization(localizePage);
    initPageName();
    addTableEventListener();
    initLocalization();
    //initUser();
}

function initPageName() {
    try {
        if (pageNameKey != undefined && pageNameKey != null) {
            var pageName = app.localizationManager.getLocaliziedValue(pageNameKey);
            $('#_pageName').text(pageName);
        }
    } catch (ex) { }
}

function initUser() {
    var admin = window.localStorage.getItem('admin');
    if (admin != null && admin != undefined) {
        $("#_admin_name").html(admin.name.toUpperCase() + " " + admin.surname.toUpperCase());
    }
}

function addTableEventListener() {

    $(".datatable").on("click", ".table-actions", function (e) {
        e.preventDefault();
        var action = "tableActionHandler";
        var actionType = $(this).attr("action-handler");
        try { window[action](actionType); } catch (exc) { }
    });

    $("body").on("click", ".table-row-action", function (e) {
        e.preventDefault();
        var actionType = $(this).attr("action-type");

        if (actionType == "delete") {
            var callbackHandler = $(this).attr("callback-handler");
            var actionParameter = $(this).attr("action-parameter");
            var actionHandler = $(this).attr("action-handler");
            if (actionHandler == null || actionHandler == undefined) {
                confirmDeleteItem(function (actionParameter) {
                    var selectedService = serviceName;
                    var selectedFunction = 'DeleteModel';
                    var data = JSON.stringify({ ID: actionParameter });
                    callService(selectedService, selectedFunction, data, function (response) {
                        try {
                            alertDeleteItem(function () {
                                loadData();
                                if (callbackHandler != null && callbackHandler != undefined) {
                                    window[callbackHandler](actionParameter);
                                }
                            });
                        } catch (exc) { }
                    }, function (errormsg) { console.log(errormsg); }, false);
                }, actionParameter);
            } else {
                window[actionHandler](actionParameter);
            }

        }
        else if (actionType == "edit") {
            var actionHandler = $(this).attr("action-handler");
            var actionParameter = $(this).attr("action-parameter");
            var selectedService = serviceName;
            var selectedFunction = 'LoadModel';
            var data = JSON.stringify({ ID: actionParameter });
            callService(selectedService, selectedFunction, data, function (response) {
                try { window[actionHandler](response.response); } catch (exc) { }
            }, function (errormsg) { console.log(errormsg); }, false);
        }
        else if (actionType == "up") {
            if (selectedRow > 0 && prevRow > 0) {
                var callbackHandler = $(this).attr("callback-handler");
                var actionParameter = $(this).attr("action-parameter");
                var selectedService = serviceName;
                var selectedFunction = 'ChangeModel';
                var data = JSON.stringify({ ID: selectedRow, ID2: prevRow });
                callService(selectedService, selectedFunction, data, function (response) {
                    try { window[callbackHandler](actionParameter); } catch (exc) { }
                }, function (errormsg) { console.log(errormsg); }, false);
            }
        }
        else if (actionType == "down") {
            if (selectedRow > 0 && nextRow > 0) {
                var callbackHandler = $(this).attr("callback-handler");
                var actionParameter = $(this).attr("action-parameter");
                var selectedService = serviceName;
                var selectedFunction = 'ChangeModel';
                var data = JSON.stringify({ ID: selectedRow, ID2: nextRow });
                callService(selectedService, selectedFunction, data, function (response) {
                    try { window[callbackHandler](actionParameter); } catch (exc) { }
                }, function (errormsg) { console.log(errormsg); }, false);
            }
        }
        else {
            var actionHandler = $(this).attr("action-handler");
            var actionParameter = $(this).attr("action-parameter");
            try { window[actionHandler](actionParameter); } catch (exc) { }
        }
    });

    $(".datatable").on("click", ".table-active-action", function (e) {
        e.preventDefault();
        var callbackHandler = $(this).attr("callback-handler");
        var actionParameter = $(this).attr("action-parameter");
        var actionHandler = $(this).attr("action-handler");
        if (actionHandler == null || actionHandler == undefined) {
            if (serviceName != null && serviceName != undefined) {
                var selectedService = serviceName;
                var selectedFunction = 'ActiveModel';
                var data = JSON.stringify({ ID: actionParameter });
                callService(selectedService, selectedFunction, data, function () {
                    try { window[callbackHandler](); } catch (exc) { }
                }, function (errormsg) { console.log(errormsg); }, false);
            }
        }
        else {
            window[actionHandler](actionParameter);
        }
    });

    $(".datatable").on("click", ".table-row-action-menu", function (e) {
        e.preventDefault();
        var actionParameter = $(this).attr("action-parameter");
        try {
            selectedRow = actionParameter;

            var closestRow = $(this).closest('tr');
            var prev = closestRow.prev();
            if (prev.html() != undefined) {
                var data = table.row(prev).data();
                prevRow = data.ID;
            } else {
                prevRow = 0;
            }

            var next = closestRow.next();
            if (next.html() != undefined) {
                var data = table.row(next).data();
                nextRow = data.ID;
            } else {
                nextRow = 0;
            }

            console.log("selectedRow: " + selectedRow + " nextRow: " + nextRow + " prevRow: " + prevRow)
        } catch (exc) { }
        //e.stopPropagation();
    });

}


function setLocalization(lang, success_handler) {
    app.localizationManager.setLocalization(lang, success_handler);
}

function initLocalization() {
    var path = "";
    if (app.localizationManager.isLocalizationTr()) {
        path = "/Panel/assets/media/flags/006-turkey.svg";
    } else {
        path = "/Panel/assets/media/flags/012-uk.svg";
    }

    $('#_selectedLanguage').attr("src", path);
}

function localizationChanged(lang) {
    window.location.reload();
    /*
    var url = '/Service/MenuService.svc/ChangeClientSideLang';
    var data = JSON.stringify({ lang: lang });
    app.serviceManager.callService(url, data, function (data) {
        console.log("lang is changed to " + data);
        window.location.reload();
    }, function (errormsg) {
        console.log("fail");
    }, true);
    */
}

function getImagePath(image) {
    if (image == null || image == undefined || image == "")
        return "/Data/nocontent.png";
    else
        return "/Data/" + image;
}

function uploadImage(inputName) {
    var image = "";
    try {
        if ($('#' + inputName)[0].files && $('#' + inputName)[0].files[0]) {
            image = app.serviceManager.uploadImage($('#' + inputName)[0].files[0]);
        } else {
            var bg = $('#' + inputName).parent().parent().find("div").css('background-image');
            bg = bg.replace('url(', '').replace(')', '').replace(/\"/gi, "");
            var res = bg.split("Data/");
            bg = res[1];

            image = bg;
            if (image == undefined) {
                image = "";
            }
        }
    } catch (ex) {
        image = "";
    }

    return image;
}

function uploadFile(inputName) {
    var image = "";
    if ($('#' + inputName)[0].files && $('#' + inputName)[0].files[0]) {
        image = app.serviceManager.uploadFile($('#' + inputName)[0].files[0]);
    } else {
        image = $('#' + inputName + '_val').attr("src").split("/")[2];
    }
    return image;
}

function downloadFile(filePath) {
    app.serviceManager.downloadFile(filePath);
}

function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getParameterSeo(url) {
    if (!url) {
        url = window.location.href;
    }
    var links = url.split('/');
    if (links != null && links.length > 0) {
        return links[links.length - 1];
    }
    return null;
}

function clearSeo(element) {
    var charMap = { Ç: 'c', Ö: 'o', Ş: 's', İ: 'i', I: 'i', Ü: 'u', Ğ: 'g', ç: 'c', ö: 'o', ş: 's', ı: 'i', ü: 'u', ğ: 'g' };
    var str = $(element).val();
    str_array = str.split('');

    for (var i = 0, len = str_array.length; i < len; i++) {
        str_array[i] = charMap[str_array[i]] || str_array[i];
    }

    str = str_array.join('');
    var clearStr = str.replace("&", "-").replace(",", "").replace(".", "").replace(" ", "-").replace("--", "-").replace("/", "").replace(/'/g, "").toLowerCase();
    $(element).val(clearStr);
    return clearStr;
}

function validateSeo(element, val) {
    var str = $(element).val();
    if (val == str) {
        return true;
    }
    var result = false;
    var data = JSON.stringify({ seo: str });
    callService(serviceName, 'CheckSeo', data, function (response) {
        if (response.responseCode == 200) {
            if (response.response == true) {
                result = true;
            } else {
                result = false;
            }
        } else if (response.responseCode == 500) {
            result = false;
        }
    }, function (errormsg) { console.log(errormsg); }, true);

    setTimeout(function () {
        if (result == false) {
            if (!$(element).hasClass("is-invalid")) {
                $(element).addClass("is-invalid")
                var error = `<div id="_name_seo-error" class="error invalid-feedback">invalid seo value</div>`;
                $(element).after(error);
            } else {
                $(element).next().html("invalid seo value");
                $(element).next().show();
            }
        } else {
            console.log("seo is validated.");
        }
    }, 100);

    return result;
}

function getItemsAsynch(selectedService, parameters, success_handler, fail_handler, selectedFunction) {
    getItems(selectedService, parameters, success_handler, fail_handler, selectedFunction, false);
}

function getItemsSynch(selectedService, parameters, success_handler, fail_handler, selectedFunction) {
    getItems(selectedService, parameters, success_handler, fail_handler, selectedFunction, true);
}

function getItems(selectedService, parameters, success_handler, fail_handler, selectedFunction, synch) {

    if (selectedFunction == undefined && selectedFunction == null) {
        selectedFunction = "ListModel";
    }

    var paramater = {};
    if (parameters != null) {
        paramater = parameters;
    }

    var items = {};
    var url = '/Service/' + selectedService + '.asmx/' + selectedFunction;
    app.serviceManager.callService(url, paramater, function (data) {
        items = data;
        success_handler(items);
    }, function (errormsg) {
        fail_handler(errormsg);
    }, synch);
}

function createSiteMap() {
    var paramater = {};
    var items = {};
    var url = '/Service/GeneralSettingService.svc/CreateSiteMap';
    app.serviceManager.callService(url, paramater, function (data) {
        console.log("başarılı");
    }, function (errormsg) {
        console.log("fail");
    }, true);
    return items;
}

function getLocalizedItem(trVal, engVal, thirdVal) {
    if (app.localizationManager.isLocalizationTr()) {
        return trVal;
    } else if (app.localizationManager.isLocalizationEn()) {
        return engVal;
    } else {
        return thirdVal;
    }
}

function getLocaliziedValue(key) {
    return app.localizationManager.getLocaliziedValue(key);
}

function callService(selectedService, selectedFunction, data, success_handler, fail_handler, synch) {
    var url = '/Service/' + selectedService + '.asmx/' + selectedFunction;
    app.serviceManager.callService(url, data, success_handler, fail_handler, synch);
}

function callServiceWithoutEval(selectedService, selectedFunction, data, success_handler, fail_handler, synch) {
    var url = '/Service/' + selectedService + '.asmx/' + selectedFunction;
    app.serviceManager.callServiceWithoutEval(url, data, success_handler, fail_handler, synch);
}

function getCountyList(success_handler) {
    var data = "{}";
    var url = '/Service/AddressService.asmx/ListModel_Country';
    app.serviceManager.callService(url, data, success_handler, function (errorMsg) { console.log(errorMsg); }, false);
}

function getCityListTurkey(success_handler) {
    var url = '/Service/AddressService.asmx/ListModel_CityTurkey';
    app.serviceManager.callService(url, null, success_handler, function (errorMsg) { console.log(errorMsg); }, false);
}

function getCityList(country_id, success_handler) {
    var url = '/Service/AddressService.asmx/ListModel_City';
    var data = JSON.stringify({ country_id: country_id });
    app.serviceManager.callService(url, data, success_handler, function (errorMsg) { console.log(errorMsg); }, false);
}

function getTownList(city_id, success_handler) {
    var url = '/Service/AddressService.asmx/ListModel_Town';
    var data = JSON.stringify({ city_id: city_id });
    app.serviceManager.callService(url, data, success_handler, function (errorMsg) { console.log(errorMsg); }, false);
}

function getSideOrderDetail(success_handler) {
    var detail = `<div class="panel panel-default">

        <div class="panel-heading text-center">
            <h4>Sipariş Özeti</h4>
            <a href="MyBasket.aspx" class="btn btn-default"><i class="fa fa-shopping-basket" aria-hidden="true"></i>Sepete Git</a>

        </div>

        <div id="basket_side" class="panel-body">
        </div>

        <div class="panel-footer">
            <h4 class="text-center">GENEL TOPLAM</h4>
            <h4 id="total_price_side" class="text-center"></h4>
        </div>

    </div>
        <div class="well well-default">
            <button id="btn_finish2" type="submit" class="btn_finish btn btn-default btn-block">ALIŞVERİŞİ BİTİR <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
        </div>`;

    success_handler(detail);
}

function getSideMenu(active_page) {
    var page_name = "";
    if (active_page == 1) {
        page_name = getLocaliziedValue('Side_orders');
    } else if (active_page == 2) {
        page_name = getLocaliziedValue('Side_member_info');
    } else if (active_page == 3) {
        page_name = getLocaliziedValue('Side_address');
    } else if (active_page == 4) {
        page_name = getLocaliziedValue('Side_change_password');
    } else if (active_page == 5) {
        page_name = getLocaliziedValue('Side_remove_member');
    }

    var detail = `<aside class="col-sm-3 col-sm-pull-9 sidebar">
        <h4 class="sidebar-ana-baslik">` + getLocaliziedValue('Side_account') + ` > ` + page_name + `</h4>
        <div class="user-info ">
            <i class="fa fa-user user-icon" aria-hidden="true"></i>
            <h4 class="sidebar-user">Emre YILMAZ</h4>
        </div>
        <ul class="hesap-kategori cerceve">
            <li><a href="/MyOrders.aspx">` + getLocaliziedValue('Side_orders') + `</a></li>
            <li><a href="/CustomerUpdate.aspx">` + getLocaliziedValue('Side_member_info') + `</a></li>
            <li><a href="/MyAddress.aspx">` + getLocaliziedValue('Side_address') + `</a></li>
            <li><a href="/ChangePassword.aspx">` + getLocaliziedValue('Side_change_password') + `</a></li>
            <li><a href="/DeleteMember.aspx">` + getLocaliziedValue('Side_remove_member') + `</a></li>
        </ul>
    </aside>`;
    $('#side_menu').empty();
    $('#side_menu').append(detail);
}

function getUpMenu(active_page) {
    var active_basket = "";
    var active_address = "";
    var active_payment = "";

    if (active_page == 1) {
        active_basket = " aktif";
    } else if (active_page == 2) {
        active_address = " aktif";
    } else if (active_page == 3) {
        active_payment = " aktif";
    }
    var detail = `<div class="col-sm-12 odeme-adimlari">
        <div class="row">
            <div class="col-xs-4 adim` + active_basket + `">
                <h4>` + getLocaliziedValue('Up_basket') + `</h4>
            </div>

            <div class="col-xs-4 adim` + active_address + `">
                <h4>` + getLocaliziedValue('Up_address') + `</h4>
            </div>

            <div class="col-xs-4 adim` + active_payment + `">
                <h4>` + getLocaliziedValue('Up_payment') + `</h4>
            </div>
        </div>
    </div>`;
    $('#up_menu').empty();
    $('#up_menu').append(detail);
}

function initializeUser() {
    callService("LoginService", "GetAdmin", null, function (data) {
        window.localStorage.setItem('name', data.name + " " + data.surname);
    }, function (errormsg) {
        console.log(errormsg);
    }, false);
}

function initializeSettings() {
    callService("PackagePropertyService", "GetModel", null, function (data) {
        console.log(data);
        window.localStorage.setItem('package_property', JSON.stringify(data));
        var package_property_json = window.localStorage.getItem('package_property');
        var package_property = JSON.parse(package_property_json);
    }, function (errormsg) {
        console.log(errormsg);
    }, false);
}

function initDataTable(data, columns, buttons, orderHandler, pageLength) {
    tableData = data;

    if (table != undefined) {
        destroyTable(table);
        setTimeout(function () {
            initTable(data, columns, buttons, orderHandler, pageLength)
        }, 1000);
    } else {
        initTable(data, columns, buttons, orderHandler, pageLength)
    }
}

function destroyTable(table) {
    //table.destroy();
}

function initTable(data, columns, buttons, orderHandler, pageLength) {
    var _pageLength = pageLength == undefined ? 10 : pageLength;
    if (buttons == null || buttons == undefined) {
        buttons = [];
    }

    table = $('#_tableForm').DataTable({
        "language": {
            "lengthMenu": getLocaliziedValue("datatableButton_perPage"),
            "zeroRecords": getLocaliziedValue("datatableButton_noRecord"),
            "info": getLocaliziedValue("datatableButton_activePage") + ": _PAGE_ / _PAGES_",
            "infoEmpty": getLocaliziedValue("datatableButton_noData"),
            "infoFiltered": "",
            "search": getLocaliziedValue("datatableButton_search") + ":"
        },
        "aaSorting": [],
        data: data,
        columns: columns,
        dom: 'Bfrtip',
        buttons: buttons,
        autoWidth: false,
        rowReorder: true,
        "destroy": true,
        "pageLength": _pageLength
    });

    table.on('row-reorder', function (e, diff, edit) {
        var parameter = [];
        for (var i = 0; i < diff.length; i++) {
            var item = diff[i];
            var ID = item.node.getElementsByTagName('td')[0].textContent
            var newPosition = item.newPosition;
            var oldPosition = item.oldPosition;
            var newOrder = tableData[newPosition].order_id;
            var oldOrder = tableData[oldPosition].order_id;

            var data = { 'ID': ID, 'new_order_id': newOrder, 'old_order_id': oldOrder };
            parameter.push(data);
            console.log(data);
        }

        handleOrder(orderHandler, parameter);
    });
}

function handleOrder(orderHandler, parameter) {
    console.log(parameter);
    var selectedService = serviceName;
    var selectedFunction = 'OrderModel';
    var data = JSON.stringify({ parameter: parameter });
    callService(selectedService, selectedFunction, data, function (response) {
        if (response.responseCode == 200) {
            try { orderHandler(); } catch (exc) { }
        }
    }, function (errormsg) { console.log(errormsg); }, true);
}


function swapDataTableRows(selector, row1Index, row2Index) {
    var datatable = selector.DataTable();
    //var rows = datatable.rows().data();
    var row1Data = datatable.row(row1Index).data();
    var row2Data = datatable.row(row2Index).data();

    datatable.row(row1Index).data(row2Data).draw();
    datatable.row(row2Index).data(row1Data).draw();
    //    datatable.drow();
}

function setImageSrc(input, src) {
    //$('#' + input).attr("src", src);
    $('#' + input).parent().parent().find("div").css("background-image", 'url("' + src + '")');
}

function clearAndShowForm() {
    $('#_modalForm').modal('show');
    clearForm();
}

function clearAndHideForm() {
    //$('#_modalForm').modal('hide');
    clearForm();
}

function exportToCsv() {
    var filename = "export.csv";
    var rows = getTableData();
    var processRow = function (row) {

        var finalVal = '';
        for (var j in row) {
            if (row.hasOwnProperty(j)) {

                var innerValue = row[j] === null ? '' : row[j].toString();

                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                };

                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }
        }

        return finalVal + '\n';
    };

    var csvFile = '';
    for (var i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
    }

    var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}

function prepareSelectData(list, selectedValue, objField) {
    var data = [];

    var field = "name";
    if (objField && objField != null && objField != undefined) {
        field = objField;
    }

    $.each(list, function () {
        var selected = false;
        if (selectedValue && this.ID == selectedValue) {
            selected = true;
        }

        data.push({
            id: this.ID,
            text: this[field],
            selected: selected
        });
    });
    return data;
}

function getCategoryList(parentId) {
    if (parentId == undefined || parentId == null) {
        var data;
        getItemsSynch("CategoryService", null, function (response) {
            data = response.response;
        }, function (errormsg) {
            console.log(errormsg)
        }, "ListModel");
        return data;
    } else {
        var data;
        var parameter = JSON.stringify({ parent_id: parentId });
        getItemsSynch("CategoryService", parameter, function (response) {
            if (response.responseCode == 200) {
                data = response.response;
            }
        }, function (errormsg) {
            console.log(errormsg)
        }, "ListModel_ParentId");
        return data;
    }
}