﻿var Wizard = function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_contacts_add', {
            startStep: 1, // initial active step number
            clickableSteps: true  // allow step clicking
        });

        // Validation before going to next page
        wizard.on('beforeNext', function (wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop();  // don't go to the next step
            }
        })

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    }

    var initValidation = function () {
        validator = formEl.validate({
            // Validation rules
            rules: {
                // Step 1
                _company_title: {
                    required: true
                },
            },

            // Display error
            invalidHandler: function (event, validator) {
                KTUtil.scrollTop();
                alertFail("Hata", "Zorunlu alanları kontrol ediniz.");
            },

            // Submit valid form
            submitHandler: function (form) {

            }
        });
    }

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function (e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                KTApp.unprogress(btn);

                // TODO
                _form_save();
                // call form save
            }
        });
    }

    return {
        // public functions
        init: function () {
            formEl = $('#kt_contacts_add_form');

            initWizard();
            initValidation();
            initSubmit();
        }
    };
}();

var selected = 0;
var selectedModel = 0;
var pageNameKey = "PAGENAME_ADMINLIST";
var serviceName = "KurumSaglikIlanService";

function initpage() {
    new KTAvatar("_image");
    $("#_price").inputmask("999.999.999,99", { numericInput: !0 });
}

function afterPageLoad() {
    $('#_tıbbi_merkez_id').on('change', function () {
        var tibbi_merkez = this.value;
        loadTibbiMerkezDetay(tibbi_merkez);
    })
    $('#_birim_id').on('change', function () {
        var ilgili_birim = this.value;
        loadIslem(ilgili_birim);
    })

    loadTibbiMerkez();
    loadHizmetler();
    loadCurrency();
    loadDatePicker();
    Wizard.init();
}

function loadDatePicker(discount_start_date, discount_end_date) {
    $("#_discount_start_date").datepicker({
        todayHighlight: !0,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        "autoclose": true
    }, function (a, t, e) {
        $("#_discount_start_date .form-control").val(a.format("DD/MM/YYYY"));
        selectedDate = a.format("DD/MM/YYYY") + "/" + t.format("DD/MM/YYYY");
    });
    //$("#_establish_date").val(establish_date);
    if (discount_start_date) {
        $('#_discount_start_date').datepicker('setDate', new Date(discount_start_date.split("/")[2], discount_start_date.split("/")[1] - 1, discount_start_date.split("/")[0]));
    }

    $("#_discount_end_date").datepicker({
        todayHighlight: !0,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        "autoclose": true
    }, function (a, t, e) {
        $("#_discount_end_date .form-control").val(a.format("DD/MM/YYYY"));
        selectedDate = a.format("DD/MM/YYYY") + "/" + t.format("DD/MM/YYYY");
    });
    //$("#_renovation_date").val(renovation_date);
    if (discount_end_date) {
        $("#_discount_end_date").datepicker("setDate", new Date(discount_end_date.split("/")[2], discount_end_date.split("/")[1] - 1, discount_end_date.split("/")[0]));
    }
}

function _form_save() {
    var image = uploadImage("_image_val");
    var hizmetler = convertModelArray($('#_hizmetler').val());

    var model = {
        ID: selected,
        header: $('#_header').val(),
        header_eng: $('#_header_eng').val(),
        header_other: $('#_header_other').val(),
        tıbbi_merkez_id: $('#_tıbbi_merkez_id').val(),
        birim_id: $('#_birim_id').val(),
        islem_id: $('#_islem_id').val(),
        detail: $('#_detail').val(),
        detail_eng: $('#_detail_eng').val(),
        detail_other: $('#_detail_other').val(),
        prepare: $('#_prepare').val(),
        prepare_eng: $('#_prepare_eng').val(),
        prepare_other: $('#_prepare_other').val(),
        time: $('#_time').val(),
        time_eng: $('#_time_eng').val(),
        time_other: $('#_time_other').val(),
        later: $('#_later').val(),
        later_eng: $('#_later_eng').val(),
        later_other: $('#_later_other').val(),
        hizmetler: hizmetler,
        date_option: $('#_date_option').val(),
        price: $('#_price').inputmask('unmaskedvalue') == '' ? 0 : $('#_price').inputmask('unmaskedvalue'),
        currency: $('#_currency').val(),
        discount: $('#_discount').val(),
        discount_start_date_s: $('#_discount_start_date').val(),
        discount_end_date_s: $('#_discount_end_date').val(),
        prepaid: $('#_prepaid').val(),
        cancel_type: $('#_cancel_type').val(),
        image: image
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {

            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function convertModelArray(array) {
    var newArray = [];
    array.forEach(function (item) {
        newArray.push({ ID: item });
    });
    return newArray;
}









//////////////************ sayfa datasını yükleyen fonksiyonlar ************//////////////
function _load_relations(model) {
    loadCompany(model.company_type_id);
    loadLanguage(model.favorite_language_id);
    loadCurrency(model.currency);
    loadCenter(model.center_type_id);
    loadCenterStatu(model.center_statu_id);
    loadDoctorTitle(model.doktorlar);

    loadPoliklinik(model.poliklinikler);
    loadCerrahiBirimler(model.cerrahi_birimler);
    loadAgizDisSagligi(model.agiz_dis_sagligi);
    loadGozSagligi(model.goz_sagligi);
    loadEstetikGuzellik(model.estetik_guzellik);
    loadBeslenmeDiyet(model.beslenme_diyet);
    loadFizikTedaviRehabilitasyon(model.fizik_tedavi_rehabilitasyon);
    loadDiyaliz(model.diyaliz);
    loadAnlasmaliKurumlar(model.anlasmali_kurumlar);
    loadLanguageAll(model.languages);

    doctorList = model.doktorlar;
    loadDoctors();

    loadCountry(model.country_id);
    loadAvatar();
    setImageSrc("_logo_val", "/Data/" + model.logo);
    setImageSrc("_image_val", "/Data/" + model.image);

    var establish_date = moment(model.establish_date).format('DD/MM/YYYY');
    var renovation_date = moment(model.renovation_date).format('DD/MM/YYYY');
    loadDatePicker(establish_date, renovation_date);

    $('#_company_title').val(model.company_title);
    $('#_tax_office').val(model.tax_office);
    $('#_tax_number').val(model.tax_number);

    $('#_center_name').val(model.center_name);
    $('#_tax_number').val(model.tax_number);

    $('#_area').val(model.area);
    $('#_capacity').val(model.capacity);
    $('#_personnel_count').val(model.personnel_count);

    $('#_address').val(model.address);
    $('#_zip_code').val(model.zip_code);
    $('#_distance_airplane').val(model.distance_airplane);
    $('#_distance_center').val(model.distance_center);
    $('#_detail').val(model.detail);
    $('#_detail_eng').val(model.detail_eng);
    $('#_detail_other').val(model.detail_other);

}

function loadCurrency() {
    getItemsAsynch(serviceName, null, function (response) {
        loadSelectData("#_currency", response.response);
    }, function (errormsg) { console.log(errormsg) }, "ListCurrency");
}

function loadHizmetler() {
    getItemsAsynch("HizmetService", null, function (response) {
        loadSelectData("#_hizmetler", response.response);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadTibbiMerkez() {
    getItemsAsynch("TibbiMerkezService", null, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_tıbbi_merkez_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadTibbiMerkezDetay(tibbi_merkez) {
    if (tibbi_merkez == 1) {
        loadPoliklinik();
    } else if (tibbi_merkez == 2) {
        loadCerrahiBirimler();
    } else if (tibbi_merkez == 3) {
        loadAgizDisSagligi();
    } else if (tibbi_merkez == 4) {
        loadGozSagligi();
    } else if (tibbi_merkez == 5) {
        loadEstetikGuzellik();
    } else if (tibbi_merkez == 6) {
        loadBeslenmeDiyet();
    } else if (tibbi_merkez == 7) {
        loadFizikTedaviRehabilitasyon();
    } else if (tibbi_merkez == 8) {
        loadDiyaliz();
    }
}

function loadIslem(birim_id) {
    var data = JSON.stringify({ birim_id: birim_id });
    getItemsAsynch("IslemService", data, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_islem_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModelBirimId");
}

function loadPoliklinik() {
    getItemsAsynch("PoliklinikService", null, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_birim_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadCerrahiBirimler() {
    getItemsAsynch("CerrahiBirimlerService", null, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_birim_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadAgizDisSagligi() {
    getItemsAsynch("AgizDisSagligiService", null, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_birim_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadGozSagligi() {
    getItemsAsynch("GozSagligiService", null, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_birim_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadEstetikGuzellik() {
    getItemsAsynch("EstetikGuzellikService", null, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_birim_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadBeslenmeDiyet() {
    getItemsAsynch("BeslenmeDiyetService", null, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_birim_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadFizikTedaviRehabilitasyon() {
    getItemsAsynch("FizikTedaviRehabilitasyonService", null, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_birim_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadDiyaliz() {
    getItemsAsynch("DiyalizService", null, function (response) {
        var list = response.response;
        list.unshift({
            id: 0,
            name: getLocaliziedValue("FORM_select"),
            selected: true
        });
        loadSelectData("#_birim_id", list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}



function loadAvatar() {
    new KTAvatar("_logo");
    new KTAvatar("_image");
    new KTAvatar("_galeri");
}





function loadSelectData(select_id, data, list) {
    var select = $(select_id);
    select.html('');
    $.each(data, function (key, val) {
        select.append('<option value="' + val.ID + '">' + val.name + '</option>');
    });
    select.selectpicker('refresh');

    if (list && list.length > 0) {
        var array = [];
        $.each(list, function (key, val) { array.push(val.ID); });
        $(select_id).selectpicker('val', array);
    }
}








