﻿var Validator = function () {

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
        $('#_name_eng').on('input', function () { validator.element($(this)); });
        $('#_name_other').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 50 },
                _name_eng: { required: true, minlength: 1, maxlength: 50 },
                _name_other: { required: true, minlength: 1, maxlength: 50 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_CONTENTDETAIL";
var serviceName = "ContentService";

function initpage() {
    $('.summenote-sm').summernote({ height: 300 });
}

function afterPageLoad() {
    setTimeout(function () {
        var ID = getParameterByName("ID");
        loadData(ID);
    }, 1000)
}

function addEventListener() {

}

function loadData(ID) {
    var data = JSON.stringify({ ID: ID });
    callService(serviceName, 'LoadModel', data, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;
    $('#_name').val(model.name);
    $('#_name_eng').val(model.name_eng);
    $('#_name_other').val(model.name_other);
    $('.note-editable').eq(0).html(model.detail);
    $('.note-editable').eq(1).html(model.detail_eng);
    $('.note-editable').eq(2).html(model.detail_other);
}

function _form_save() {
    var model = {
        ID: selected,
        name: $('#_name').val(),
        name_eng: $('#_name_eng').val(),
        name_other: $('#_name_other').val(),
        detail: $('.note-editable').eq(0).html(),
        detail_eng: $('.note-editable').eq(1).html(),
        detail_other: $('.note-editable').eq(2).html()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                window.location.href = "/Panel/Pages/Settings/ContentList.aspx";
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    $('#_name').val("");
    $('#_name_eng').val("");
    $('#_name_other').val("");
    $('.note-editable').eq(0).html("");
    $('.note-editable').eq(1).html("");
    $('.note-editable').eq(2).html("");
}