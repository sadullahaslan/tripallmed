﻿function initpage() {

    $(document).ready(function () {

        var card = new Card({
            form: 'form', // *required*
            container: '.card-wrapper', // *required*
            // Default placeholders for rendered fields - optional
            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'xxxx xxxx',
                expiry: '••/••',
                cvc: '•••'
            },
            masks: {
                cardNumber: '•' // optional - mask card number
            },
            // if true, will log helpful messages for setting up Card
            debug: true // optional - default false
        });
    });
    $('#_pay').on('click', function (e) {
        e.preventDefault();
        pay();
    });
}

function pay() {

    var number = $('#_number').val().replace(/ /g, '');
    var name = $('#_name').val();
    var expiry = $('#_expiry').val().replace(/ /g, '').split('/');
    var year = expiry[1];
    var month = expiry[0];
    var cvc = $('#_cvc').val().replace(/ /g, '');

    var card = {
        cardNumber: number,
        name: name,
        month: month,
        year: year,
        cvv: cvc,
    }
    var data = JSON.stringify({ card: card });

    callServiceWithoutEval('PaymentService', 'Pay', data, function (response) {
        if (response.responseCode == 200) {
            var html = response.response;
            $('#_modalForm').modal('show');
            document.getElementById('iframe').src = "data:text/html;charset=utf-8," + escape(html);
        } else {
            var html = response.response;
            alert("hata" + html);
        }

        
    }, function (errormsg) { console.log(errormsg); }, false);

}
