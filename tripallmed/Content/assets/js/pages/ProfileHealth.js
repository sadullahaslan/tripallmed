﻿var Wizard = function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_contacts_add', {
            startStep: 1, // initial active step number
            clickableSteps: true  // allow step clicking
        });

        // Validation before going to next page
        //wizard.on('beforeNext', function (wizardObj) {
        //    if (validator.form() !== true) {
        //        wizardObj.stop();  // don't go to the next step
        //    }
        //})

        // Change event
        wizard.on('change', function (wizard) {
            KTUtil.scrollTop();
        });
    }

    //var initValidation = function () {
    //    validator = formEl.validate({
    //        // Validation rules
    //        rules: {
    //            // Step 1
    //            _company_title: {
    //                required: true
    //            },
    //        },

    //        // Display error
    //        invalidHandler: function (event, validator) {
    //            KTUtil.scrollTop();
    //            alertFail("Hata", "Zorunlu alanları kontrol ediniz.");
    //        },

    //        // Submit valid form
    //        submitHandler: function (form) {

    //        }
    //    });
    //}

    var initSubmit = function () {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function (e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                KTApp.unprogress(btn);

                // TODO
                _form_save();
                // call form save
            }
        });
    }

    return {
        // public functions
        init: function () {
            formEl = $('#kt_contacts_add_form');

            initWizard();
            //initValidation();
            //initSubmit();
        }
    };
}();

var selected = 0;
var selectedModel = 0;
var pageNameKey = "PAGENAME_ADMINLIST";
var serviceName = "ProfileHealthService";
var doctorList = [];

function initpage() {
    callService(serviceName, 'LoadModelSession', null, function (response) {
        if (response.responseCode == 200) {
            var model = response.response;
            selectedModel = model;
            selected = model.ID;
            _load_relations(model);
        } else {
            alert("HATA")
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function afterPageLoad() {
    $('#_country_id').on('change', function () {
        var country = this.value;
        loadCity(country);
    })
    $('#_city_id').on('change', function () {
        var city = this.value;
        loadTown(city);
    })

    //$("#_btn_doctor").click(function (e) {
    //    e.preventDefault();
    //    addDoctor();
    //    loadDoctors();
    //});

    //$('#table_doctor').on('click', '.btn-delete', function () {
    //    var order = $($((this).closest('tr')).find('td')[0]).text();
    //    doctorList.splice((order - 1), 1);
    //    loadDoctors();
    //});

    Wizard.init();
}

function _form_save() {
    var logo = uploadImage("_logo_val");
    var image = uploadImage("_image_val");

    var languages = convertModelArray($('#_languages').val());
    var poliklinikler = convertModelArray($('#_poliklinikler').val());
    var cerrahi_birimler = convertModelArray($('#_cerrahi_birimler').val());
    var agiz_dis_sagligi = convertModelArray($('#_agiz_ve_dis_sagligi').val());
    var goz_sagligi = convertModelArray($('#_goz_sagligi').val());
    var estetik_guzellik = convertModelArray($('#_estetik_guzellik').val());
    var beslenme_diyet = convertModelArray($('#_beslenme_diyet').val());
    var fizik_tedavi_rehabilitasyon = convertModelArray($('#_fizik_tedavi_rehabilitasyon').val());
    var diyaliz = convertModelArray($('#_diyaliz').val());
    var anlasmali_kurumlar = convertModelArray($('#_anlasmali_kurumlar').val());
    var doktorlar = doctorList;
    var galeri = [];

    var model = {
        ID: selected,
        company_type_id: $('#_company_type_id').val(),
        company_title: $('#_company_title').val(),
        tax_office: $('#_tax_office').val(),
        tax_number: $('#_tax_number').val(),
        favorite_language_id: $('#_favorite_language_id').val(),
        favorite_currency_id: $('#_favorite_currency_id').val(),
        center_name: $('#_center_name').val(),
        center_type_id: $('#_center_type_id').val(),
        center_statu_id: $('#_center_statu_id').val(),
        establish_date_s: $('#_establish_date').val(),
        renovation_date_s: $('#_renovation_date').val(),
        area: $('#_area').val(),
        capacity: $('#_capacity').val(),
        personnel_count: $('#_personnel_count').val(),
        languages: languages,
        poliklinikler: poliklinikler,
        cerrahi_birimler: cerrahi_birimler,
        agiz_dis_sagligi: agiz_dis_sagligi,
        goz_sagligi: goz_sagligi,
        estetik_guzellik: estetik_guzellik,
        beslenme_diyet: beslenme_diyet,
        fizik_tedavi_rehabilitasyon: fizik_tedavi_rehabilitasyon,
        diyaliz: diyaliz,
        anlasmali_kurumlar: anlasmali_kurumlar,
        doktorlar: doktorlar,

        country_id: $('#_country_id').val(),
        city_id: $('#_city_id').val(),
        town_id: $('#_town_id').val(),
        address: $('#_address').val(),
        zip_code: $('#_zip_code').val(),

        distance_airplane: $('#_distance_airplane').val(),
        distance_center: $('#_distance_center').val(),

        detail: $('#_detail').val(),
        detail_eng: $('#_detail_eng').val(),
        detail_other: $('#_detail_other').val(),

        logo: logo,
        image: image,
        galeri: galeri
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {

            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function convertModelArray(array) {
    var newArray = [];
    array.forEach(function (item) {
        newArray.push({ ID: item });
    });
    return newArray;
}

function addDoctor() {
    var doktor_type_id = $("#_doktor_title").val();
    var doktor_type_name = $("#_doktor_title option:selected").text();
    var name = $("#_doktor_name").val();
    var surname = $("#_doktor_surname").val();
    var poliklinik_id = $("#_doktor_poliklinikler").val();
    var poliklinik_name = $("#_doktor_poliklinikler option:selected").text();

    var doctor = {
        doktor_type_id: doktor_type_id,
        doktor_type_name: doktor_type_name,
        name: name,
        surname: surname,
        poliklinik_id: poliklinik_id,
        poliklinik_name: poliklinik_name
    }

    doctorList.push(doctor);
}

function loadDoctors() {
    $('#table_body_doctor').empty();
    for (var i = 0; i < doctorList.length; i++) {
        var line =
            `<tr>
                <td>` + (i + 1) + `</td>
                <td>` + doctorList[i].doktor_type_name + `</td>
                <td>` + doctorList[i].name + " " + doctorList[i].surname + `</td>
                <td>` + doctorList[i].poliklinik_name + `</td>
                <td><button class="btn-delete" type="button">Sil</button></td>
            </tr>`;
        $('#table_body_doctor').append(line);
    }
}

function mapDoctors(model) {

    for (var i = 0; i < model.doktorlar.length; i++) {
        var doctor = {
            doktor_type_id: model.doktorlar[i].doktor_type_id,
            doktor_type_name: model.doktorlar[i].doktor_type_name,
            name: model.doktorlar[i].name,
            surname: model.doktorlar[i].surname,
            poliklinik_id: model.doktorlar[i].poliklinik_id,
            poliklinik_name: model.doktorlar[i].poliklinik_name
        }
        doctorList.push(doctor);
    }
}









//////////////************ sayfa datasını yükleyen fonksiyonlar ************//////////////
function _load_relations(model) {
    loadCompany(model.company_type_id);
    loadLanguage(model.favorite_language_id);
    loadCurrency(model.favorite_currency_id);
    loadCenter(model.center_type_id);
    loadCenterStatu(model.center_statu_id);
    loadDoctorTitle(model.doktorlar);

    loadPoliklinik(model.poliklinikler);
    loadCerrahiBirimler(model.cerrahi_birimler);
    loadAgizDisSagligi(model.agiz_dis_sagligi);
    loadGozSagligi(model.goz_sagligi);
    loadEstetikGuzellik(model.estetik_guzellik);
    loadBeslenmeDiyet(model.beslenme_diyet);
    loadFizikTedaviRehabilitasyon(model.fizik_tedavi_rehabilitasyon);
    loadDiyaliz(model.diyaliz);
    loadAnlasmaliKurumlar(model.anlasmali_kurumlar);
    loadLanguageAll(model.languages);

    mapDoctors(model);
    loadDoctors();

    loadCountry(model.country_id);
    loadAvatar();
    setImageSrc("_logo_val", "/Data/" + model.logo);
    setImageSrc("_image_val", "/Data/" + model.image);

    var establish_date = moment(model.establish_date).format('DD/MM/YYYY');
    var renovation_date = moment(model.renovation_date).format('DD/MM/YYYY');
    loadDatePicker(establish_date, renovation_date);

    $('#_company_title').val(model.company_title);
    $('#_tax_office').val(model.tax_office);
    $('#_tax_number').val(model.tax_number);

    $('#_center_name').val(model.center_name);
    $('#_tax_number').val(model.tax_number);

    $('#_area').val(model.area);
    $('#_capacity').val(model.capacity);
    $('#_personnel_count').val(model.personnel_count);

    $('#_address').val(model.address);
    $('#_zip_code').val(model.zip_code);
    $('#_distance_airplane').val(model.distance_airplane);
    $('#_distance_center').val(model.distance_center);
    $('#_detail').val(model.detail);
    $('#_detail_eng').val(model.detail_eng);
    $('#_detail_other').val(model.detail_other);

}

function loadCompany(ID) {
    getItemsAsynch(serviceName, null, function (response) {
        loadSelectData("#_company_type_id", response.response, [{ ID: ID }]);
    }, function (errormsg) { console.log(errormsg) }, "ListCompanyType");
}

function loadLanguage(ID) {
    getItemsAsynch(serviceName, null, function (response) {
        loadSelectData("#_favorite_language_id", response.response, [{ ID: ID }]);
    }, function (errormsg) { console.log(errormsg) }, "ListLanguage");
}

function loadCurrency(ID) {
    getItemsAsynch(serviceName, null, function (response) {
        loadSelectData("#_favorite_currency_id", response.response, [{ ID: ID }]);
    }, function (errormsg) { console.log(errormsg) }, "ListCurrency");
}

function loadCenter(ID) {
    getItemsAsynch(serviceName, null, function (response) {
        loadSelectData("#_center_type_id", response.response, [{ ID: ID }]);
    }, function (errormsg) { console.log(errormsg) }, "ListCenter");
}

function loadCenterStatu(ID) {
    getItemsAsynch(serviceName, null, function (response) {
        loadSelectData("#_center_statu_id", response.response, [{ ID: ID }]);
    }, function (errormsg) { console.log(errormsg) }, "ListCenterStatu");
}

function loadDoctorTitle(list) {
    getItemsAsynch(serviceName, null, function (response) {
        loadSelectData("#_doktor_title", response.response);
    }, function (errormsg) { console.log(errormsg) }, "ListDoctorTitle");
}

function loadPoliklinik(list) {
    getItemsAsynch("PoliklinikService", null, function (response) {
        loadSelectData("#_poliklinikler", response.response, list);
        loadSelectData("#_doktor_poliklinikler", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadCerrahiBirimler(list) {
    getItemsAsynch("CerrahiBirimlerService", null, function (response) {
        loadSelectData("#_cerrahi_birimler", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadAgizDisSagligi(list) {
    getItemsAsynch("AgizDisSagligiService", null, function (response) {
        loadSelectData("#_agiz_ve_dis_sagligi", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadGozSagligi(list) {
    getItemsAsynch("GozSagligiService", null, function (response) {
        loadSelectData("#_goz_sagligi", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadEstetikGuzellik(list) {
    getItemsAsynch("EstetikGuzellikService", null, function (response) {
        loadSelectData("#_estetik_guzellik", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadBeslenmeDiyet(list) {
    getItemsAsynch("BeslenmeDiyetService", null, function (response) {
        loadSelectData("#_beslenme_diyet", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadFizikTedaviRehabilitasyon(list) {
    getItemsAsynch("FizikTedaviRehabilitasyonService", null, function (response) {
        loadSelectData("#_fizik_tedavi_rehabilitasyon", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadDiyaliz(list) {
    getItemsAsynch("DiyalizService", null, function (response) {
        loadSelectData("#_diyaliz", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadAnlasmaliKurumlar(list) {
    getItemsAsynch("AnlasmaliKurumlarService", null, function (response) {
        loadSelectData("#_anlasmali_kurumlar", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadLanguageAll(list) {
    getItemsAsynch("LanguageService", null, function (response) {
        loadSelectData("#_languages", response.response, list);
    }, function (errormsg) { console.log(errormsg) }, "ListModel");
}

function loadCountry(ID) {
    getItemsAsynch("AddressService", null, function (response) {
        loadSelectData("#_country_id", response.response, [{ ID: ID }]);
        loadCity(ID, selectedModel.city_id);
    }, function (errormsg) { console.log(errormsg) }, "ListModel_Country");
}

function loadCity(country_id, ID) {
    var data = JSON.stringify({ country_id: country_id });
    getItemsAsynch("AddressService", data, function (response) {
        loadSelectData("#_city_id", response.response, [{ ID: ID }]);
        loadTown(ID, selectedModel.town_id);
    }, function (errormsg) { console.log(errormsg) }, "ListModel_City");
}

function loadTown(city_id, ID) {
    var data = JSON.stringify({ city_id: city_id });
    getItemsAsynch("AddressService", data, function (response) {
        loadSelectData("#_town_id", response.response, [{ ID: ID }]);
    }, function (errormsg) { console.log(errormsg) }, "ListModel_Town");
}

function loadDatePicker(establish_date, renovation_date) {
    $("#_establish_date").datepicker({
        todayHighlight: !0,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        "autoclose": true
    }, function (a, t, e) {
        $("#_establish_date .form-control").val(a.format("DD/MM/YYYY"));
        selectedDate = a.format("DD/MM/YYYY") + "/" + t.format("DD/MM/YYYY");
    });
    //$("#_establish_date").val(establish_date);
    $('#_establish_date').datepicker('setDate', new Date(establish_date.split("/")[2], establish_date.split("/")[1] - 1, establish_date.split("/")[0]));

    $("#_renovation_date").datepicker({
        todayHighlight: !0,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        "autoclose": true
    }, function (a, t, e) {
        $("#_renovation_date .form-control").val(a.format("DD/MM/YYYY"));
        selectedDate = a.format("DD/MM/YYYY") + "/" + t.format("DD/MM/YYYY");
    });
    //$("#_renovation_date").val(renovation_date);
    $("#_renovation_date").datepicker("setDate", new Date(renovation_date.split("/")[2], renovation_date.split("/")[1] - 1, renovation_date.split("/")[0]));

}

function loadAvatar() {
    new KTAvatar("_logo");
    new KTAvatar("_image");
    new KTAvatar("_galeri");
}





function loadSelectData(select_id, data, list) {
    var select = $(select_id);
    select.html('');
    $.each(data, function (key, val) {
        select.append('<option value="' + val.ID + '">' + val.name + '</option>');
    });
    select.selectpicker('refresh');

    if (list && list.length > 0) {
        var array = [];
        $.each(list, function (key, val) { array.push(val.ID); });
        $(select_id).selectpicker('val', array);
    }
}








