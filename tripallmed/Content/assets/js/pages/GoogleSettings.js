﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_validation_code').on('input', function () { validator.element($(this)); });
        $('#_analytics_code').on('input', function () { validator.element($(this)); });
        $('#_remarketing_code').on('input', function () { validator.element($(this)); });
        $('#_conversion_code').on('input', function () { validator.element($(this)); });
        $('#_result_code').on('input', function () { validator.element($(this)); });
        $('#_merchant_link').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _validation_code: { required: false, minlength: 1 },
				_analytics_code: { required: false, minlength: 1 },
				_remarketing_code: { required: false, minlength: 1 },
				_conversion_code: { required: false, minlength: 1 },
				_result_code: { required: false, minlength: 1 },
				_merchant_link: { required: false, minlength: 1 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_GOOGLESETTINGS";
var serviceName = "SettingGoogleService";

function initpage() {
}

function afterPageLoad() {
    loadData();
}

function addEventListener() {

}

function loadData() {
    callService(serviceName, 'LoadModelExact', null, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;
    $('#_validation_code').val(model.validation_code);
    $('#_analytics_code').val(model.analytics_code);
    $('#_remarketing_code').val(model.remarketing_code);
    $('#_conversion_code').val(model.conversion_code);
    $('#_result_code').val(model.result_code);
    $('#_merchant_link').val(model.merchant_link);
    $('#_modalForm').modal('show');
}

function _form_save() {
    var model = {
        ID: selected,
        validation_code: $('#_validation_code').val(),
        analytics_code: $('#_analytics_code').val(),
        remarketing_code: $('#_remarketing_code').val(),
        conversion_code: $('#_conversion_code').val(),
        result_code: $('#_result_code').val(),
        merchant_link: $('#_merchant_link').val()       
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    $('#_validation_code').val("");
    $('#_analytics_code').val("");
    $('#_remarketing_code').val("");
    $('#_conversion_code').val("");
    $('#_result_code').val("");
    $('#_merchant_link').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}