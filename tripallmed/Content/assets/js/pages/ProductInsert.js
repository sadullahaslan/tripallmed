﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
        $('#_name_eng').on('input', function () { validator.element($(this)); });
        $('#_name_other').on('input', function () { validator.element($(this)); });
        $('#_detail').on('input', function () { validator.element($(this)); });
        $('#_detail_eng').on('input', function () { validator.element($(this)); });
        $('#_detail_other').on('input', function () { validator.element($(this)); });
        $('#_name_seo').on('input', function () { validator.element($(this)); });
        $('#_name_seo_eng').on('input', function () { validator.element($(this)); });
        $('#_name_seo_other').on('input', function () { validator.element($(this)); });

        $('#_name_seo').bind("keyup focusout", function () {
            clearSeo(this);
        });

        $('#_name_seo_eng').bind("keyup focusout", function () {
            clearSeo(this);
        });

        $('#_name_seo_other').bind("keyup focusout", function () {
            clearSeo(this);
        });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 50 },
                _name_eng: { required: true, minlength: 1, maxlength: 50 },
                _name_other: { required: true, minlength: 1, maxlength: 50 },
                _detail: { required: true, minlength: 1, maxlength: 200 },
                _detail_eng: { required: true, minlength: 1, maxlength: 200 },
                _detail_other: { required: true, minlength: 1, maxlength: 200 },
                _name_seo: { required: true, minlength: 1, maxlength: 50 },
                _name_seo_eng: { required: true, minlength: 1, maxlength: 50 },
                _name_seo_other: { required: true, minlength: 1, maxlength: 50 },
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_PRODUCTINSERT";
var serviceName = "ProductService";
var productVariantItemList = [];

function initpage() {
    loadCategoryList();
    loadBrandList();
    loadSupplierList();
    loadProductVaryantList();
}

function afterPageLoad() {
    initImages();
    initEditors();
    initSwitches();
    initSelects();
    initMasks();

    setTimeout(function () {
        var ID = getParameterByName("ID");
        if (ID != null && ID != undefined) {
            loadData(ID);
        }
    }, 500);

    $("#_form_action_varyant").click(function (e) {
        e.preventDefault();
        var msg = "Varyant Seçimine yüklenen seçenekler arasından stok eklemek istediğiz seçeneği ve bilgileri girerek ilerleyebilirsiniz.";
        alertWarning("Uyarı", msg, function () { loadProductVaryantItemList(); });
    });

    $("#_form_action_varyant_item").click(function (e) {
        e.preventDefault();
        addProductVaryantListItem();
        loadProductVaryantListItem();
        alertSuccess("Ekleme", "Varyant Eklenecek");
    });

    $('#tableVaryant').on('click', '.btn-delete', function () {
        var order = $($((this).closest('tr')).find('td')[0]).text();
        productVariantItemList.splice((order - 1), 1);
        loadProductVaryantListItem();
    });

}

function initImages() {
    new KTAvatar("_image1");
    new KTAvatar("_image2");
    new KTAvatar("_image3");
    new KTAvatar("_image4");
    new KTAvatar("_image5");
    new KTAvatar("_image6");
}

function initEditors() {
    $('.summenote-sm').summernote({ height: 300 });
}

function initSwitches() {
    $("[data-switch=true]").bootstrapSwitch();
}

function initSelects() {
    $("#_delivery_time").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_facebook_store").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_facebook_store_enabled").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_price_type_variant").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_currency_id").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
    
}

function initMasks() {
    $("#_buy_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_sell_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_discount_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_kdv").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_price_variant").inputmask("999.999.999,99", { numericInput: !0 });
}

function loadData(ID) {
    var data = JSON.stringify({ ID: ID });
    callService(serviceName, 'LoadModel', data, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;

    setImageSrc("_image1_val", "/Data/" + model.image1);
    setImageSrc("_image2_val", "/Data/" + model.image2);
    setImageSrc("_image3_val", "/Data/" + model.image3);
    setImageSrc("_image4_val", "/Data/" + model.image4);
    setImageSrc("_image5_val", "/Data/" + model.image5);
    setImageSrc("_image6_val", "/Data/" + model.image6);
    setImageSrc("_image7_val", "/Data/" + model.image7);
    setImageSrc("_image8_val", "/Data/" + model.image8);

    $('#_name').val(model.name);
    $('#_name_eng').val(model.name_eng);
    $('#_name_other').val(model.name_other);

    $("#_category").val(model.category_id).trigger('change');
    $("#_brand").val(model.brand_id).trigger('change');
    $("#_supplier").val(model.supplier_id).trigger('change');

    $('#_barcode').val(model.barcode);
    $('#_stock_code').val(model.stock_code);
    $('#_product_code').val(model.product_code);

    $('#_name_seo').val(model.name_seo);
    $('#_name_seo_eng').val(model.name_seo_eng);
    $('#_name_seo_other').val(model.name_seo_other);

    $('#_detail').val(model.detail);
    $('#_detail_eng').val(model.detail_eng);
    $('#_detail_other').val(model.detail_other);

    $('#_delivery_info').val(model.delivery_info);
    $('#_delivery_info_eng').val(model.delivery_info_eng);
    $('#_delivery_info_other').val(model.delivery_info_other);

    $('#_stock').val(model.stock);
    $('#_desi').val(model.desi);
    $('#_delivery_time').val(model.delivery_time);

    $('#_buy_price').val(model.buy_price);
    $('#_sell_price').val(model.sell_price);
    $('#_discount_price').val(model.discount_price);
    $('#_currency_id').val(model.currency_id);
    $('#_kdv').val(model.kdv);

    $('#_gtin').val(model.gtin);
    $('#_mpn').val(model.mpn);

    $("#_free_cargo_product_enabled").bootstrapSwitch('state', model.free_cargo_product_enabled == 1 ? true : false);
    $("#_best_seller_product_enabled").bootstrapSwitch('state', model.best_seller_product_enabled == 1 ? true : false);
    $("#_new_product_enabled").bootstrapSwitch('state', model.new_product_enabled == 1 ? true : false);
    $("#_discounted_product_enabled").bootstrapSwitch('state', model.discounted_product_enabled == 1 ? true : false);
    $("#_special_product_enabled").bootstrapSwitch('state', model.special_product_enabled == 1 ? true : false);

    $('.note-editable').eq(0).html(model.long_detail);
    $('.note-editable').eq(1).html(model.long_detail_eng);
    $('.note-editable').eq(2).html(model.long_detail_other);

    $('#_seo_title').val(model.seo_title);
    $('#_seo_description').val(model.seo_description);
    $('#_seo_keywords').val(model.seo_keywords);

    $('#_google_ads_type').val(model.google_ads_type);
    $('#_google_ads_category').val(model.google_ads_category);
    $('#_google_ads_detail').val(model.google_ads_detail);
    $("#_facebook_store_enabled").val(model.facebook_store_enabled).trigger('change');

    $('#_video1').val(model.video1);
    $('#_video2').val(model.video2);
    $('#_video3').val(model.video3);

    // TODO varyantItemList
    productVariantItemList = model.varyantItemList;
    loadProductVaryantListItem();
}

function loadProductVaryantList() {
    callService("ProductVaryantService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            var varyantList = prepareSelectData(response.response);
            $("#_product_varyant").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: varyantList
            })
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function loadProductVaryantItemList() {
    var product_variant_id = $('#_product_varyant').val();
    var data = JSON.stringify({ 'product_variant_id': product_variant_id });
    callService("ProductVaryantService", 'ListModelVariantItem', data, function (response) {
        if (response.responseCode == 200) {
            var varyantItemList = prepareSelectData(response.response, null, "name");
            $("#_product_varyant_item").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: varyantItemList
            })
        } else if (response.responseCode == 500) {

        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function loadCategoryList() {
    var data = [];
    var parentList = getCategoryList(0);
    $.each(parentList, function () {
        data.push({
            id: this.ID,
            text: this.name
        });
        var subList = getCategoryList(this.ID);
        $.each(subList, function () {
            data.push({
                id: this.ID,
                text: "-" + this.name
            });
            var subSubList = getCategoryList(this.ID);
            $.each(subSubList, function () {
                data.push({
                    id: this.ID,
                    text: "--" + this.name
                });
            });
        });
    });
    $("#_category").select2({
        placeholder: getLocaliziedValue("action_select"),
        data: data
    })
}

function loadBrandList() {
    callService("BrandService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            var brandList = prepareSelectData(response.response);
            $("#_brand").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: brandList
            })
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function loadSupplierList() {
    callService("SupplierService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            var brandList = prepareSelectData(response.response);
            $("#_supplier").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: brandList
            })
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function loadProductAttributeList() {
    callService("ProductAttributeService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            $('#product_attribute_list').empty();
            $.each(response.response, function () {
                var opt = '<label class="kt-checkbox"> <input class="product_attribute" type="checkbox" id="' + this.ID + '" name="' + this.name + '" > ' + this.name + ' <span></span></label>';
                $('#product_attribute_list').append(opt);
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function getProductAttributeList() {
    var productAttributeList = [];
    var productAttributeListDisable = [];
    $(".product_attribute").each(function () {
        if (this.checked) {
            var ID = $(this).attr("ID");
            productAttributeList.push(ID);
        } else {
            var ID = $(this).attr("ID");
            productAttributeListDisable.push(ID);
        }
    });
    return { productAttributeList: productAttributeList, productAttributeListDisable: productAttributeListDisable };
}

function createProductVaryantMatrix() {
    var productAttributeList = [];
    var matrixLength = 1;
    $(".product_attribute").each(function () {
        if (this.checked) {
            var ID = $(this).attr("ID");
            var val = $(this).attr("name");
            var items = getProductAttributeOptions(ID);
            items.unshift(val);
            matrixLength = matrixLength * items.length;
            productAttributeList.push(items);
        }
    });

    $('#table_body').empty();
    for (var i = 1; i < matrixLength + 1; i++) {
        var line = createProductVaryantMatrixItem(i, productAttributeList, matrixLength);
        $('#table_body').append(line);
    }
}

function getProductAttributeOptions(product_attribute_id) {
    var result;
    var data = JSON.stringify({ 'product_attribute_id': product_attribute_id });
    callService("ProductAttributeOptionService", 'ListModelAttributeId', data, function (response) {
        if (response.responseCode == 200) {
            result = response.response;
        } else if (response.responseCode == 500) {
        }
    }, function (errormsg) { console.log(errormsg); }, true);
    return result;
}

function createProductVaryantMatrixItem(index, array, totalSize) {
    var total = totalSize;
    var element = '';
    var elementID = '';
    for (var i = 0; i < array.length; i++) {
        var newIndex = (index - 1) % total;

        total = total / (array[i].length - 1);
        var ind = Math.floor((newIndex) / total);
        element += "<div>" + array[i][0] + ":" + array[i][ind + 1].name + "</div>";
        elementID += array[i][ind + 1].ID + "-";
    }
    //element = element.substring(0, element.length - 1);
    elementID = elementID.substring(0, elementID.length - 1);

    var line = `<tr>
                            <td> ` + index + ` </td>
                            <td> ` + element + ` </td>
                            <td><input type="text" class ="form-control" value="0"></td>
                            <td><select class ="form-control" id="input_type_category"><option value='1'>Sabit</option><option value='2'>Yüzde</option></select></td>
                            <td><input type="text" class ="form-control" value="0"></td>
                            <td><input type="text" class ="form-control" value="0"></td>
                            <td><input type="text" class ="form-control" value="0"></td>
                            <td> ` + elementID + ` </td>
                        </tr>`
    return line;
}

function validateSeoLinks() {
    var isSeoValid = validateSeo($('#_name_seo'));
    var isSeoValidEng = validateSeo($('#_name_seo_eng'));
    var isSeoValidOther = validateSeo($('#_name_seo_other'));
    return isSeoValid && isSeoValidEng && isSeoValidOther;
}

function _form_save() {
    var isSeoValid = validateSeoLinks();
    if (!isSeoValid) {
        return;
    }

    var image1 = uploadImage("_image1_val");
    var image2 = uploadImage("_image2_val");
    var image3 = uploadImage("_image3_val");
    var image4 = uploadImage("_image4_val");
    var image5 = uploadImage("_image5_val");
    var image6 = uploadImage("_image6_val");
    var image7 = uploadImage("_image7_val");
    var image8 = uploadImage("_image8_val");

    var attributeList = getProductAttributeList();

    var model = {
        ID: selected,
        name: $('#_name').val(),
        name_eng: $('#_name_eng').val(),
        name_other: $('#_name_other').val(),

        category_id: $('#_category').val(),
        brand_id: $('#_brand').val(),
        supplier_id: $('#_supplier').val(),

        barcode: $('#_barcode').val(),
        stock_code: $('#_stock_code').val(),
        product_code: $('#_product_code').val(),

        name_seo: $('#_name_seo').val(),
        name_seo_eng: $('#_name_seo_eng').val(),
        name_seo_other: $('#_name_seo_other').val(),

        detail: $('#_detail').val(),
        detail_eng: $('#_detail_eng').val(),
        detail_other: $('#_detail_other').val(),

        delivery_info: $('#_delivery_info').val(),
        delivery_info_eng: $('#_delivery_info_eng').val(),
        delivery_info_other: $('#_delivery_info_other').val(),

        stock: $('#_stock').val(),
        desi: $('#_desi').val(),
        delivery_time: $('#_delivery_time').val(),

        buy_price: $('#_buy_price').inputmask('unmaskedvalue') == '' ? 0 : $('#_buy_price').inputmask('unmaskedvalue'),
        sell_price: $('#_sell_price').inputmask('unmaskedvalue') == '' ? 0 : $('#_sell_price').inputmask('unmaskedvalue'),
        discount_price: $('#_discount_price').inputmask('unmaskedvalue') == '' ? 0 : $('#_discount_price').inputmask('unmaskedvalue'),
        currency_id: $('#_currency_id').val(),
        kdv: $('#_kdv').inputmask('unmaskedvalue'),

        gtin: $('#_gtin').val(),
        mpn: $('#_mpn').val(),
        
        free_cargo_product_enabled: $("#_free_cargo_product_enabled").bootstrapSwitch('state') == true ? 1 : 0,
        best_seller_product_enabled: $("#_best_seller_product_enabled").bootstrapSwitch('state') == true ? 1 : 0,
        new_product_enabled: $("#_new_product_enabled").bootstrapSwitch('state') == true ? 1 : 0,
        discounted_product_enabled: $("#_discounted_product_enabled").bootstrapSwitch('state') == true ? 1 : 0,
        special_product_enabled: $("#_special_product_enabled").bootstrapSwitch('state') == true ? 1 : 0,

        long_detail: $('.note-editable').eq(0).html(),
        long_detail_eng: $('.note-editable').eq(1).html(),
        long_detail_other: $('.note-editable').eq(2).html(),

        image1: image1,
        image2: image2,
        image3: image3,
        image4: image4,
        image5: image5,
        image6: image6,
        image7: image7,
        image8: image8,

        seo_title: $('#_seo_title').val(),
        seo_description: $('#_seo_description').val(),
        seo_keywords: $('#_seo_keywords').val(),

        google_ads_type: $('#_google_ads_type').val(),
        google_ads_category: $('#_google_ads_category').val(),
        google_ads_detail: $('#_google_ads_detail').val(),
        facebook_store_enabled: $('#_facebook_store_enabled').val(),

        video1: $('#_video1').val(),
        video2: $('#_video2').val(),
        video3: $('#_video3').val(),

        productAttributeList: attributeList.productAttributeList,
        productAttributeListDisable: attributeList.productAttributeListDisable,

        varyantItemList: productVariantItemList
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function getProductVaryantMatrix() {
    var array = [];;
    $('#table > tbody  > tr').each(function () {
        var code = $(this).find('td:eq(1)').text().trim();
        var codeID = $(this).find('td:eq(7)').text().trim();
        var stock = $(this).find('td:eq(2)').find('input')[0].value;
        var discountType = $(this).find('td:eq(3)').find('select')[0].value;
        var discountPrice = $(this).find('td:eq(4)').find('input')[0].value;
        var price = $(this).find('td:eq(5)').find('input')[0].value;
        var desi = $(this).find('td:eq(6)').find('input')[0].value;

        var model = {
            code: code,
            codeID: codeID,
            stock: stock,
            discountType: discountType,
            discountPrice: discountPrice,
            price: discountPrice,
            desi: desi,
        }
        array.push(model);
        console.log(JSON.stringify(model));
    });
    return array;
}

function addProductVaryantListItem() {
    var product_variant_id = $("#_product_varyant").val();
    var product_variant_item_id = $("#_product_varyant_item").val();
    var product_variant_item_value = $("#_product_varyant_item option:selected").text();
    var stock = $("#_stock_variant").val();
    var stock_code = $("#_stock_code_variant").val();
    var desi = $("#_desi_variant").val();
    var price_type_name = $("#_price_type_variant option:selected").text();
    var price_type = $("#_price_type_variant").val();
    var price = $('#_price_variant').inputmask('unmaskedvalue') == '' ? 0 : $('#_price_variant').inputmask('unmaskedvalue');

    var productVaryantListItem = {
        product_variant_id: product_variant_id,
        product_variant_item_id: product_variant_item_id,
        product_variant_item_value: product_variant_item_value,
        stock: stock,
        stock_code: stock_code,
        desi: desi,
        price_type_name: price_type_name,
        price_type: price_type,
        price: price,
    }

    productVariantItemList.push(productVaryantListItem);
}

function loadProductVaryantListItem() {
    $('#table_body').empty();
    for (var i = 0; i < productVariantItemList.length; i++) {
        var line = createProductVaryantListItem((i + 1), productVariantItemList[i]);
        $('#table_body').append(line);
    }
}

function createProductVaryantListItem(index, varyantItem) {
    var line = `<tr>
                    <td> ` + index + ` </td>
                    <td> ` + varyantItem.product_variant_item_value + ` </td>
                    <td> ` + varyantItem.stock + ` </td>
                    <td> ` + varyantItem.stock_code + ` </td>
                    <td> ` + varyantItem.desi + ` </td>
                    <td> ` + varyantItem.price_type_name + ` </td>
                    <td> ` + varyantItem.price + ` </td>
                    <td><button class="btn-delete" type="button">Delete</button></td>
                </tr>`
    return line;
}