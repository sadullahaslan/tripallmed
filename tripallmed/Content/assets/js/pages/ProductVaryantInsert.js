﻿var Validator = function () {

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 100 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_VARYANTINSERT";
var serviceName = "ProductVaryantService";

function initpage() {

}

function loadProductAttributeList() {
    callService("ProductAttributeService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            $('#product_attribute_list').empty();
            $.each(response.response, function () {
                var opt = '<label class="kt-checkbox"> <input class="product_attribute" type="checkbox" id="' + this.ID + '" name="' + this.name + '" > ' + this.name + ' <span></span></label>';
                $('#product_attribute_list').append(opt);
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function getProductAttributeList() {
    var productAttributeList = [];
    var productAttributeListDisable = [];
    $(".product_attribute").each(function () {
        if (this.checked) {
            var ID = $(this).attr("ID");
            var item = { 'ID': ID };
            productAttributeList.push(item);
        } else {
            var ID = $(this).attr("ID");
            var item = { 'ID': ID };
            productAttributeListDisable.push(item);
        }
    });
    return { productAttributeList: productAttributeList, productAttributeListDisable: productAttributeListDisable };
}

function afterPageLoad() {
    setTimeout(function () {
        var ID = getParameterByName("ID");
        if (ID != null && ID != undefined) {
            $('#_insert_varyant').hide();
            loadSelectedVaryantDetail(ID);
        } else {
            loadProductAttributeList();
        }
    }, 500);

    $('#btn_create_varyant').on('click', function (e) {
        e.preventDefault();
        createProductVaryantMatrix();
    });
}

function createProductVaryantMatrix() {
    var productAttributeList = [];
    var matrixLength = 1;
    $(".product_attribute").each(function () {
        if (this.checked) {
            var ID = $(this).attr("ID");
            var val = $(this).attr("name");
            var items = getProductAttributeOptions(ID);
            matrixLength = matrixLength * items.length;
            items.unshift(val);
            productAttributeList.push(items);
        }
    });

    $('#table_body').empty();
    for (var i = 1; i < matrixLength + 1; i++) {
        var line = createProductVaryantMatrixItem(i, productAttributeList, matrixLength);
        $('#table_body').append(line);
    }
}

function getProductAttributeOptions(product_attribute_id) {
    var result;
    var data = JSON.stringify({ 'product_attribute_id': product_attribute_id });
    callService("ProductAttributeOptionService", 'ListModelAttributeId', data, function (response) {
        if (response.responseCode == 200) {
            result = response.response;
        } else if (response.responseCode == 500) {
        }
    }, function (errormsg) { console.log(errormsg); }, true);
    return result;
}

function createProductVaryantMatrixItem(index, array, totalSize) {
    var total = totalSize;
    var element = '';
    var elementID = '';
    for (var i = 0; i < array.length; i++) {
        var newIndex = (index - 1) % total;

        total = total / (array[i].length - 1);
        var ind = Math.floor((newIndex) / total);
        element += "-<span style='padding-right:20px'>" + array[i][0] + ":" + array[i][ind + 1].name + "</span>";
        elementID += array[i][ind + 1].ID + "-";
    }
    elementID = elementID.substring(0, elementID.length - 1);

    var line = `<tr>
                    <td> ` + index + ` </td>
                    <td> ` + element + ` </td>
                    <td> ` + elementID + ` </td>
                </tr>`
    return line;
}

function _form_save() {
    var attributeList = getProductAttributeList();
    var itemList = getProductVaryantItemList();
    var model = {
        ID: selected,
        name: $('#_name').val(),
        attributeList: attributeList.productAttributeList,
        itemList: itemList
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                window.location.href = "/Panel/Pages/Catalog/ProductVaryantList.aspx";
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    $('#_header').val("");
    $('#_header_eng').val("");
    $('#_header_other').val("");
    $('#_seo').val("");
    $('#_seo_eng').val("");
    $('#_seo_other').val("");
    $('#_summary').val("");
    $('#_summary_eng').val("");
    $('#_summary_other').val("");
    setImageSrc("_image1_val", "/Data/nocontent.png");
    setImageSrc("_image2_val", "/Data/nocontent.png");
    setImageSrc("_image3_val", "/Data/nocontent.png");
    $('.note-editable').eq(0).html("");
    $('.note-editable').eq(1).html("");
    $('.note-editable').eq(2).html("");
}

function getProductVaryantItemList() {
    var array = [];;
    $('#tableVaryant > tbody  > tr').each(function () {
        var number = $(this).find('td:eq(0)').text().trim();
        var detail = $(this).find('td:eq(1)').text().trim();
        var code = $(this).find('td:eq(2)').text().trim();

        var model = {
            code: code,
            value: detail
        }
        array.push(model);
    });
    return array;
}

function loadSelectedVaryantDetail(product_variant_id) {
    var data = JSON.stringify({ 'product_variant_id': product_variant_id });
    callService("ProductVaryantService", 'ListModelVariantItem', data, function (response) {
        if (response.responseCode == 200) {
            $('#table_body').empty();
            var index = 1;
            $.each(response.response, function () {
                var valueList = this.value.split("-");
                var value = "";
                $.each(valueList, function () {
                    value += "<span style='padding-right:20px'>" + this + "</span>";
                });
                
                var line = `<tr>
                    <td> ` + index + ` </td>
                    <td> ` + value + ` </td>
                    <td> ` + this.code + ` </td>
                </tr>`
                $('#table_body').append(line);
                index++;
            });
        } else if (response.responseCode == 500) {

        }
    }, function (errormsg) { console.log(errormsg); }, false);
}


