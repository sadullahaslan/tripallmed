﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
        $('#_person').on('input', function () { validator.element($(this)); });
        $('#_email').on('input', function () { validator.element($(this)); });
        $('#_phone').on('input', function () { validator.element($(this)); });
        $('#_phone2').on('input', function () { validator.element($(this)); });
        $('#_fax').on('input', function () { validator.element($(this)); });
        $('#_webAddress').on('input', function () { validator.element($(this)); });
        $('#_accountInfo').on('input', function () { validator.element($(this)); });
        $('#_taxNumber').on('input', function () { validator.element($(this)); });
        $('#_taxOffice').on('input', function () { validator.element($(this)); });
        $('#_address').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 255 },
                _person: { required: false, minlength: 0, maxlength: 255 },
				_email: { required: false, minlength: 0, maxlength: 50 },
				_phone: { required: false, minlength: 0, maxlength: 50 },
				_phone2: { required: false, minlength: 0, maxlength: 50 },
				_fax: { required: false, minlength: 0, maxlength: 50 },
				_webAddress: { required: false, minlength: 0, maxlength: 50 },
				_accountInfo: { required: false, minlength: 0, maxlength: 50 },
				_taxNumber: { required: false, minlength: 0, maxlength: 50 },
				_taxOffice: { required: false, minlength: 0, maxlength: 50 },
				_address: { required: false, minlength: 0, maxlength: 255 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_SUPPLIERLIST";
var serviceName = "SupplierService";

function initpage() {
    iniDataTableAttributes();
    loadData();
}

function afterPageLoad() {

}

function addEventListener() {

}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        { data: "name" },
        { data: "person" },
        { data: "email" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="edit" action-handler="bindModel" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="delete" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                clearAndShowForm();
            }
        },
        {
            text: getLocaliziedValue("TABLE_export"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                exportToCsv();
            }
        }
    ];
}

function loadData() {
    var data = getTableData();
    initDataTable(data, columns, buttons, orderHandler);
}

function getTableData() {
    var data;
    getItemsSynch(serviceName, null, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
    return data;
}

function bindModel(model) {
    selected = model.ID;
    $('#_name').val(model.name);
    $('#_person').val(model.person);
    $('#_email').val(model.email);
    $('#_phone').val(model.phone);
    $('#_phone2').val(model.phone2);
    $('#_fax').val(model.fax);
    $('#_webAddress').val(model.webAddress);
    $('#_accountInfo').val(model.accountInfo);
    $('#_taxNumber').val(model.taxNumber);
    $('#_taxOffice').val(model.taxOffice);
    $('#_taxNumber').val(model.taxNumber);
    $('#_address').val(model.address);

    $('#_modalForm').modal('show');
}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var model = {
        ID: selected,
        name: $('#_name').val(),
        person: $('#_person').val(),
        email: $('#_email').val(),
        phone: $('#_phone').val(),
        phone2: $('#_phone2').val(),
        fax: $('#_fax').val(),
        webAddress: $('#_webAddress').val(),
        accountInfo: $('#_accountInfo').val(),
        taxNumber: $('#_taxNumber').val(),
        taxOffice: $('#_taxOffice').val(),
        address: $('#_address').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    selected = 0;
    $('#_name').val("");
    $('#_person').val("");
    $('#_email').val("");
    $('#_phone').val("");
    $('#_phone2').val("");
    $('#_fax').val("");
    $('#_webAddress').val("");
    $('#_accountInfo').val("");
    $('#_taxNumber').val("");
    $('#_taxOffice').val("");
    $('#_taxNumber').val("");
    $('#_address').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}

function orderHandler() {
    loadData();
}