﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_store_number').on('input', function () { validator.element($(this)); });
        $('#_api_username').on('input', function () { validator.element($(this)); });
        $('#_api_password').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _store_number: { required: true, minlength: 1, maxlength: 50 },
                _api_username: { required: true, minlength: 1, maxlength: 50 },
                _api_password: { required: true, minlength: 1, maxlength: 50 },
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_CREDITCARS";
var serviceName = "CreditCardSettingsService";

function initpage() {
    iniDataTableAttributes();
    loadData();
}

function afterPageLoad() {
}

function addEventListener() {

}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        {
            data: null, render: function (data, type, row) {
                return `<img src="/Panel/assets/media/banks/` + data.card_image + `" class="card-img-top-custom">`;
            },
            className: "center",
            orderable: false
        },
        { data: "bank_name" },
        { data: "card_name" },
        {
            data: null, render: function (data, type, row) {
                var isDefault = data.default_option ?
                    `<span><span class="btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_default_option") + `</span></span>` :
                    ``;
                return isDefault;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="custom" action-handler="doDefault" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_default") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="edit" action-handler="bindModel" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [];
}

function loadData() {
    var data = getTableData();
    initDataTable(data, columns, buttons, orderHandler, 20);
}

function getTableData() {
    var data;
    getItemsSynch(serviceName, null, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
    return data;
}

function bindModel(model) {
    selected = model.ID;
    $('#_store_number').val(model.store_number);
    $('#_api_username').val(model.api_username);
    $('#_api_password').val(model.api_password);
    $('#_modalForm').modal('show');
}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function doDefault(parameter) {
    var data = JSON.stringify({ ID: parameter });
    callService(serviceName, 'DoDefault', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var model = {
        ID: selected,
        store_number: $('#_store_number').val(),
        api_username: $('#_api_username').val(),
        api_password: $('#_api_password').val(),
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    selected = 0;
    $('#_store_number').val("");
    $('#_api_username').val("");
    $('#_api_password').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}

function orderHandler() {
    loadData();
}