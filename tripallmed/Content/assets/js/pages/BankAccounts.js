﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_bank_name').on('input', function () { validator.element($(this)); });
        $('#_branch_name').on('input', function () { validator.element($(this)); });
        $('#_branch_code').on('input', function () { validator.element($(this)); });
        $('#_account_number').on('input', function () { validator.element($(this)); });
        $('#_iban').on('input', function () { validator.element($(this)); });
        $('#_currency_id').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _bank_name: { required: true, minlength: 1, maxlength: 50 },
                _branch_name: { required: true, minlength: 1, maxlength: 50 },
                _branch_code: { required: true, minlength: 1, maxlength: 50 },
                _account_number: { required: true, minlength: 1, maxlength: 50 },
                _iban: { required: true, minlength: 1, maxlength: 50 },
                _currency_id: { required: true, minlength: 1},
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_BANKACCOUNTS";
var serviceName = "BankAccountService";

function initpage() {
    iniDataTableAttributes();
    loadData();
}

function afterPageLoad() {
    loadBankList();
    $("#_currency_id").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
}

function addEventListener() {

}

function loadBankList() {
    getItemsSynch('BankService', null, function (response) {
        var data = prepareSelectData(response.response);
        $("#_bank_id").select2().empty();
        $("#_bank_id").select2({ data: data });
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        { data: "bank_name" },
        { data: "branch_name" },
        { data: "account_number" },
        { data: "iban" },
        { data: "currency_name" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="edit" action-handler="bindModel" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="delete" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                clearAndShowForm();
            }
        },
        {
            text: getLocaliziedValue("TABLE_export"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                exportToCsv();
            }
        }
    ];
}

function loadData() {
    var data = getTableData();
    initDataTable(data, columns, buttons, orderHandler);
}

function getTableData() {
    var data;
    getItemsSynch(serviceName, null, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
    return data;
}

function bindModel(model) {
    selected = model.ID;
    $('#_bank_id').val(model.bank_id).trigger('change');
    $('#_branch_name').val(model.branch_name);
    $('#_branch_code').val(model.branch_code);
    $('#_account_number').val(model.account_number);
    $('#_iban').val(model.iban); 
    $("#_currency_id").val(model.currency_id).trigger('change');
    $('#_modalForm').modal('show');
}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var model = {
        ID: selected,
        bank_id: $('#_bank_id').val(),
        branch_name: $('#_branch_name').val(),
        branch_code: $('#_branch_code').val(),
        account_number: $('#_account_number').val(),
        iban: $('#_iban').val(),
        currency_id: $('#_currency_id').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    selected = 0;
    $('#_bank_id').val("");
    $('#_branch_name').val("");
    $('#_branch_code').val("");
    $('#_account_number').val("");
    $('#_iban').val("");
    $("#_currency_id").val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}

function orderHandler() {
    loadData();
}