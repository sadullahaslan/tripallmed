﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_PAYMENTSETTINGS";
var serviceName = "SettingPaymentService";

function initpage() {
}

function afterPageLoad() {
    loadData();
    $("#_havale_enabled").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
    $("#_credit_card_enabled").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
    $("#_credit_card_installment_enabled").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
    $("#_door_payment_enabled").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
}

function addEventListener() {

}

function loadData() {
    callService(serviceName, 'LoadModelExact', null, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;
    $("#_havale_enabled").val(model.havale_enabled).trigger('change');
    $("#_credit_card_enabled").val(model.credit_card_enabled).trigger('change');
    $("#_credit_card_installment_enabled").val(model.credit_card_installment_enabled).trigger('change');
    $("#_door_payment_enabled").val(model.door_payment_enabled).trigger('change');
    $('#_modalForm').modal('show');
}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var model = {
        ID: selected,
        havale_enabled: $('#_havale_enabled').val(),
        credit_card_enabled: $('#_credit_card_enabled').val(),
        credit_card_installment_enabled: $('#_credit_card_installment_enabled').val(),
        door_payment_enabled: $('#_door_payment_enabled').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    $('#_havale_enabled').val("");
    $('#_credit_card_enabled').val("");
    $('#_credit_card_installment_enabled').val("");
    $('#_door_payment_enabled').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}