﻿var app = new Application();

$(function () {
    $("#_btnRegister").click(function (e) {
        e.preventDefault();
        login();
    });
});

function login() {
    var name = $('#_name').val();
    var surname = $('#_surname').val();
    var email = $('#_email').val();
    var phone = $('#_phone').val();
    var sector = $('#_sector').val();
    var password = $('#_password').val();

    var admin = {
        name: name,
        surname: surname,
        email: email,
        phone: phone,
        sector: sector,
        password: password
    }
    var data = JSON.stringify({ 'admin': admin});
    var selectedService = 'RegisterService';
    var selectedFunction = 'RegisterAdmin';
    callService(selectedService, selectedFunction, data, succces_handler, function (errormsg) { alertLoginOperation(); }, false);
}

function succces_handler(response) {
    if (response.responseCode == 200) {
        window.localStorage.setItem('admin', response.response);
        window.location.href = "/Panel/Profile.aspx";
    } else if (response.responseCode == 500) {
        var errorMsg = getLocaliziedValue(response.errorCode);
        alertFail(getLocaliziedValue("login_errorTitle"), errorMsg);
    }
}

function forgotPassword() {
    var email = $('#input_email_forgot_password').val();
    var url = '/Service/LoginService.svc/ForgotPasswordAdmin';
    var data = JSON.stringify({ 'email': email });

    app.serviceManager.callService(url, data, function (data) {
        alertSuccess("Yeni şifreniz email adresinize gönderilmiştir.");
    }, function (errormsg) {
        alertFail("Kayıtlı email adresi bulunamadı.");
    }, true);
}

// Class Initialization
jQuery(document).ready(function () {    
    select.selectpicker('refresh');
});
