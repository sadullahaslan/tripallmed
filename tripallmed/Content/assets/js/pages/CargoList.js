﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
        $('#_link').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 50 },
                _link: { required: true, minlength: 1, maxlength: 50 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_CARGOLIST";
var serviceName = "CargoService";

function initpage() {
    iniDataTableAttributes();
    loadData();
    initSwitches();
    initMasks();
    initSelect();
}

function afterPageLoad() {

}

function addEventListener() {

}

function initSelect() {
    $("#_currency").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
}

function initSwitches() {
    $("[data-switch=true]").bootstrapSwitch();
}

function initMasks() {
    $("#_additional_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_free_cargo_limit").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_fixed_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_delivery_payment_cash_service_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_delivery_payment_cash_service_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_delivery_payment_cash_max_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_delivery_payment_credit_card_service_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_delivery_payment_credit_card_min_price").inputmask("999.999.999,99", { numericInput: !0 });
    $("#_delivery_payment_credit_card_max_price").inputmask("999.999.999,99", { numericInput: !0 });
}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        { data: "name" },
        { data: "link" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="edit" action-handler="bindModel" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="delete" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                clearAndShowForm();
            }
        },
        {
            text: getLocaliziedValue("TABLE_export"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                exportToCsv();
            }
        }
    ];
}

function loadData() {
    var data = getTableData();
    initDataTable(data, columns, buttons, orderHandler);
}

function getTableData() {
    var data;
    getItemsSynch(serviceName, null, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
    return data;
}

function bindModel(model) {
    $('#_modalForm').modal('show');

    setTimeout(function () {

        selected = model.ID;
        $('#_name').val(model.name);
        $('#_link').val(model.link);
        $('#_additional_price').val(model.additional_price);
        $('#_free_cargo_limit').val(model.free_cargo_limit);
        $('#_free_desi_limit').val(model.free_desi_limit);
        $("#_fixed_price_enabled").bootstrapSwitch('state', model.fixed_price_enabled == 1 ? true : false);
        console.log($("#_fixed_price_enabled").bootstrapSwitch('state'));

        $('#_fixed_price').val(model.fixed_price);
        $('#_currency_abroad').val(model.currency_abroad);

        $("#_delivery_payment_cash_enabled").bootstrapSwitch('state', model.delivery_payment_cash_enabled == 1 ? true : false);
        console.log($("#_delivery_payment_cash_enabled").bootstrapSwitch('state'));

        $('#_delivery_payment_cash_service_price').val(model.delivery_payment_cash_service_price);
        $('#_delivery_payment_cash_min_price').val(model.delivery_payment_cash_min_price);
        $('#_delivery_payment_cash_max_price').val(model.delivery_payment_cash_max_price);

        $("#_delivery_payment_credit_card_enabled").bootstrapSwitch('state', model.delivery_payment_credit_card_enabled == 1 ? true : false);
        console.log($("#_delivery_payment_credit_card_enabled").bootstrapSwitch('state'));

        $('#_delivery_payment_credit_card_service_price').val(model.delivery_payment_credit_card_service_price);
        $('#_delivery_payment_credit_card_min_price').val(model.delivery_payment_credit_card_min_price);
        $('#_delivery_payment_credit_card_max_price').val(model.delivery_payment_credit_card_max_price);

        bindRegionModel(model.marmara, 'marmara');
        bindRegionModel(model.karadeniz, 'karadeniz');
        bindRegionModel(model.ege, 'ege');
        bindRegionModel(model.akdeniz, 'akdeniz');
        bindRegionModel(model.icanadolu, 'icanadolu');
        bindRegionModel(model.doguanadolu, 'doguanadolu');
        bindRegionModel(model.guneydoguanadolu, 'guneydoguanadolu');
    }, 500);

}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {

    var marmara = createRegionModel('marmara');
    var karadeniz = createRegionModel('karadeniz');
    var ege = createRegionModel('ege');
    var akdeniz = createRegionModel('akdeniz');
    var icanadolu = createRegionModel('icanadolu');
    var doguanadolu = createRegionModel('doguanadolu');
    var guneydoguanadolu = createRegionModel('guneydoguanadolu');

    var model = {
        ID: selected,
        name: $('#_name').val(),
        link: $('#_link').val(),
        additional_price: $('#_additional_price').inputmask('unmaskedvalue'),
        free_cargo_limit: $('#_free_cargo_limit').inputmask('unmaskedvalue'),
        free_desi_limit: $('#_free_desi_limit').val(),
        fixed_price_enabled: $("#_fixed_price_enabled").bootstrapSwitch('state') == true ? 1 : 0,
        fixed_price: $('#_fixed_price').inputmask('unmaskedvalue'),
        currency_abroad: $('#_currency_abroad').val(),
        delivery_payment_cash_enabled: $("#_delivery_payment_cash_enabled").bootstrapSwitch('state') == true ? 1 : 0,
        delivery_payment_cash_service_price: $('#_delivery_payment_cash_service_price').inputmask('unmaskedvalue'),
        delivery_payment_cash_min_price: $('#_delivery_payment_cash_min_price').inputmask('unmaskedvalue'),
        delivery_payment_cash_max_price: $('#_delivery_payment_cash_max_price').inputmask('unmaskedvalue'),
        delivery_payment_credit_card_enabled: $("#_delivery_payment_credit_card_enabled").bootstrapSwitch('state') == true ? 1 : 0,
        delivery_payment_credit_card_service_price: $('#_delivery_payment_credit_card_service_price').inputmask('unmaskedvalue'),
        delivery_payment_credit_card_min_price: $('#_delivery_payment_credit_card_min_price').inputmask('unmaskedvalue'),
        delivery_payment_credit_card_max_price: $('#_delivery_payment_credit_card_max_price').inputmask('unmaskedvalue'),
        marmara: marmara,
        karadeniz: karadeniz,
        ege: ege,
        akdeniz: akdeniz,
        icanadolu: icanadolu,
        doguanadolu: doguanadolu,
        guneydoguanadolu: guneydoguanadolu,
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    selected = 0;
    $('#_name').val("");
    $('#_surname').val("");
    $('#_email').val("");
    $('#_phone').val("");
    $('#_password').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}

function orderHandler() {
    loadData();
}

function bindRegionModel(model, region) {
    $('#_deci_0_' + region).val(model['deci_0']);
    $('#_deci_1_' + region).val(model['deci_1']);
    $('#_deci_2_' + region).val(model['deci_2']);
    $('#_deci_3_' + region).val(model['deci_3']);
    $('#_deci_4_' + region).val(model['deci_4']);
    $('#_deci_5_' + region).val(model['deci_5']);
    $('#_deci_6_' + region).val(model['deci_6']);
    $('#_deci_7_' + region).val(model['deci_7']);
    $('#_deci_8_' + region).val(model['deci_8']);
    $('#_deci_9_' + region).val(model['deci_9']);
    $('#_deci_10_' + region).val(model['deci_10']);
    $('#_deci_11_' + region).val(model['deci_11']);
    $('#_deci_12_' + region).val(model['deci_12']);
    $('#_deci_13_' + region).val(model['deci_13']);
    $('#_deci_14_' + region).val(model['deci_14']);
    $('#_deci_15_' + region).val(model['deci_15']);
    $('#_deci_16_' + region).val(model['deci_16']);
    $('#_deci_17_' + region).val(model['deci_17']);
    $('#_deci_18_' + region).val(model['deci_18']);
    $('#_deci_19_' + region).val(model['deci_19']);
    $('#_deci_20_' + region).val(model['deci_20']);
    $('#_deci_21_' + region).val(model['deci_21']);
    $('#_deci_22_' + region).val(model['deci_22']);
    $('#_deci_23_' + region).val(model['deci_23']);
    $('#_deci_24_' + region).val(model['deci_24']);
    $('#_deci_25_' + region).val(model['deci_25']);
    $('#_deci_26_' + region).val(model['deci_26']);
    $('#_deci_27_' + region).val(model['deci_27']);
    $('#_deci_28_' + region).val(model['deci_28']);
    $('#_deci_29_' + region).val(model['deci_29']);
    $('#_deci_30_' + region).val(model['deci_30']);
    $('#_deci_31_' + region).val(model['deci_31']);
    $('#_deci_32_' + region).val(model['deci_32']);
    $('#_deci_33_' + region).val(model['deci_33']);
    $('#_deci_34_' + region).val(model['deci_34']);
    $('#_deci_35_' + region).val(model['deci_35']);
    $('#_deci_36_' + region).val(model['deci_36']);
    $('#_deci_37_' + region).val(model['deci_37']);
    $('#_deci_38_' + region).val(model['deci_38']);
    $('#_deci_39_' + region).val(model['deci_39']);
    $('#_deci_40_' + region).val(model['deci_40']);
    $('#_deci_41_' + region).val(model['deci_41']);
    $('#_deci_42_' + region).val(model['deci_42']);
    $('#_deci_43_' + region).val(model['deci_43']);
    $('#_deci_44_' + region).val(model['deci_44']);
    $('#_deci_45_' + region).val(model['deci_45']);
    $('#_deci_50_' + region).val(model['deci_50']);
    $('#_deci_100_' + region).val(model['deci_100']);
}

function createRegionModel(region) {
    var model = {
        deci_0: $('#_deci_0_' + region).val(),
        deci_1: $('#_deci_1_' + region).val(),
        deci_2: $('#_deci_2_' + region).val(),
        deci_3: $('#_deci_3_' + region).val(),
        deci_4: $('#_deci_4_' + region).val(),
        deci_5: $('#_deci_5_' + region).val(),
        deci_6: $('#_deci_6_' + region).val(),
        deci_7: $('#_deci_7_' + region).val(),
        deci_8: $('#_deci_8_' + region).val(),
        deci_9: $('#_deci_9_' + region).val(),

        deci_10: $('#_deci_10_' + region).val(),
        deci_11: $('#_deci_11_' + region).val(),
        deci_12: $('#_deci_12_' + region).val(),
        deci_13: $('#_deci_13_' + region).val(),
        deci_14: $('#_deci_14_' + region).val(),
        deci_15: $('#_deci_15_' + region).val(),
        deci_16: $('#_deci_16_' + region).val(),
        deci_17: $('#_deci_17_' + region).val(),
        deci_18: $('#_deci_18_' + region).val(),
        deci_19: $('#_deci_19_' + region).val(),

        deci_20: $('#_deci_20_' + region).val(),
        deci_21: $('#_deci_21_' + region).val(),
        deci_22: $('#_deci_22_' + region).val(),
        deci_23: $('#_deci_23_' + region).val(),
        deci_24: $('#_deci_24_' + region).val(),
        deci_25: $('#_deci_25_' + region).val(),
        deci_26: $('#_deci_26_' + region).val(),
        deci_27: $('#_deci_27_' + region).val(),
        deci_28: $('#_deci_28_' + region).val(),
        deci_29: $('#_deci_29_' + region).val(),

        deci_30: $('#_deci_30_' + region).val(),
        deci_31: $('#_deci_31_' + region).val(),
        deci_32: $('#_deci_32_' + region).val(),
        deci_33: $('#_deci_33_' + region).val(),
        deci_34: $('#_deci_34_' + region).val(),
        deci_35: $('#_deci_35_' + region).val(),
        deci_36: $('#_deci_36_' + region).val(),
        deci_37: $('#_deci_37_' + region).val(),
        deci_38: $('#_deci_38_' + region).val(),
        deci_39: $('#_deci_39_' + region).val(),

        deci_40: $('#_deci_40_' + region).val(),
        deci_41: $('#_deci_41_' + region).val(),
        deci_42: $('#_deci_42_' + region).val(),
        deci_43: $('#_deci_43_' + region).val(),
        deci_44: $('#_deci_44_' + region).val(),
        deci_45: $('#_deci_45_' + region).val(),
        deci_46: 0,
        deci_47: 0,
        deci_48: 0,
        deci_49: 0,
        deci_50: $('#_deci_50_' + region).val(),
        deci_100: $('#_deci_100_' + region).val(),
    }
    return model;
}