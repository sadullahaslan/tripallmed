﻿var Validator = function () {

    var validator;

    var initWidgets = function () {

    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var pageNameKey = "PAGENAME_FACEBOOKFEED";
var serviceName = "GeneralService";

function initpage() {
}

function afterPageLoad() {
}

function addEventListener() {
    setTimeout(function () {
        createFeed();
    }, 500);
}

function createFeed() {
    callService(serviceName, 'CreateFacebookXmlFeed', null, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(content) {
    console.log(content);
    var beat = vkbeautify.xml(content);
    $('#_content').val(beat);
}

function _form_save() {
    callService(serviceName, 'SaveFacebookXmlFeed', null, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}