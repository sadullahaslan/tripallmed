﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_address').on('input', function () { validator.element($(this)); });
        $('#_password').on('input', function () { validator.element($(this)); });
        $('#_server').on('input', function () { validator.element($(this)); });
        $('#_port').on('input', function () { validator.element($(this)); });
        $('#_order_address').on('input', function () { validator.element($(this)); });
        $('#_other_address').on('input', function () { validator.element($(this)); });
        $('#_admin_address1').on('input', function () { validator.element($(this)); });
        $('#_admin_address2').on('input', function () { validator.element($(this)); });
        $('#_connection_type').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _address: { required: true, minlength: 1, maxlength: 50 },
                _password: { required: true, minlength: 1, maxlength: 50 },
                _server: { required: true, minlength: 1, maxlength: 50 },
                _port: { required: true, minlength: 1, maxlength: 50 },
                _order_address: { required: true, minlength: 1, maxlength: 50 },
                _other_address: { required: true, minlength: 1, maxlength: 50 },
                _admin_address1: { required: true, minlength: 1, maxlength: 50 },
                _admin_address2: { required: false, minlength: 1, maxlength: 50 },
                _connection_type: { required: true, minlength: 1, maxlength: 50 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_SITESETTINGS";
var serviceName = "SettingSiteService";

function initpage() {
}

function afterPageLoad() {
    loadData();
}

function addEventListener() {

}

function loadData() {
    callService(serviceName, 'LoadModelExact', null, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;
    $('#_url').val(model.url);
    $('#_modalForm').modal('show');
}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var model = {
        ID: selected,
        url: $('#_url').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    $('#_url').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}