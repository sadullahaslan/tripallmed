﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_header').on('input', function () { validator.element($(this)); });
        $('#_header_eng').on('input', function () { validator.element($(this)); });
        $('#_header_other').on('input', function () { validator.element($(this)); });
        $('#_summary').on('input', function () { validator.element($(this)); });
        $('#_summary_eng').on('input', function () { validator.element($(this)); });
        $('#_summary_other').on('input', function () { validator.element($(this)); });
        $('#_seo').on('input', function () { validator.element($(this)); });
        $('#_seo_eng').on('input', function () { validator.element($(this)); });
        $('#_seo_other').on('input', function () { validator.element($(this)); });
        $('#_detail').on('input', function () { validator.element($(this)); });
        $('#_detail_eng').on('input', function () { validator.element($(this)); });
        $('#_detail_other').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _header: { required: true, minlength: 1, maxlength: 100 },
                _header_eng: { required: true, minlength: 1, maxlength: 100 },
                _header_other: { required: true, minlength: 1, maxlength: 100 },
                _seo: { required: true, minlength: 1, maxlength: 200 },
                _seo_eng: { required: true, minlength: 1, maxlength: 200 },
                _seo_other: { required: true, minlength: 1, maxlength: 200 },
                _detail: { required: true, minlength: 1 },
                _detail_eng: { required: true, minlength: 1 },
                _detail_other: { required: true, minlength: 1 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
        console.log(validator);
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_BLOGDETAIL";
var serviceName = "BlogService";

function initpage() {
    new KTAvatar("_image1");
    new KTAvatar("_image2");
    new KTAvatar("_image3");
    $('.summenote-sm').summernote({ height: 300 });
}

function afterPageLoad() {
    setTimeout(function () {
        var ID = getParameterByName("ID");
        loadData(ID);
    }, 1000)
}

function addEventListener() {

}

function loadData(ID) {
    var data = JSON.stringify({ ID: ID });
    callService(serviceName, 'LoadModel', data, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;
    $('#_header').val(model.header);
    $('#_header_eng').val(model.header_eng);
    $('#_header_other').val(model.header_other);
    $('#_seo').val(model.seo);
    $('#_seo_eng').val(model.seo_eng);
    $('#_seo_other').val(model.seo_other);
    $('#_summary').val(model.summary);
    $('#_summary_eng').val(model.summary_eng);
    $('#_summary_other').val(model.summary_other);
    $('.note-editable').eq(0).html(model.detail); 
    $('.note-editable').eq(1).html(model.detail_eng);
    $('.note-editable').eq(2).html(model.detail_other);
    setImageSrc("_image1_val", "/Data/" + model.image);
    setImageSrc("_image2_val", "/Data/" + model.image_eng);
    setImageSrc("_image3_val", "/Data/" + model.image_other);
}

function _form_save() {
    var image = uploadImage("_image1_val");
    var image_eng = uploadImage("_image2_val");
    var image_other = uploadImage("_image3_val");

    var model = {
        ID: selected,
        header: $('#_header').val(),
        header_eng: $('#_header_eng').val(),
        header_other: $('#_header_other').val(),
        seo: $('#_seo').val(),
        seo_eng: $('#_seo_eng').val(),
        seo_other: $('#_seo_other').val(),
        summary: $('#_summary').val(),
        summary_eng: $('#_summary_eng').val(),
        summary_other: $('#_summary_other').val(),
        detail: $('.note-editable').eq(0).html(),
        detail_eng: $('.note-editable').eq(1).html(),
        detail_other: $('.note-editable').eq(2).html(),
        image: image,
        image_eng: image_eng,
        image_other: image_other,
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                window.location.href = "/Panel/Pages/Marketting/BlogList.aspx";
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    $('#_header').val("");
    $('#_header_eng').val("");
    $('#_header_other').val("");
    $('#_seo').val("");
    $('#_seo_eng').val("");
    $('#_seo_other').val("");
    $('#_summary').val("");
    $('#_summary_eng').val("");
    $('#_summary_other').val("");
    setImageSrc("_image1_val", "/Data/nocontent.png");
    setImageSrc("_image2_val", "/Data/nocontent.png");
    setImageSrc("_image3_val", "/Data/nocontent.png");
    $('.note-editable').eq(0).html("");
    $('.note-editable').eq(1).html("");
    $('.note-editable').eq(2).html("");
}