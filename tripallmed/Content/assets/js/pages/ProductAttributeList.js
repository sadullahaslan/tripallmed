﻿var Validator = function () {

    var validator;

    var validator_options;

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
        $('#_name_eng').on('input', function () { validator.element($(this)); });
        $('#_name_other').on('input', function () { validator.element($(this)); });
        $('#_type').on('input', function () { validator.element($(this)); });
    }

    var initWidgets_options = function () {
        $('#_name_options').on('input', function () { validator_options.element($(this)); });
        $('#_name_eng_options').on('input', function () { validator_options.element($(this)); });
        $('#_name_other_options').on('input', function () { validator_options.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 50 },
                _name_eng: { required: true, minlength: 1, maxlength: 50 },
                _name_other: { required: true, minlength: 1, maxlength: 50 },
                _type: { required: true, minlength: 1, maxlength: 200 },
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    };

    var initValidation_options = function () {
        validator_options = $("#_form_options").validate({
            rules: {
                _name_options: { required: true, minlength: 1, maxlength: 50 },
                _name_eng: { required: false, minlength: 0, maxlength: 50 },
                _name_other: { required: false, minlength: 0, maxlength: 50 },
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();

            initWidgets_options();
            initValidation_options();
        }
    };
}();

var selected = 0;
var selected_attribute = 0;
var selected_options = 0;
var pageNameKey = "PAGENAME_ATTRIBUTELIST";
var serviceName = "ProductAttributeService";
var table_options;
var columns_options;

function initpage() {
    iniDataTableAttributes();
    loadData();
}

function afterPageLoad() {
    $("#_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
}

function addEventListener() {

}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        { data: "name" },
        { data: "name_eng" },
        { data: "option_type_name" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="custom" action-handler="loadOptions" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_options") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="edit" action-handler="bindModel" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="delete" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                clearAndShowForm();
            }
        },
        {
            text: getLocaliziedValue("TABLE_export"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                exportToCsv();
            }
        }
    ];
    columns_options = [
        { data: "ID" },
        { data: "name" },
        { data: "name_eng" },
        { data: "name_other" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-handler="activeHandler_options" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-handler="activeHandler_options" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButtonOptions` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButtonOptions` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="custom" action-handler="editHandler_options" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="custom" action-handler="deleteHandler_options" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
}

function loadData() {
    var data = getTableData();
    initDataTable(data, columns, buttons, orderHandler);
}

function getTableData() {
    var data;
    getItemsSynch(serviceName, null, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
    return data;
}

function bindModel(model) {
    selected = model.ID;
    $('#_name').val(model.name);
    $('#_name_eng').val(model.name_eng);
    $('#_name_other').val(model.name_other);
    $("#_type").val(model.option_type_id).trigger('change');
    $('#_modalForm').modal('show');
}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var model = {
        ID: selected,
        name: $('#_name').val(),
        name_eng: $('#_name_eng').val(),
        name_other: $('#_name_other').val(),
        option_type_id: $('#_type').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    selected = 0;
    $('#_name').val("");
    $('#_name_eng').val("");
    $('#_name_other').val("");
    $('#_type').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}

function orderHandler() {
    loadData();
}


//****///
function loadOptions(parameter) {
    $('#_modalForm_options').modal('show');
    selected_attribute = parameter;
    loadAttributeOptions(selected_attribute);
}

function loadAttributeOptions(parameter) {
    var data = JSON.stringify({ product_attribute_id: parameter });
    getItemsAsynch("ProductAttributeOptionService", data, function (response) {
        if (response.responseCode == 200) {
            var data = response.response;
            initAttributeOptionsTable(data, columns_options, []);
        }
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
}

function initAttributeOptionsTable(data, columns, buttons) {
    table_options = $('#_tableForm_options').DataTable({
        "language": {
            "lengthMenu": getLocaliziedValue("datatableButton_perPage"),
            "zeroRecords": getLocaliziedValue("datatableButton_noRecord"),
            "info": getLocaliziedValue("datatableButton_activePage") + ": _PAGE_ / _PAGES_",
            "infoEmpty": getLocaliziedValue("datatableButton_noData"),
            "infoFiltered": "",
        },
        "aaSorting": [],
        data: data,
        columns: columns,
        dom: 'Bfrtip',
        buttons: buttons,
        autoWidth: false,
        rowReorder: true,
        "destroy": true,
        searching: false,
        bFilter: false,
        pageLength: 3
    });

    table_options.on('row-reorder', function (e, diff, edit) {
        var parameter = [];
        for (var i = 0; i < diff.length; i++) {
            var item = diff[i];
            var ID = item.node.getElementsByTagName('td')[0].textContent
            var newPosition = item.newPosition;
            var oldPosition = item.oldPosition;
            var newOrder = tableData[newPosition].order_id;
            var oldOrder = tableData[oldPosition].order_id;

            var data = { 'ID': ID, 'new_order_id': newOrder, 'old_order_id': oldOrder };
            parameter.push(data);
            console.log(data);
        }

        //sırala
        var data = JSON.stringify({ parameter: parameter });
        callService('ProductAttributeOptionService', 'OrderModel', data, function (response) {
            if (response.responseCode == 200) {

            }
        }, function (errormsg) { console.log(errormsg); }, true);
    });
}

function _form_save_option() {
    var model = {
        ID: selected_options,
        product_attribute_id: selected_attribute,
        name: $('#_name_options').val(),
        name_eng: $('#_name_eng_options').val(),
        name_other: $('#_name_other_options').val()
    }

    var data = JSON.stringify({ model: model });
    callService('ProductAttributeOptionService', 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm_options();
                loadOptions(selected_attribute);
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function cancelForm_options() {
    selected_attribute = 0;
    selected_options = 0;
    $('#_name_options').val("");
    $('#_name_eng_options').val("");
    $('#_name_other_options').val("");
    $('#_modalForm_options').modal('hide');
}

function clearForm_options() {
    selected_options = 0;
    $('#_name_options').val("");
    $('#_name_eng_options').val("");
    $('#_name_other_options').val("");
}


function activeHandler_options(parameter) {
    var data = JSON.stringify({ ID: parameter });
    callService('ProductAttributeOptionService', 'ActiveModel', data, function () {
        loadOptions(selected_attribute);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function deleteHandler_options(parameter) {
    confirmDeleteItem(function (parameter) {
        var data = JSON.stringify({ ID: parameter });
        callService('ProductAttributeOptionService', 'DeleteModel', data, function (response) {
            try {
                alertDeleteItem(function () {
                    loadOptions(selected_attribute);
                });
            } catch (exc) { }
        }, function (errormsg) { console.log(errormsg); }, false);
    }, parameter);
}

function editHandler_options(parameter) {
    var data = JSON.stringify({ ID: parameter });
    callService('ProductAttributeOptionService', 'LoadModel', data, function (response) {
        if (response.responseCode == 200) {
            selected_options = response.response.ID;
            $('#_name_options').val(response.response.name);
            $('#_name_eng_options').val(response.response.name_eng);
            $('#_name_other_options').val(response.response.name_other);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}