﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_facebook_app_code').on('input', function () { validator.element($(this)); });
        $('#_facebook_app_secret').on('input', function () { validator.element($(this)); });
        $('#_facebook_connect_type').on('input', function () { validator.element($(this)); });
        $('#_facebook_store_type').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _facebook_app_code: { required: true, minlength: 1, maxlength: 255 },
                _facebook_app_secret: { required: true, minlength: 1, maxlength: 255 },
                _facebook_connect_type: { required: true, minlength: 1 },
                _facebook_store_type: { required: true, minlength: 1 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_FACEBOOKENTEGRATION";
var serviceName = "SettingFacebookService";

function initpage() {
}

function afterPageLoad() {
    loadData();
    $("#_facebook_connect_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
    $("#_facebook_store_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
}

function addEventListener() {

}

function loadData() {
    callService(serviceName, 'LoadModelExact', null, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;
    $('#_facebook_app_code').val(model.facebook_app_code);
    $('#_facebook_app_secret').val(model.facebook_app_secret);
    $("#_facebook_connect_type").val(model.facebook_connect_type).trigger('change');
    $("#_facebook_store_type").val(model.facebook_store_type).trigger('change');
    $('#_modalForm').modal('show');
}

function _form_save() {
    var model = {
        ID: selected,
        facebook_app_code: $('#_facebook_app_code').val(),
        facebook_app_secret: $('#_facebook_app_secret').val(),
        facebook_connect_type: $('#_facebook_connect_type').val(),
        facebook_store_type: $('#_facebook_store_type').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    $('#_facebook_app_code').val("");
    $('#_facebook_app_secret').val("");
    $('#_facebook_connect_type').val("");
    $('#_facebook_store_type').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}