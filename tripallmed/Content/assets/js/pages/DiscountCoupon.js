﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        //$('#_name').on('input', function () { validator.element($(this)); });
        //$('#_code').on('input', function () { validator.element($(this)); });
        //$('#_discount_amount').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                //_name: { required: true, minlength: 1, maxlength: 100 },
                //_code: { required: true, minlength: 1, maxlength: 100 },
                //_discount_amount: { required: true, minlength: 1, maxlength: 100 },
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
        console.log(validator);
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_DISCTOUNT_COUPON";
var serviceName = "DiscountCouponService";
var selectedDate;

function initpage() {
    moment.defaultFormat = "DD/MM/YYYY HH:mm";

    $("#_status").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    callService("CustomerTypeService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            var data = prepareSelectData(response.response);
            data.unshift({
                id: 0,
                text: getLocaliziedValue("FORM_select"),
                selected: true
            });
            $("#_member_group_id").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: data
            })
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

    $("#_order_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    callService("MemberService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            var data = prepareSelectData(response.response, undefined, "member_name");
            data.unshift({
                id: 0,
                text: getLocaliziedValue("FORM_select"),
                selected: true
            });
            $("#_member_id").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: data
            })
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

    var pdata = [];
    pdata.push({
        id: 0,
        text: getLocaliziedValue("FORM_select"),
        selected: true
    });
    $("#_product_id").select2({
        placeholder: getLocaliziedValue("action_select"),
        data: pdata
    })
    /*
    callService("ProductService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            var data = prepareSelectData(response.response);
            data.unshift({
                id: 0,
                text: getLocaliziedValue("FORM_select"),
                selected: true
            });
            $("#_product").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: data
            })
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
    */

    $("#_discount_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_date_range").daterangepicker(
        {
            buttonClasses: " btn",
            applyClass: "btn-primary",
            cancelClass: "btn-secondary",
            timePicker: !0,
            timePicker24Hour: true,
            timePickerIncrement: 30,
            locale: { format: "DD/MM/YYYY h:mm" },
            autoUpdateInput: true
        }, function (a, t, e) {
            $("#_date_range .form-control").val(a.format("DD/MM/YYYY h:mm") + " / " + t.format("DD/MM/YYYY h:mm"));
            selectedDate = a.format("DD/MM/YYYY/h/mm") + "/" + t.format("DD/MM/YYYY/h/mm");
        })

}

function afterPageLoad() {
    setTimeout(function () {
        var ID = getParameterByName("ID");
        if (ID && ID != undefined && ID != null) {
            loadData(ID);
        }
    }, 1000)
}

function addEventListener() {

}

function loadData(ID) {
    var data = JSON.stringify({ ID: ID });
    callService(serviceName, 'LoadModel', data, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;
    $('#_name').val(model.name);
    $('#_code').val(model.code);
    $("#_status").val(model.active).trigger('change');
    $("#_member_group_id").val(model.member_group_id).trigger('change');
    $("#_order_type").val(model.order_type).trigger('change');
    $("#_member_id").val(model.member_id).trigger('change');
    $('#_member_usage_limit').val(model.member_usage_limit);
    $('#_total_usage_limit').val(model.total_usage_limit);
    $("#_product_id").val(model.product_id).trigger('change');
    $('#_basket_amount').val(model.basket_amount);
    $("#_discount_type").val(model.discount_type).trigger('change');
    $('#_discount_amount').val(model.discount_amount);

    var start_date = moment(model.start_date).format('DD/MM/YYYY h:mm');
    var end_date = moment(model.end_date).format('DD/MM/YYYY h:mm');
    $('#_date_range').data('daterangepicker').setStartDate(start_date);
    $('#_date_range').data('daterangepicker').setEndDate(end_date);

    var newDate = moment(model.start_date).format('DD/MM/YYYY h:mm') + " / " + moment(model.end_date).format('DD/MM/YYYY h:mm');
    selectedDate = moment(model.start_date).format('DD/MM/YYYY/h/mm') + "/" + moment(model.end_date).format('DD/MM/YYYY/h/mm');
    $('#_date_range input').val(newDate);
}

function _form_save() {
    var model = {
        ID: selected,
        name: $('#_name').val(),
        code: $('#_code').val(),
        active: $('#_status').val(),
        member_group_id: $('#_member_group_id').val(),
        order_type: $('#_order_type').val(),
        member_id: $('#_member_id').val(),
        member_usage_limit: $('#_member_usage_limit').val(),
        total_usage_limit: $('#_total_usage_limit').val(),
        product_id: $('#_product_id').val(),
        basket_amount: $('#_basket_amount').val(),
        selectedDate: selectedDate.replace(/\s/g, ""),
        discount_type: $('#_discount_type').val(),
        discount_amount: $('#_discount_amount').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                window.location.href = "/Panel/Pages/Marketting/DiscountCouponList.aspx";
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    $('#_header').val("");
    $('#_header_eng').val("");
    $('#_header_other').val("");
    $('#_seo').val("");
    $('#_seo_eng').val("");
    $('#_seo_other').val("");
    $('#_summary').val("");
    $('#_summary_eng').val("");
    $('#_summary_other').val("");
}