﻿var pageNameKey = "PAGENAME_CONTRACTLIST";
var serviceName = "ContractService";

function initpage() {
    iniDataTableAttributes();
    loadData();
}

function afterPageLoad() {

}

function addEventListener() {

}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        { data: "name" },
        { data: "name_eng" },
        { data: "name_other" },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="custom" action-handler="loadDetail" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_CONTRACTLIST_detail") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [];
}

function loadData() {
    var data = getTableData();
    initDataTable(data, columns, buttons, null);
}

function getTableData() {
    var data;
    getItemsSynch(serviceName, null, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
    return data;
}

function loadDetail(parameter) {
    window.location.href = "/Panel/Pages/Settings/ContractDetail.aspx?ID=" + parameter;
}