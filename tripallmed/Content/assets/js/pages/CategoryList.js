﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
        $('#_name_eng').on('input', function () { validator.element($(this)); });
        $('#_name_other').on('input', function () { validator.element($(this)); });
        $('#_detail').on('input', function () { validator.element($(this)); });
        $('#_detail_eng').on('input', function () { validator.element($(this)); });
        $('#_detail_other').on('input', function () { validator.element($(this)); });
        $('#_name_seo').on('input', function () { validator.element($(this)); });
        $('#_name_seo_eng').on('input', function () { validator.element($(this)); });
        $('#_name_seo_other').on('input', function () { validator.element($(this)); });
        $('#_code').on('input', function () { validator.element($(this)); });
        $('#_seo_title').on('input', function () { validator.element($(this)); });
        $('#_seo_description').on('input', function () { validator.element($(this)); });
        $('#_seo_keywords').on('input', function () { validator.element($(this)); });

        $('#_name_seo').bind("keyup focusout", function () {
            clearSeo(this);
        });

        $('#_name_seo_eng').bind("keyup focusout", function () {
            clearSeo(this);
        });

        $('#_name_seo_other').bind("keyup focusout", function () {
            clearSeo(this);
        });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 50 },
                _name_eng: { required: false, minlength: 0, maxlength: 50 },
                _name_other: { required: false, minlength: 0, maxlength: 50 },
                _detail: { required: false, minlength: 0, maxlength: 200 },
                _detail_eng: { required: false, minlength: 0, maxlength: 200 },
                _detail_other: { required: false, minlength: 0, maxlength: 200 },
                _name_seo: { required: true, minlength: 1, maxlength: 50 },
                _name_seo_eng: { required: false, minlength: 0, maxlength: 50 },
                _name_seo_other: { required: false, minlength: 0, maxlength: 50 },
                _code: { required: false, minlength: 0, maxlength: 100 },
                _seo_title: { required: false, minlength: 0, maxlength: 255 },
                _seo_description: { required: false, minlength: 0, maxlength: 255 },
                _seo_keywords: { required: false, minlength: 0, maxlength: 255 },

            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var selectedModel = 0;
var parent_id = 0;
var pageNameKey = "PAGENAME_CATEGORYLIST";
var serviceName = "CategoryService";

function initpage() {
    iniDataTableAttributes();
    loadData();
    loadProductAttributeList();
}

function afterPageLoad() {
    initImages();
    initEditors();
    initSelects();
    loadCategoryList();
}

function initImages() {
    new KTAvatar("_image1");
    new KTAvatar("_image2");
    new KTAvatar("_image3");
}

function initEditors() {
    $('.summenote-sm').summernote({ height: 300 });
}

function initSelects() {
    $("#_default_order").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
    $("#_default_installment").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
}

function addEventListener() {

}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        { data: "name" },
        { data: "name_seo" },
        { data: "code" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="custom" action-handler="loadSubCategories" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_subCategories") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="edit" action-handler="bindModel" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="delete" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                clearAndShowForm();
            }
        },
        {
            text: getLocaliziedValue("TABLE_export"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                exportToCsv();
            }
        },
        {
            text: getLocaliziedValue("TABLE_reset"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                loadData();
            }
        }
    ];
}

function loadCategoryList() {
    var data = [];
    var parentList = getCategoryList(0);
    $.each(parentList, function () {
        data.push({
            id: this.ID,
            text: this.name
        });
        var subList = getCategoryList(this.ID);
        $.each(subList, function () {
            data.push({
                id: this.ID,
                text: "-" + this.name
            });
            var subSubList = getCategoryList(this.ID);
            $.each(subSubList, function () {
                data.push({
                    id: this.ID,
                    text: "--" + this.name
                });
            });
        });
    });
    data.unshift({
        id: 0,
        text: getLocaliziedValue("FORM_CATEGORYLIST_main_category"),
        selected: true
    });
    $("#_category").select2({
        placeholder: getLocaliziedValue("action_select"),
        data: data
    })
}

function loadData() {
    var data = getCategoryList();
    initDataTable(data, columns, buttons, orderHandler);
}

function loadSubCategories(parameter) {
    parent_id = parameter;
    var data = JSON.stringify({ parent_id: parameter });
    getItemsAsynch("CategoryService", data, function (response) {
        initDataTable(response.response, columns, buttons, orderHandler);
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel_ParentId");
}

function bindModel(model) {
    selectedModel = model;
    selected = model.ID;
    $('#_name').val(model.name);
    $('#_name_eng').val(model.name_eng);
    $('#_name_other').val(model.name_other);
    $('#_detail').val(model.detail);
    $('#_detail_eng').val(model.detail_eng);
    $('#_detail_other').val(model.detail_other);
    $('#_name_seo').val(model.name_seo);
    $('#_name_seo_eng').val(model.name_seo_eng);
    $('#_name_seo_other').val(model.name_seo_other);
    $('#_code').val(model.code);
    $('#_seo_title').val(model.seo_title);
    $('#_seo_description').val(model.seo_description);
    $('#_seo_keywords').val(model.seo_keywords);
    $('.note-editable').eq(0).html(model.top_content);
    $('.note-editable').eq(1).html(model.bottom_content);
    $('#_default_order').val(model.default_order);
    $('#_default_installment').val(model.default_installment);
    $('#_google_merchant').val(model.google_merchant);
    $("#_category").val(model.parent_id).trigger('change');
    setImageSrc("_image1_val", "/Data/" + model.image);
    setImageSrc("_image2_val", "/Data/" + model.image_eng);
    setImageSrc("_image3_val", "/Data/" + model.image_other);

    bindProductAttributeList();

    $('#_modalForm').modal('show');
}

function tableActiveActionCallback() {
    initpage();
}

function validateSeoLinks() {
    var isSeoValid = validateSeo($('#_name_seo'), selectedModel.name_seo);
    var isSeoValidEng = validateSeo($('#_name_seo_eng'), selectedModel.name_seo_eng);
    var isSeoValidOther = validateSeo($('#_name_seo_other'), selectedModel.name_seo_other);
    return isSeoValid && isSeoValidEng && isSeoValidOther;
}

function _form_save() {
    var isSeoValid = validateSeoLinks();
    if (!isSeoValid) {
        return;
    }

    var image = uploadImage("_image1_val");
    var image_eng = uploadImage("_image2_val");
    var image_other = uploadImage("_image3_val");
    var attributeList = getProductAttributeList();

    var model = {
        ID: selected,
        parent_id: $('#_category').val(),
        name: $('#_name').val(),
        name_eng: $('#_name_eng').val(),
        name_other: $('#_name_other').val(),
        name_seo: $('#_name_seo').val(),
        name_seo_eng: $('#_name_seo_eng').val(),
        name_seo_other: $('#_name_seo_other').val(),
        detail: $('#_detail').val(),
        detail_eng: $('#_detail_eng').val(),
        detail_other: $('#_detail_other').val(),
        code: $('#_code').val(),
        seo_title: $('#_seo_title').val(),
        seo_description: $('#_seo_description').val(),
        seo_keywords: $('#_seo_keywords').val(),
        top_content: $('.note-editable').eq(0).html(),
        bottom_content: $('.note-editable').eq(1).html(),
        default_order: $('#_default_order').val(),
        default_installment: $('#_default_installment').val(),
        google_merchant: $('#_google_merchant').val(),
        image: image,
        image_eng: image_eng,
        image_other: image_other,
        productAttributeList: attributeList.productAttributeList,
        productAttributeListDisable: attributeList.productAttributeListDisable
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    selected = 0;
    $('#_name').val("");
    $('#_name_eng').val("");
    $('#_name_other').val("");
    $('#_detail').val("");
    $('#_detail_eng').val("");
    $('#_detail_other').val("");
    $('#_name_seo').val("");
    $('#_name_seo_eng').val("");
    $('#_name_seo_other').val("");
    $('#_code').val("");
    setImageSrc("_image1_val", "/Data/nocontent.png");
    setImageSrc("_image2_val", "/Data/nocontent.png");
    setImageSrc("_image3_val", "/Data/nocontent.png");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}

function bindProductAttributeList() {
    var data = JSON.stringify({ category_id: selected });
    callService("CategoryService", 'ListCategoryFilter', data, function (response) {
        if (response.responseCode == 200) {
            $(".product_attribute").each(function () {
                var that = this;
                var ID = $(this).attr("ID");
                $.each(response.response, function () {
                    if (ID == this.product_attribute_id) {
                        that.checked = this.active ? true : false;
                    }
                });
            });
        } else if (response.responseCode == 500) {
            console.log(response.errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function loadProductAttributeList() {
    callService("ProductAttributeService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            $('#product_attribute_list').empty();
            $.each(response.response, function () {
                var opt = '<label class="kt-checkbox"> <input class="product_attribute" type="checkbox" id="' + this.ID + '" > ' + this.name + ' <span></span></label>';
                $('#product_attribute_list').append(opt);
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function getProductAttributeList() {
    var productAttributeList = [];
    var productAttributeListDisable = [];
    $(".product_attribute").each(function () {
        if (this.checked) {
            var ID = $(this).attr("ID");
            productAttributeList.push(ID);
        } else {
            var ID = $(this).attr("ID");
            productAttributeListDisable.push(ID);
        }
    });
    return { productAttributeList: productAttributeList, productAttributeListDisable: productAttributeListDisable };
}

function orderHandler() {
    loadData();
}