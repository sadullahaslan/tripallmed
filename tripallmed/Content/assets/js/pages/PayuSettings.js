﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_entegration_name').on('input', function () { validator.element($(this)); });
        $('#_code_key').on('input', function () { validator.element($(this)); });
        $('#_return_url').on('input', function () { validator.element($(this)); });
        $('#_entegration_name_test').on('input', function () { validator.element($(this)); });
        $('#_code_key_test').on('input', function () { validator.element($(this)); });
        $('#_return_url_test').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _entegration_name: { required: true, minlength: 1, maxlength: 50 },
                _code_key: { required: true, minlength: 1, maxlength: 50 },
                _return_url: { required: true, minlength: 1, maxlength: 50 },
                _entegration_name_test: { required: true, minlength: 1, maxlength: 50 },
                _code_key_test: { required: true, minlength: 1, maxlength: 50 },
                _return_url_test: { required: true, minlength: 1, maxlength: 50 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_PAYUSETTINGS";
var serviceName = "SettingPayuService";

function initpage() {
}

function afterPageLoad() {
    loadData();
    $("#_payu_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })
}

function addEventListener() {

}

function loadData() {
    callService(serviceName, 'LoadModelExact', null, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;
    $('#_entegration_name').val(model.entegration_name);
    $('#_code_key').val(model.code_key);
    $('#_return_url').val(model.return_url);
    $('#_entegration_name_test').val(model.entegration_name_test);
    $('#_code_key_test').val(model.code_key_test);
    $('#_return_url_test').val(model.return_url_test);
    $("#_payu_type").val(model.payu_type).trigger('change');

    $('#_modalForm').modal('show');
}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var model = {
        ID: selected,
        entegration_name: $('#_entegration_name').val(),
        code_key: $('#_code_key').val(),
        return_url: $('#_return_url').val(),
        entegration_name_test: $('#_entegration_name_test').val(),
        code_key_test: $('#_code_key_test').val(),
        return_url_test: $('#_return_url_test').val(),
        payu_type: $('#_payu_type').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function clearForm() {
    $('#_entegration_name').val("");
    $('#_code_key').val("");
    $('#_return_url').val("");
    $('#_entegration_name_test').val("");
    $('#_code_key_test').val("");
    $('#_return_url_test').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}