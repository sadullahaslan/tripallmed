﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
        $('#_surname').on('input', function () { validator.element($(this)); });
        $('#_email').on('input', function () { validator.element($(this)); });
        $('#_phone').on('input', function () { validator.element($(this)); });
        $('#_password').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 50 },
                _surname: { required: true, minlength: 1, maxlength: 50 },
                _email: { required: true, minlength: 1, maxlength: 50 },
                _phone: { required: true, minlength: 1, maxlength: 20 },
                _password: { required: true, minlength: 1, maxlength: 20 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_CUSTOMERLIST";
var serviceName = "MemberService";

function initpage() {
    iniDataTableAttributes();
    loadData();
}

function afterPageLoad() {
    $("[data-switch=true]").bootstrapSwitch();
    $("#_type").select2({})
}

function addEventListener() {

}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        {
            data: null, render: function (data, type, row) { return data.name + " " + data.surname; }
        },
        { data: "register_date_string" },
        { data: "email" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="edit" action-handler="bindModel" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="delete" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [
        {
            text: getLocaliziedValue("TABLE_export"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                exportToCsv();
            }
        }
    ];
}

function loadData() {
    var data = getTableData();
    initDataTable(data, columns, buttons, orderHandler);
}

function getTableData() {
    var data;
    getItemsSynch(serviceName, null, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
    return data;
}

function bindModel(model) {
    selected = model.ID;

    bindGeneralInfoTab(model);
    bindAddressTab(model);
    bindOrderTab(model);
    bindBasketTab(model);
    bindEmailTab(model);

    $('#_modalForm').modal('show');
}

function bindGeneralInfoTab(model) {
    $('#_name').val(model.name);
    $('#_surname').val(model.surname);
    $('#_email').val(model.email);
    $('#_phone').val(model.phone);
    $('#_birthday').val(model.birth_date_string);
    $('#_type').val(model.member_type_id);
}

function bindAddressTab(model) {
    var row = "";
    var parameter = JSON.stringify({ member_id: model.ID });
    getItemsSynch("AddressService", parameter, function (response) {
        var list = response.response;
        $.each(list, function () {
            var address = `<div class="col-xl-3">
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-portlet__body">
                                <!--begin::Widget -->
                                <div class="kt-widget kt-widget--user-profile-2">
                                    <div class="kt-widget__head">
                                        <div class="kt-widget__info">
                                            <a href="#" class="kt-widget__titel kt-hidden-">
                                                ` + this.address_name + `                                              
                                            </a>
                                        </div>
                                    </div>
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__section">
						                    ` + this.name + ` ` + this.surname + `
					                    </div>
                                        <div class="kt-widget__section">
						                    ` + this.address + `
					                    </div>
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">Ülke</span>
                                                <a href="#" class="kt-widget__data">` + this.country_name + `</a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">İl</span>
                                                <a href="#" class="kt-widget__data">` + this.city_name + `</a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">İlçe</span>
                                                <a href="#" class="kt-widget__data">` + this.town_name + `</a>
                                            </div>
                                            <div class="kt-widget__contact">
                                                <span class="kt-widget__label">Telefon</span>
                                                <a href="#" class="kt-widget__data">` + this.phone + `</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>         
                            </div>
                        </div>
                    </div>`;
            row += address;
        });
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel_MemberId_Panel");

    $('#_address_list').empty();
    $('#_address_list').append(row);
}

function bindOrderTab(model) {

}

function bindBasketTab(model) {

}

function bindEmailTab(model) {

}
function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var model = {
        ID: selected,
        name: $('#_name').val(),
        surname: $('#_surname').val(),
        email: $('#_email').val(),
        phone: $('#_phone').val(),
        password: $('#_password').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    selected = 0;
    $('#_name').val("");
    $('#_surname').val("");
    $('#_email').val("");
    $('#_phone').val("");
    $('#_password').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}

function orderHandler() {
    loadData();
}