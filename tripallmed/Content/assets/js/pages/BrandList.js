﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
        $('#_name_eng').on('input', function () { validator.element($(this)); });
        $('#_name_other').on('input', function () { validator.element($(this)); });
        $('#_detail').on('input', function () { validator.element($(this)); });
        $('#_detail_eng').on('input', function () { validator.element($(this)); });
        $('#_detail_other').on('input', function () { validator.element($(this)); });
        $('#_name_seo').on('input', function () { validator.element($(this)); });
        $('#_name_seo_eng').on('input', function () { validator.element($(this)); });
        $('#_name_seo_other').on('input', function () { validator.element($(this)); });
        $('#_url').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 255 },
                _name_eng: { required: false, minlength: 0, maxlength: 255 },
				_name_other: { required: false, minlength: 0, maxlength: 255 },
				_detail: { required: false, minlength: 0, maxlength: 255 },
				_detail_eng: { required: false, minlength: 0, maxlength: 255 },
				_detail_other: { required: false, minlength: 0, maxlength: 255 },
                _name_seo: { required: true, minlength: 1, maxlength: 255 },
				_name_seo_eng: { required: false, minlength: 0, maxlength: 255 },
				_name_seo_other: { required: false, minlength: 0, maxlength: 255 },
				_url: { required: false, minlength: 0, maxlength: 255 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_BRANDLIST";
var serviceName = "BrandService";

function initpage() {
    iniDataTableAttributes();
    loadData();
    loadProductAttributeList();
}

function afterPageLoad() {
    new KTAvatar("_image1");
    new KTAvatar("_image2");
    new KTAvatar("_image3");
}

function addEventListener() {

}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        { data: "name" },
        { data: "name_seo" },
        { data: "url" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="edit" action-handler="bindModel" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="delete" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                clearAndShowForm();
            }
        },
        {
            text: getLocaliziedValue("TABLE_export"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                exportToCsv();
            }
        }
    ];
}

function loadData() {
    var data = getTableData();
    initDataTable(data, columns, buttons, orderHandler);
}

function getTableData() {
    var data;
    getItemsSynch(serviceName, null, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
    return data;
}

function bindModel(model) {
    selected = model.ID;
    $('#_name').val(model.name);
    $('#_name_eng').val(model.name_eng);
    $('#_name_other').val(model.name_other);
    $('#_detail').val(model.detail);
    $('#_detail_eng').val(model.detail_eng);
    $('#_detail_other').val(model.detail_other);
    $('#_name_seo').val(model.name_seo);
    $('#_name_seo_eng').val(model.name_seo_eng);
    $('#_name_seo_other').val(model.name_seo_other);
    $('#_url').val(model.url);
    setImageSrc("_image1_val", "/Data/" + model.image);
    setImageSrc("_image2_val", "/Data/" + model.image_eng);
    setImageSrc("_image3_val", "/Data/" + model.image_other);

    $('#_modalForm').modal('show');
}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var image = uploadImage("_image1_val");
    var image_eng = uploadImage("_image2_val");
    var image_other = uploadImage("_image3_val");

    var model = {
        ID: selected,
        name: $('#_name').val(),
        name_eng: $('#_name_eng').val(),
        name_other: $('#_name_other').val(),
        name_seo: $('#_name_seo').val(),
        name_seo_eng: $('#_name_seo_eng').val(),
        name_seo_other: $('#_name_seo_other').val(),
        detail: $('#_detail').val(),
        detail_eng: $('#_detail_eng').val(),
        detail_other: $('#_detail_other').val(),
        url: $('#_url').val(),
        image: image,
        image_eng: image_eng,
        image_other: image_other
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    selected = 0;
    $('#_name').val("");
    $('#_name_eng').val("");
    $('#_name_other').val("");
    $('#_detail').val("");
    $('#_detail_eng').val("");
    $('#_detail_other').val("");
    $('#_name_seo').val("");
    $('#_name_seo_eng').val("");
    $('#_name_seo_other').val("");
    $('#_url').val("");
    setImageSrc("_image1_val", "/Data/nocontent.png");
    setImageSrc("_image2_val", "/Data/nocontent.png");
    setImageSrc("_image3_val", "/Data/nocontent.png");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}

function orderHandler() {
    loadData();
}