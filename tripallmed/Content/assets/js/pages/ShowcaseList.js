﻿var pageNameKey = "PAGENAME_SHOWCASELIST";
var serviceName = "ShowcaseService";
var tableTr, tableEng, tableOther;
var buttonsTr, buttonsEng, buttonsOther;

function initpage() {
    iniDataTableAttributes();
    loadData();
}

function afterPageLoad() {

}

function addEventListener() {

}

function tableActiveActionCallback() {
    loadData();
}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        { data: "name" },
        { data: "showcase_type_text" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="edit" action-handler="bindModel" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_edit") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="delete" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttonsTr = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                window.location.href = "/Panel/Pages/Settings/ShowcaseInsert.aspx?language=tr";
            }
        }
    ];
    buttonsEng = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                window.location.href = "/Panel/Pages/Settings/ShowcaseInsert.aspx?language=eng";
            }
        }
    ];
    buttonsOther = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                window.location.href = "/Panel/Pages/Settings/ShowcaseInsert.aspx?language=other";
            }
        }
    ];
}

function loadData() {
    var data = getTableDataTr();
    initTableTr(data, columns, buttonsTr);

    var dataEng = getTableDataEng();
    initTableEng(dataEng, columns, buttonsEng);

    var dataOther = getTableDataOther();
    initTableOther(dataOther, columns, buttonsOther);
}

function initTableTr(data, columns, buttons) {
    tableTr = $('#_tableFormTr').DataTable({
        "language": {
            "lengthMenu": getLocaliziedValue("datatableButton_perPage"),
            "zeroRecords": getLocaliziedValue("datatableButton_noRecord"),
            "info": getLocaliziedValue("datatableButton_activePage") + ": _PAGE_ / _PAGES_",
            "infoEmpty": getLocaliziedValue("datatableButton_noData"),
            "infoFiltered": "",
        },
        "aaSorting": [],
        data: data,
        columns: columns,
        dom: 'Bfrtip',
        buttons: buttons,
        autoWidth: false,
        rowReorder: true,
        "destroy": true,
        searching: false,
        bFilter: false,
        pageLength: 3
    });

    tableTr.on('row-reorder', function (e, diff, edit) {
        var parameter = [];
        for (var i = 0; i < diff.length; i++) {
            var item = diff[i];
            var ID = item.node.getElementsByTagName('td')[0].textContent
            var newPosition = item.newPosition;
            var oldPosition = item.oldPosition;
            var newOrder = tableData[newPosition].order_id;
            var oldOrder = tableData[oldPosition].order_id;

            var data = { 'ID': ID, 'new_order_id': newOrder, 'old_order_id': oldOrder };
            parameter.push(data);
            console.log(data);
        }

        //sırala
        var data = JSON.stringify({ parameter: parameter });
        callService(serviceName, 'OrderModel', data, function (response) {
            if (response.responseCode == 200) {

            }
        }, function (errormsg) { console.log(errormsg); }, true);
    });
}

function initTableEng(data, columns, buttons) {
    tableEng = $('#_tableFormEng').DataTable({
        "language": {
            "lengthMenu": getLocaliziedValue("datatableButton_perPage"),
            "zeroRecords": getLocaliziedValue("datatableButton_noRecord"),
            "info": getLocaliziedValue("datatableButton_activePage") + ": _PAGE_ / _PAGES_",
            "infoEmpty": getLocaliziedValue("datatableButton_noData"),
            "infoFiltered": "",
        },
        "aaSorting": [],
        data: data,
        columns: columns,
        dom: 'Bfrtip',
        buttons: buttons,
        autoWidth: false,
        rowReorder: true,
        "destroy": true,
        searching: false,
        bFilter: false,
        pageLength: 3
    });

    tableEng.on('row-reorder', function (e, diff, edit) {
        var parameter = [];
        for (var i = 0; i < diff.length; i++) {
            var item = diff[i];
            var ID = item.node.getElementsByTagName('td')[0].textContent
            var newPosition = item.newPosition;
            var oldPosition = item.oldPosition;
            var newOrder = tableData[newPosition].order_id;
            var oldOrder = tableData[oldPosition].order_id;

            var data = { 'ID': ID, 'new_order_id': newOrder, 'old_order_id': oldOrder };
            parameter.push(data);
            console.log(data);
        }

        //sırala
        var data = JSON.stringify({ parameter: parameter });
        callService(serviceName, 'OrderModel', data, function (response) {
            if (response.responseCode == 200) {

            }
        }, function (errormsg) { console.log(errormsg); }, true);
    });
}

function initTableOther(data, columns, buttons) {
    tableOther = $('#_tableFormOther').DataTable({
        "language": {
            "lengthMenu": getLocaliziedValue("datatableButton_perPage"),
            "zeroRecords": getLocaliziedValue("datatableButton_noRecord"),
            "info": getLocaliziedValue("datatableButton_activePage") + ": _PAGE_ / _PAGES_",
            "infoEmpty": getLocaliziedValue("datatableButton_noData"),
            "infoFiltered": "",
        },
        "aaSorting": [],
        data: data,
        columns: columns,
        dom: 'Bfrtip',
        buttons: buttons,
        autoWidth: false,
        rowReorder: true,
        "destroy": true,
        searching: false,
        bFilter: false,
        pageLength: 3
    });

    tableOther.on('row-reorder', function (e, diff, edit) {
        var parameter = [];
        for (var i = 0; i < diff.length; i++) {
            var item = diff[i];
            var ID = item.node.getElementsByTagName('td')[0].textContent
            var newPosition = item.newPosition;
            var oldPosition = item.oldPosition;
            var newOrder = tableData[newPosition].order_id;
            var oldOrder = tableData[oldPosition].order_id;

            var data = { 'ID': ID, 'new_order_id': newOrder, 'old_order_id': oldOrder };
            parameter.push(data);
            console.log(data);
        }

        //sırala
        var data = JSON.stringify({ parameter: parameter });
        callService(serviceName, 'OrderModel', data, function (response) {
            if (response.responseCode == 200) {

            }
        }, function (errormsg) { console.log(errormsg); }, true);
    });
}

function getTableDataTr() {
    var data = JSON.stringify({ language: 'tr' });
    getItemsSynch(serviceName, data, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModelLanguge");
    return data;
}

function getTableDataEng() {
    var data = JSON.stringify({ language: 'eng' });
    getItemsSynch(serviceName, data, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModelLanguge");
    return data;
}

function getTableDataOther() {
    var data = JSON.stringify({ language: 'other' });
    getItemsSynch(serviceName, data, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModelLanguge");
    return data;
}

function bindModel(parameter) {
    window.location.href = "/Panel/Pages/Settings/ShowcaseInsert.aspx?ID=" + parameter.ID;
}