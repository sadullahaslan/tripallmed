﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 50 },
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_SHOWCASEINSERT";
var serviceName = "ShowcaseService";

function initpage() {
    initSelectes();
    initImages();
    loadProductList();
}

function afterPageLoad() {
    initImages();
    setTimeout(function () {
        var ID = getParameterByName("ID");
        if (ID != null && ID != undefined) {
            loadData(ID);
        }
    }, 500);
}

function initSelectes() {
    $("#_showcase_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image1_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image2_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image3_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image4_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image5_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image6_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image7_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image8_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image9_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image10_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image11_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

    $("#_image12_type").select2({
        placeholder: getLocaliziedValue("FORM_select")
    })

}

function initImages() {
    new KTAvatar("_image1");
    new KTAvatar("_image2");
    new KTAvatar("_image3");
    new KTAvatar("_image4");
    new KTAvatar("_image5");
    new KTAvatar("_image6");
    new KTAvatar("_image7");
    new KTAvatar("_image8");
    new KTAvatar("_image9");
    new KTAvatar("_image10");
    new KTAvatar("_image11");
    new KTAvatar("_image12");
}

function loadData(ID) {
    var data = JSON.stringify({ ID: ID });
    callService(serviceName, 'LoadModel', data, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;

    $('#_name').val(model.name);
    $('#_detail').val(model.detail);
    $('#_showcase_type').val(model.showcase_type);

    setImageSrc("_image1_val", "/Data/" + model.image1);
    $('#_image1_detail').val(model.image1_detail);
    $('#_image1_url').val(model.image1_url);
    $("#_image1_type").val(model.image1_type).trigger('change');

    setImageSrc("_image2_val", "/Data/" + model.image2);
    $('#_image2_detail').val(model.image2_detail);
    $('#_image2_url').val(model.image2_url);
    $("#_image2_type").val(model.image2_type).trigger('change');

    setImageSrc("_image3_val", "/Data/" + model.image3);
    $('#_image3_detail').val(model.image3_detail);
    $('#_image3_url').val(model.image3_url);
    $("#_image3_type").val(model.image3_type).trigger('change');

    setImageSrc("_image4_val", "/Data/" + model.image4);
    $('#_image4_detail').val(model.image4_detail);
    $('#_image4_url').val(model.image4_url);
    $("#_image4_type").val(model.image4_type).trigger('change');

    setImageSrc("_image5_val", "/Data/" + model.image5);
    $('#_image5_detail').val(model.image5_detail);
    $('#_image5_url').val(model.image5_url);
    $("#_image5_type").val(model.image5_type).trigger('change');

    setImageSrc("_image6_val", "/Data/" + model.image6);
    $('#_image6_detail').val(model.image6_detail);
    $('#_image6_url').val(model.image6_url);
    $("#_image6_type").val(model.image6_type).trigger('change');

    setImageSrc("_image7_val", "/Data/" + model.image7);
    $('#_image7_detail').val(model.image7_detail);
    $('#_image7_url').val(model.image7_url);
    $("#_image7_type").val(model.image7_type).trigger('change');

    setImageSrc("_image8_val", "/Data/" + model.image8);
    $('#_image8_detail').val(model.image8_detail);
    $('#_image8_url').val(model.image8_url);
    $("#_image8_type").val(model.image8_type).trigger('change');

    setImageSrc("_image9_val", "/Data/" + model.image9);
    $('#_image9_detail').val(model.image9_detail);
    $('#_image9_url').val(model.image9_url);
    $("#_image9_type").val(model.image9_type).trigger('change');

    setImageSrc("_image10_val", "/Data/" + model.image10);
    $('#_image10_detail').val(model.image10_detail);
    $('#_image10_url').val(model.image10_url);
    $("#_image10_type").val(model.image10_type).trigger('change');

    setImageSrc("_image11_val", "/Data/" + model.image11);
    $('#_image11_detail').val(model.image11_detail);
    $('#_image11_url').val(model.image11_url);
    $("#_image11_type").val(model.image11_type).trigger('change');

    setImageSrc("_image12_val", "/Data/" + model.image12);
    $('#_image12_detail').val(model.image12_detail);
    $('#_image12_url').val(model.image12_url);
    $("#_image12_type").val(model.image12_type).trigger('change');

    $("#_product1").val(model.product1).trigger('change');
    $("#_product2").val(model.product2).trigger('change');
    $("#_product3").val(model.product3).trigger('change');
    $("#_product4").val(model.product4).trigger('change');
    $("#_product5").val(model.product5).trigger('change');
    $("#_product6").val(model.product6).trigger('change');
    $("#_product7").val(model.product7).trigger('change');
    $("#_product8").val(model.product8).trigger('change');
    $("#_product9").val(model.product9).trigger('change');
    $("#_product10").val(model.product10).trigger('change');
    $("#_product11").val(model.product11).trigger('change');
    $("#_product12").val(model.product12).trigger('change');
}

function loadProductList() {
    callService("ProductService", 'ListModel', null, function (response) {
        if (response.responseCode == 200) {
            var productList = prepareSelectData(response.response);
            productList.unshift({
                id: 0,
                text: getLocaliziedValue("FORM_select"),
                selected: true
            });
            $("#_product1").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product2").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product3").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product4").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product5").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product6").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product7").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product8").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product9").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product10").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product11").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
            $("#_product12").select2({
                placeholder: getLocaliziedValue("action_select"),
                data: productList
            })
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);
}

function _form_save() {
    var image1 = uploadImage("_image1_val");
    var image2 = uploadImage("_image2_val");
    var image3 = uploadImage("_image3_val");
    var image4 = uploadImage("_image4_val");
    var image5 = uploadImage("_image5_val");
    var image6 = uploadImage("_image6_val");
    var image7 = uploadImage("_image7_val");
    var image8 = uploadImage("_image8_val");
    var image9 = uploadImage("_image9_val");
    var image10 = uploadImage("_image10_val");
    var image11 = uploadImage("_image11_val");
    var image12 = uploadImage("_image12_val");
    var language = getParameterByName("language");
    if (language == null || language == undefined) {
        language = 'tr';
    }

    var model = {
        ID: selected,

        name: $('#_name').val(),
        detail: $('#_detail').val(),
        showcase_type: $('#_showcase_type').val(),
        language: language,

        image1: image1,
        image1_detail: $('#_image1_detail').val(),
        image1_url: $('#_image1_url').val(),
        image1_type: $('#_image1_type').val(),

        image2: image2,
        image2_detail: $('#_image2_detail').val(),
        image2_url: $('#_image2_url').val(),
        image2_type: $('#_image2_type').val(),

        image3: image3,
        image3_detail: $('#_image3_detail').val(),
        image3_url: $('#_image3_url').val(),
        image3_type: $('#_image3_type').val(),

        image4: image4,
        image4_detail: $('#_image4_detail').val(),
        image4_url: $('#_image4_url').val(),
        image4_type: $('#_image4_type').val(),

        image5: image5,
        image5_detail: $('#_image5_detail').val(),
        image5_url: $('#_image5_url').val(),
        image5_type: $('#_image5_type').val(),

        image6: image6,
        image6_detail: $('#_image6_detail').val(),
        image6_url: $('#_image6_url').val(),
        image6_type: $('#_image6_type').val(),

        image7: image7,
        image7_detail: $('#_image7_detail').val(),
        image7_url: $('#_image7_url').val(),
        image7_type: $('#_image7_type').val(),

        image8: image8,
        image8_detail: $('#_image8_detail').val(),
        image8_url: $('#_image8_url').val(),
        image8_type: $('#_image8_type').val(),

        image9: image9,
        image9_detail: $('#_image9_detail').val(),
        image9_url: $('#_image9_url').val(),
        image9_type: $('#_image9_type').val(),

        image10: image10,
        image10_detail: $('#_image10_detail').val(),
        image10_url: $('#_image10_url').val(),
        image10_type: $('#_image10_type').val(),

        image11: image11,
        image11_detail: $('#_image11_detail').val(),
        image11_url: $('#_image11_url').val(),
        image11_type: $('#_image11_type').val(),

        image12: image12,
        image12_detail: $('#_image12_detail').val(),
        image12_url: $('#_image12_url').val(),
        image12_type: $('#_image12_type').val(),

        product1: $('#_product1').val(),
        product2: $('#_product2').val(),
        product3: $('#_product3').val(),
        product4: $('#_product4').val(),
        product5: $('#_product5').val(),
        product6: $('#_product6').val(),
        product7: $('#_product7').val(),
        product8: $('#_product8').val(),
        product9: $('#_product9').val(),
        product10: $('#_product10').val(),
        product11: $('#_product11').val(),
        product12: $('#_product12').val(),
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}