﻿var pageNameKey = "PAGENAME_VARYANTLIST";
var serviceName = "ProductVaryantService";

function initpage() {
    iniDataTableAttributes();
    loadData();
}

function afterPageLoad() {

}

function addEventListener() {

}

function tableActiveActionCallback() {
    loadData();
}

function iniDataTableAttributes() {
    columns = [
        { data: "ID" },
        { data: "name" },
        {
            data: null, render: function (data, type, row) {
                var activeClass = data.active ?
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-success">` + getLocaliziedValue("TABLE_active") + `</span></span>` :
                    `<span><span callback-handler="tableActiveActionCallback" action-parameter="` + data.ID + `" class="table-active-action btn btn-bold btn-sm btn-font-sm btn-label-danger">` + getLocaliziedValue("TABLE_passive") + `</span></span>`;
                return activeClass;
            },
            className: "center",
            orderable: false
        },
        {
            data: null, render: function (data, type, row) {
                return `<div class="table-row-action-menu dropdown" action-parameter="` + data.ID + `">
                          <button class="btn btn-sm btn-clean btn-icon btn-icon-md dropdown-toggle" type="button" id="dropdownMenuButton` + data.ID + `" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="flaticon-more-1"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton` + data.ID + `">
                            <a class="table-row-action dropdown-item" href="#" action-type="custom" action-handler="loadVaryantDetail" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_varyantList") + `</a>
                            <a class="table-row-action dropdown-item" href="#" action-type="delete" callback-handler="tableRowActionDelete" action-parameter="` + data.ID + `">` + getLocaliziedValue("TABLE_delete") + `</a>
                          </div>
                        </div>`;
            },
            className: "center",
            orderable: true
        }
    ];
    buttons = [
        {
            text: getLocaliziedValue("TABLE_new"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                window.location.href = "/Panel/Pages/Catalog/ProductVaryantInsert.aspx";
            }
        },
        {
            text: getLocaliziedValue("TABLE_export"),
            className: 'btn btn-label-brand btn-bold table-actions',
            action: function (e, dt, node, config) {
                exportToCsv();
            }
        }
    ];
}

function loadData() {
    var data = getTableData();
    initDataTable(data, columns, buttons, null);
}

function loadVaryantDetail(parameter) {
    window.location.href = "/Panel/Pages/Catalog/ProductVaryantInsert.aspx?ID=" + parameter;
}



function getTableData() {
    var data;
    getItemsSynch(serviceName, null, function (response) {
        data = response.response;
    }, function (errormsg) {
        console.log(errormsg)
    }, "ListModel");
    return data;
}