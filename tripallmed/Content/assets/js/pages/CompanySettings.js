﻿var Validator = function () {

    var validator;

    var initWidgets = function () {
        $('#_name').on('input', function () { validator.element($(this)); });
        $('#_site').on('input', function () { validator.element($(this)); });
        $('#_person').on('input', function () { validator.element($(this)); });
        $('#_phone').on('input', function () { validator.element($(this)); });
        $('#_fax').on('input', function () { validator.element($(this)); });
        $('#_email').on('input', function () { validator.element($(this)); });
        $('#_tax_number').on('input', function () { validator.element($(this)); });
        $('#_tax_office').on('input', function () { validator.element($(this)); });
        $('#_address').on('input', function () { validator.element($(this)); });
    }

    var initValidation = function () {
        validator = $("#_form").validate({
            rules: {
                _name: { required: true, minlength: 1, maxlength: 255 },
				_site: { required: true, minlength: 1, maxlength: 255 },
				_person: { required: true, minlength: 1, maxlength: 255 },
				_phone: { required: true, minlength: 1, maxlength: 255 },
				_fax: { required: true, minlength: 1, maxlength: 255 },
                _email: { required: true, minlength: 1, maxlength: 255 },
                _tax_number: { required: true, minlength: 1, maxlength: 255 },
                _tax_office: { required: true, minlength: 1, maxlength: 255 },
                _address: { required: true, minlength: 1, maxlength: 255 }
            },

            invalidHandler: function (event, validator) {
                $('#_form_msg').removeClass('kt--hide').show();
            },

            submitHandler: function (form) {
                try {
                    var action = $(form).attr("data-action");
                    window[action]();
                } catch (exc) { }
            }
        });
    }

    return {
        // public functions
        init: function () {
            initWidgets();
            initValidation();
        }
    };
}();

var selected = 0;
var pageNameKey = "PAGENAME_COMPANYSETTINGS";
var serviceName = "SettingCompanyService";

function initpage() {
    new KTAvatar("_logo");
    new KTAvatar("_favicon");
}

function afterPageLoad() {
    loadData();

    $('#_city').on("select2:select", function (e) {
        var city_id = e.currentTarget.selectedOptions[0].value;
        loadTown(city_id);
    });
}

function addEventListener() {

}

function loadCity(city_id) {
    getCityListTurkey(function (response) {
        var data = prepareSelectData(response.response, city_id);
        $("#_city").select2().empty();
        $("#_city").select2({ data: data });
    });
}

function loadTown(city_id, town_id) {
    getTownList(city_id, function (response) {
        var data = prepareSelectData(response.response, town_id);
        $("#_town").select2().empty();
        $("#_town").select2({ data: data });
    });
}

function loadData() {
    callService(serviceName, 'LoadModelExact', null, function (response) {
        bindModel(response.response);
    }, function (errormsg) { console.log(errormsg); }, false);
}

function bindModel(model) {
    selected = model.ID;
    $('#_name').val(model.name);
    $('#_site').val(model.site);
    $('#_person').val(model.person);
    $('#_phone').val(model.phone);
    $('#_fax').val(model.fax);
    $('#_email').val(model.email);
    $('#_tax_number').val(model.tax_no);
    $('#_tax_office').val(model.tax_office);
    $('#_address').val(model.address);
    setImageSrc("_logo_val", "/Data/" + model.logo);
    setImageSrc("_favicon_val", "/Data/" + model.favicon);
    loadCity(model.city_id);
    loadTown(model.city_id, model.town_id);

    $('#_modalForm').modal('show');
}

function tableChangeOrderCallback(parameter) {
    alert("tableChangeOrderCallback: " + parameter)
}

function tableActiveActionCallback() {
    initpage();
}

function _form_save() {
    var logo = uploadImage("_logo_val");
    var favicon = uploadImage("_favicon_val");

    var model = {
        ID: selected,
        name: $('#_name').val(),
        site: $('#_site').val(),
        person: $('#_person').val(),
        phone: $('#_phone').val(),
        fax: $('#_fax').val(),
        email: $('#_email').val(),
        tax_no: $('#_tax_number').val(),
        tax_office: $('#_tax_office').val(),
        address: $('#_address').val(),
        logo: logo,
        favicon: favicon,
        city_id: $('#_city').val(),
        town_id: $('#_town').val()
    }

    var data = JSON.stringify({ model: model });
    callService(serviceName, 'SaveModel', data, function (response) {
        if (response.responseCode == 200) {
            alertSuccess(getLocaliziedValue("success_operation"), getLocaliziedValue("success_operationDetail"), function () {
                clearForm();
                $('#_modalForm').modal('hide');
                loadData();
            });
        } else if (response.responseCode == 500) {
            var errorMsg = getLocaliziedValue(response.errorCode);
            alertFail(getLocaliziedValue("ERROR_save"), errorMsg);
        }
    }, function (errormsg) { console.log(errormsg); }, false);

}

function clearForm() {
    $('#_name').val("");
    $('#_site').val("");
    $('#_person').val("");
    $('#_phone').val("");
    $('#_fax').val("");
    $('#_email').val("");
    $('#_tax_number').val("");
    $('#_tax_office').val("");
    $('#_address').val("");
    $('.nav-tabs a[href="#tab1"]').tab('show');
}