﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismStatus : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Statü 50 karakteri geçemez.")]
        public string Status { get; set; }

    }
}