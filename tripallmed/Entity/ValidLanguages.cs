﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class ValidLanguages : BaseEntity
    {
        public ValidLanguages()
        {
            HealthcareCompanies = new HashSet<HealthcareCompany>();
            TourismmCompanies = new HashSet<TourismmCompany>();
        }


        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Dil adı 50 karakteri geçemez.")]
        public string LanguageName { get; set; }


        public virtual ICollection<HealthcareCompany> HealthcareCompanies { get; set; }
        public virtual ICollection<TourismmCompany> TourismmCompanies { get; set; }
    }
}