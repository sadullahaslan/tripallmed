﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismAccommodationType : BaseEntity
    {
        public TourismAccommodationType()
        {
            TourismRoomType = new HashSet<TourismRoomType>();
        }
        public int Id { get; set; }

        [MaxLength(30)]
        public string Type { get; set; }

        public virtual ICollection<TourismRoomType> TourismRoomType { get; set; }
    }
}