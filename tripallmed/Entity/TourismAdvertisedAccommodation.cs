﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismAdvertisedAccommodation : BaseEntity
    {
        public TourismAdvertisedAccommodation()
        {
            TourismAdvertisedRoomProperties = new HashSet<TourismAdvertisedRoomProperty>();
        }

        public int Id { get; set; }


        public int? TourismAccommodationTypeId { get; set; }
        public virtual TourismAccommodationType TourismAccommodationType { get; set; }


        public virtual ICollection<TourismAdvertisedRoomProperty> TourismAdvertisedRoomProperties { get; set; }
    }
}