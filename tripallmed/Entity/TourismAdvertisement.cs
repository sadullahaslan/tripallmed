﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismAdvertisement : BaseEntity
    {
        public TourismAdvertisement()
        {
            TourismAmenities = new HashSet<TourismAmenities>();
            TourismSecurities = new HashSet<TourismSecurity>();
            TourismAdvertisedPersonPrices = new HashSet<TourismAdvertisedPersonPrices>();
        }

        public int Id { get; set; }



        [MaxLength(100, ErrorMessage = "İlan başlığı 100 karakteri geçemez.")]
        public string AdvertTitle { get; set; }



        [MaxLength(100)]
        public string AdvertTitle_Eng { get; set; }



        [MaxLength(100)]
        public string AdvertTitle_Oth { get; set; }


        public int? TourismAdvertisedAccommodationId { get; set; }
        public virtual TourismAdvertisedAccommodation TourismAdvertisedAccommodation { get; set; }


        public virtual ICollection<TourismAmenities> TourismAmenities { get; set; }


        public virtual ICollection<TourismSecurity> TourismSecurities { get; set; }
       
        
        [MaxLength(50)]
        public string CoverImage { get; set; }


        public int? TourismPriceTypeId { get; set; }
        public virtual TourismPriceType TourismPriceType { get; set; }


        public short? GuestCount { get; set; }


        public decimal? NightPrice { get; set; }


        public virtual ICollection<TourismAdvertisedPersonPrices> TourismAdvertisedPersonPrices { get; set; }


        [MaxLength(30)]
        public string Bathroom { get; set; }


        public bool BabyDiscount { get; set; }


        public bool ChildDiscount { get; set; }


        public int? CurrencyUnitId { get; set; }
        public virtual CurrencyUnit CurrencyUnit { get; set; }

        public int? CampaignDiscount { get; set; }


        public DateTime? CampaignStart { get; set; }


        public DateTime? CampaignEnd { get; set; }


        public decimal? Deposit { get; set; }



        public decimal? CleaningFee { get; set; }


        public short? InstantReservation { get; set; }


        public short? Prepayment { get; set; }


        [MaxLength(20)]
        public string CancellationPolicy { get; set; }



        public int TourismmCompanyId { get; set; }
        public virtual TourismmCompany TourismmCompany { get; set; }



        public int AdvertisementStatusId { get; set; }
        public virtual AdvertisementStatus AdvertisementStatus { get; set; }

        public int? TourismMealTypeId { get; set; }
        public virtual TourismMealType TourismMealType { get; set; }

        public short CompletionRate { get; set; }

    }
}