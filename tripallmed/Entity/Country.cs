﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class Country : BaseEntity
    {
        public Country()
        {
            Cities = new HashSet<City>();
        }

        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Ülke adı 50 karakteri geçemez.")]
        public string CountryName { get; set; }


        public virtual ICollection<City> Cities { get; set; }

    }
}