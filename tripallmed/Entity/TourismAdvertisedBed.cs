﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismAdvertisedBed : BaseEntity
    {
        public int Id { get; set; }

        public short Quantity { get; set; }

        public int? TourismBedTypeId { get; set; }
        public virtual TourismBedType TourismBedType { get; set; }

        public int? TourismAdvertisedRoomPropertyId { get; set; }
        public virtual TourismAdvertisedRoomProperty TourismAdvertisedRoomProperty { get; set; }
    }
}