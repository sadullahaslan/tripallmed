﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class PolyclinicService : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(100, ErrorMessage = "Poliklinik servis adı 100 karakteri geçemez.")]
        public string ServiceName { get; set; }


        public int? PolyclinicId { get; set; }
        public virtual Polyclinic Polyclinic { get; set; }
    }
}