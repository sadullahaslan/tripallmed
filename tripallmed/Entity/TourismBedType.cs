﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismBedType : BaseEntity
    {
        public int Id { get; set; }

        [MaxLength(30)]
        public string Type { get; set; }

        [MaxLength(100)]
        public string Image { get; set; }
    }
}