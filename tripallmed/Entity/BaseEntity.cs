﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            IsDeleted = false;
            Active = true;
        }
        public bool IsDeleted { get; set; }
        public bool Active { get; set; }

    }
}