﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismAdvertisedRoomProperty : BaseEntity
    {
        public TourismAdvertisedRoomProperty()
        {
            TourismAdvertisedBed = new HashSet<TourismAdvertisedBed>();
        }

        public int Id { get; set; }


        public int? TourismRoomTypeId { get; set; }
        public virtual TourismRoomType TourismRoomType { get; set; }

        public int? TourismAdvertisedAccommodationId { get; set; }
        public virtual TourismAdvertisedAccommodation TourismAdvertisedAccommodation { get; set; }

        public virtual ICollection<TourismAdvertisedBed> TourismAdvertisedBed { get; set; }
    }
}