﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class HealthcareCompany : BaseEntity
    {
        public HealthcareCompany()
        {
            Polyclinics = new HashSet<Polyclinic>();
            ContractedInstitutions = new HashSet<ContractedInstitution>();
            Doctors = new HashSet<Doctor>();
            PictureGalleries = new HashSet<PictureGallery>();
            HealthAdvertisements = new HashSet<HealthAdvertisement>();
            ValidLanguages = new HashSet<ValidLanguages>();
        }


        [ForeignKey("HealthUser")]
        public int Id { get; set; }
        public virtual HealthUser HealthUser { get; set; }

        public int? CompanyTypeId { get; set; }
        public virtual CompanyType CompanyType { get; set; }


        [MaxLength(150, ErrorMessage = "Şirket ünvanı 150 karakteri geçemez.")]
        public string Title { get; set; }


        [MaxLength(150, ErrorMessage = "Vergi dairesi 150 karakteri geçemez.")]
        public string TaxAdministration { get; set; }


        [MaxLength(100, ErrorMessage = "Vergi no 100 karakteri geçemez.")]
        public string TaxNo { get; set; }


        [MaxLength(200, ErrorMessage = "Merkez adı 200 karakteri geçemez.")]
        public string CentralName { get; set; }


        public int? HealthCenterTypeId { get; set; }
        public virtual HealthCenterType HealthCenterType { get; set; }


        public int? HealthStatusId { get; set; }
        public virtual HealthStatus HealthStatus { get; set; }


        public DateTime? EstablishmentDate { get; set; }


        public DateTime? RenovationDate { get; set; }


        public short? AreaM2 { get; set; }


        public short? BedCapacity { get; set; }


        public short? PersonCount { get; set; }


        public virtual ICollection<ValidLanguages> ValidLanguages { get; set; }


        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionTurkish { get; set; }


        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionEnglish { get; set; }


        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionArabic { get; set; }


        public virtual ICollection<Polyclinic> Polyclinics { get; set; }


        public virtual ICollection<ContractedInstitution> ContractedInstitutions { get; set; }


        public virtual ICollection<Doctor> Doctors { get; set; }


        public int? CountryId { get; set; }
        public virtual Country Country { get; set; }


        public int? CityId { get; set; }
        public virtual City City { get; set; }


        public int? TownId { get; set; }
        public virtual Town Town { get; set; }


        [MaxLength(300, ErrorMessage = "Adres 300 karakteri geçemez.")]
        public string Address { get; set; }



        [MaxLength(50, ErrorMessage = "Posta kodu 50 karakteri geçemez.")]
        public string ZipCode { get; set; }


        public short? AirportDistance { get; set; }


        public short? CityCenterDistance { get; set; }

        [MaxLength(50)]
        public string Latitude { get; set; }

        [MaxLength(50)]
        public string Longitude { get; set; }


        [MaxLength(500, ErrorMessage = "Logo adı 500 karakteri geçemez.")]
        public string Logo { get; set; }



        [MaxLength(500, ErrorMessage = "Kapak resmi adı 500 karakteri geçemez.")]
        public string CoverImage { get; set; }


        public virtual ICollection<PictureGallery> PictureGalleries { get; set; }


        public virtual ICollection<HealthAdvertisement> HealthAdvertisements { get; set; }

    }
}