﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class BranchHospital : BaseEntity
    {
        public BranchHospital()
        {
            Polyclinics = new HashSet<Polyclinic>();
        }


        public int Id { get; set; }


        [MaxLength(150, ErrorMessage = "Dal adı 150 karakteri geçemez.")]
        public string BranchName { get; set; }


        public virtual ICollection<Polyclinic> Polyclinics { get; set; }

    }
}