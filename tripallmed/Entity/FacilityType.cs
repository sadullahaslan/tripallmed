﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class FacilityType:BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Tesis tipi 50 karakteri geçemez.")]
        public string Type { get; set; }

    }
}