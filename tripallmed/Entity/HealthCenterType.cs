﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class HealthCenterType : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Merkez tipi 50 karakteri geçemez.")]
        public string Type { get; set; }
    }
}