﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class CompanyType : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(100, ErrorMessage = "Şirket Tip 100 karakteri geçemez.")]
        public string Type { get; set; }

    }
}