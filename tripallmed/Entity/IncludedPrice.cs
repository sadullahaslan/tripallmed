﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class IncludedPrice:BaseEntity
    {
        public IncludedPrice()
        {
            healthAdvertisements = new HashSet<HealthAdvertisement>();
        }


        public int Id { get; set; }


        public string Service { get; set; }

        public ICollection<HealthAdvertisement> healthAdvertisements { get; set; }
    }
}