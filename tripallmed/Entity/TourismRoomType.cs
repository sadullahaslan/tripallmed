﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismRoomType : BaseEntity
    {
        public int Id { get; set; }

        [MaxLength(30)]
        public string Type { get; set; }

        public int? TourismAccommodationTypeId { get; set; }
        public virtual TourismAccommodationType TourismAccommodationType { get; set; }
    }
}