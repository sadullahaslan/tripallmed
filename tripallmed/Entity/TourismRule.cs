﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismRule:BaseEntity
    {
        public TourismRule()
        {
            TourismAdvertisements = new HashSet<TourismAdvertisement>();
        }

        public int Id { get; set; }

        [MaxLength(30)]
        public string Name { get; set; }

        public virtual ICollection<TourismAdvertisement> TourismAdvertisements { get; set; }

    }
}