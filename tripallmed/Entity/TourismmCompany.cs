﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismmCompany : BaseEntity
    {
        public TourismmCompany()
        {
            TourismScopes = new HashSet<TourismScopes>();
            PictureGalleries = new HashSet<PictureGallery>();
            TourismAdvertisements = new HashSet<TourismAdvertisement>();
            ValidLanguages = new HashSet<ValidLanguages>();
            TourismRules = new HashSet<TourismRule>();
        }


        [ForeignKey("TourismUser")]
        public int Id { get; set; }
        public virtual TourismUser TourismUser { get; set; }

        public int? CompanyTypeId { get; set; }
        public virtual CompanyType CompanyType { get; set; }


        [MaxLength(150, ErrorMessage = "Şirket ünvanı 150 karakteri geçemez.")]
        public string Title { get; set; }


        [MaxLength(150, ErrorMessage = "Vergi dairesi 150 karakteri geçemez.")]
        public string TaxAdministration { get; set; }


        [MaxLength(100, ErrorMessage = "Vergi no 100 karakteri geçemez.")]
        public string TaxNo { get; set; }


        [MaxLength(200, ErrorMessage = "Tesis adı 200 karakteri geçemez.")]
        public string FacilityName { get; set; }

        //public int? PreferredLanguageId { get; set; }
        //public int? CurrencyUnitId { get; set; }

        public int? FacilityTypeId { get; set; }
        public virtual FacilityType FacilityType { get; set; }


        public int? TourismStatusId { get; set; }
        public virtual TourismStatus TourismStatus { get; set; }


        public DateTime? EstablishmentDate { get; set; }


        public DateTime? RenovationDate { get; set; }


        public short? AreaM2 { get; set; }


        public short? RoomCapacity { get; set; }


        public short? BedCapacity { get; set; }


        public int? TourismThemaId { get; set; }
        public virtual TourismThema TourismThema { get; set; }


        public virtual ICollection<TourismScopes> TourismScopes { get; set; }


        public virtual ICollection<TourismRule> TourismRules { get; set; }


        [MaxLength(10)]
        public string CheckInTime { get; set; }



        [MaxLength(10)]
        public string CheckOutTime { get; set; }


        public virtual ICollection<ValidLanguages> ValidLanguages { get; set; }


        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionTurkish { get; set; }


        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionEnglish { get; set; }


        [MaxLength(1500, ErrorMessage = "Açıklama 1500 karakteri geçemez.")]
        public string DescriptionArabic { get; set; }


        public int? CountryId { get; set; }
        public virtual Country Country { get; set; }


        public int? CityId { get; set; }
        public virtual City City { get; set; }


        public int? TownId { get; set; }
        public virtual Town Town { get; set; }


        [MaxLength(300, ErrorMessage = "Adres 300 karakteri geçemez.")]
        public string Address { get; set; }



        [MaxLength(50, ErrorMessage = "Posta kodu 50 karakteri geçemez.")]
        public string ZipCode { get; set; }


        public short? AirportDistance { get; set; }


        public short? CityCenterDistance { get; set; }

        [MaxLength(50)]
        public string Latitude { get; set; }

        [MaxLength(50)]
        public string Longitude { get; set; }


        [MaxLength(500, ErrorMessage = "Logo adı 500 karakteri geçemez.")]
        public string Logo { get; set; }



        [MaxLength(500, ErrorMessage = "Kapak resmi adı 500 karakteri geçemez.")]
        public string CoverImage { get; set; }


        public virtual ICollection<PictureGallery> PictureGalleries { get; set; }

        public virtual ICollection<TourismAdvertisement> TourismAdvertisements { get; set; }
    }
}