﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class PreferredLanguage : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Dil adı 50 karakteri geçemez.")]
        public string LanguageName { get; set; }

    }
}