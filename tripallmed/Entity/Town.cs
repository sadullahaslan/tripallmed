﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class Town : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "İlçe adı 50 karakteri geçemez.")]
        public string TownName { get; set; }


        public int? CityId { get; set; }
        public virtual City City { get; set; }

    }
}