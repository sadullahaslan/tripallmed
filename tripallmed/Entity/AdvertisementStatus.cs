﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class AdvertisementStatus : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(30, ErrorMessage = "İlan statüsü 30 karakteri geçemez")]
        public string Status { get; set; }
    }
}