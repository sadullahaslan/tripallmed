﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class CurrencyUnit : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Para birimi 50 karakteri geçemez.")]
        public string Unit { get; set; }
    }
}