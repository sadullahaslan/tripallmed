﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismPriceType : BaseEntity
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}