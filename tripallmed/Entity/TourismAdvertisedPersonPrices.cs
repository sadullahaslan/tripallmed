﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismAdvertisedPersonPrices : BaseEntity
    {
        public int Id { get; set; }

        public int? Key { get; set; }

        public decimal? Price { get; set; }


        public int? TourismAdvertisementId { get; set; }
        public virtual TourismAdvertisement TourismAdvertisement { get; set; }
    }
}