﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class ContractedInstitution : BaseEntity
    {
        public ContractedInstitution()
        {
            HealthcareCompanies = new HashSet<HealthcareCompany>();
        }

        public int Id { get; set; }


        [MaxLength(100, ErrorMessage = "Anlaşmalı kurum adı 100 karakteri geçemez.")]
        public string Institution { get; set; }


        public virtual ICollection<HealthcareCompany> HealthcareCompanies { get; set; }
    }
}