﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class PictureGallery : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(100, ErrorMessage = "İlçe adı 100 karakteri geçemez.")]
        public string Name { get; set; }
    }
}