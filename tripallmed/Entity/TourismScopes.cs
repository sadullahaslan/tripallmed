﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismScopes : BaseEntity
    {
        public TourismScopes()
        {
            TourismCompanies = new HashSet<TourismmCompany>();
        }


        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Olanak adı 50 karakteri geçemez.")]
        public string Scopes { get; set; }


        public virtual ICollection<TourismmCompany> TourismCompanies { get; set; }

    }
}