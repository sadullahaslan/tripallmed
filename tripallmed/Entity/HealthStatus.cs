﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    [Table("HealthStatuses")]
    public class HealthStatus : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Statü 50 karakteri geçemez.")]
        public string Status { get; set; }
    }
}