﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class DoctorTitle : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Doktor ünvanı 50 karakteri geçemez.")]
        public string Title { get; set; }
    }
}