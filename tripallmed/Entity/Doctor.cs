﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class Doctor : BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(50, ErrorMessage = "Doktor adı 50 karakteri geçemez.")]
        public string Name { get; set; }


        [MaxLength(50, ErrorMessage = "Doktor soyadı 50 karakteri geçemez.")]
        public string Surname { get; set; }


        public int? DoctorTitleId { get; set; }
        public virtual DoctorTitle DoctorTitle { get; set; }


        public int? PolyclinicId { get; set; }
        public virtual Polyclinic Polyclinic { get; set; }


        public int? HealthcareCompanyId { get; set; }
        public virtual HealthcareCompany HealthcareCompany { get; set; }
    }
}