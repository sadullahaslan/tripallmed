﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismMealType : BaseEntity
    {
        public TourismMealType()
        {
            TourismAdvertisements = new HashSet<TourismAdvertisement>();
        }
        public int Id { get; set; }

        [MaxLength(50)]
        public string Type { get; set; }

        public virtual ICollection<TourismAdvertisement> TourismAdvertisements { get; set; }
    }
}