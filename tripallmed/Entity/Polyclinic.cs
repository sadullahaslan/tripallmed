﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class Polyclinic : BaseEntity
    {
        public Polyclinic()
        {
            HealthcareCompanies = new HashSet<HealthcareCompany>();
            PolyclinicServices = new HashSet<PolyclinicService>();
        }


        public int Id { get; set; }


        [MaxLength(100, ErrorMessage = "Poliklinik adı 100 karakteri geçemez.")]
        public string PolyclinicName { get; set; }


        [MaxLength(100, ErrorMessage = "Resim adı adı 100 karakteri geçemez.")]
        public string PictureName { get; set; }




        public virtual ICollection<HealthcareCompany> HealthcareCompanies { get; set; }


        public int? BranchHospitalId { get; set; }
        public virtual BranchHospital BranchHospital { get; set; }


        public virtual ICollection<PolyclinicService> PolyclinicServices { get; set; }
    }
}