﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class HealthAdvertisement : BaseEntity
    {
        public HealthAdvertisement()
        {
            IncludedPrice = new HashSet<IncludedPrice>();
        }

        public int Id { get; set; }

        [MaxLength(100, ErrorMessage = "İlan başlığı 100 karakteri geçemez.")]
        public string AdvertTitle { get; set; }

        [MaxLength(100)]
        public string AdvertTitle_Eng { get; set; }


        [MaxLength(100)]
        public string AdvertTitle_Oth { get; set; }


        public int MedicalCenterId { get; set; }
        public virtual BranchHospital MedicalCenter { get; set; }


        public int MedicalUnitId { get; set; }
        public virtual Polyclinic MedicalUnit { get; set; }


        public int OperationId { get; set; }
        public virtual PolyclinicService Operation { get; set; }


        [MaxLength(300)]
        public string What { get; set; }


        [MaxLength(300)]
        public string What_Eng { get; set; }


        [MaxLength(300)]
        public string What_Oth { get; set; }


        [MaxLength(300)]
        public string Preparation { get; set; }


        [MaxLength(300)]
        public string Preparation_Eng { get; set; }


        [MaxLength(300)]
        public string Preparation_Oth { get; set; }


        [MaxLength(300)]
        public string Time { get; set; }


        [MaxLength(300)]
        public string Time_Eng { get; set; }


        [MaxLength(300)]
        public string Time_Oth { get; set; }


        [MaxLength(300)]
        public string Post { get; set; }


        [MaxLength(300)]
        public string Post_Eng { get; set; }


        [MaxLength(300)]
        public string Post_Oth { get; set; }


        [MaxLength(50)]
        public string CoverImage { get; set; }


        public virtual ICollection<IncludedPrice> IncludedPrice { get; set; }


        public short? OperationOption { get; set; }


        public decimal? ProcessingFee { get; set; }


        public int CurrencyUnitId { get; set; }
        public virtual CurrencyUnit CurrencyUnit { get; set; }


        public int CampaignDiscount { get; set; }


        public DateTime? CampaignStart { get; set; }


        public DateTime? CampaignEnd { get; set; }


        public short? Prepayment { get; set; }


        [MaxLength(20)]
        public string CancellationPolicy { get; set; }


        public int HealthcareCompanyId { get; set; }
        public virtual HealthcareCompany HealthcareCompany { get; set; }


        public int AdvertisementStatusId { get; set; }
        public virtual AdvertisementStatus AdvertisementStatus { get; set; }

        public short CompletionRate { get; set; }
    }
}