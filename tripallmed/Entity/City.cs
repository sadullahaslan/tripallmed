﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class City : BaseEntity
    {
        public City()
        {
            Towns = new HashSet<Town>();
        }


        public int Id { get; set; }



        [MaxLength(50, ErrorMessage = "Şehir adı 50 karakteri geçemez.")]
        public string CityName { get; set; }


        public int? CountryId { get; set; }
        public virtual Country Country { get; set; }


        public virtual ICollection<Town> Towns { get; set; }

    }
}