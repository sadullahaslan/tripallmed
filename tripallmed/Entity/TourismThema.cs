﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tripallmed.Entity
{
    public class TourismThema:BaseEntity
    {
        public int Id { get; set; }


        [MaxLength(100, ErrorMessage = "Tema ismi 100 karakteri geçemez.")]
        public string ThemeName { get; set; }
    }
}