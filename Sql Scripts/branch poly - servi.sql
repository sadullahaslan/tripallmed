USE [u6614476_TripAll]
GO
/****** Object:  Table [dbo].[BranchHospitals]    Script Date: 13.03.2020 10:51:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BranchHospitals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BranchName] [nvarchar](150) NULL,
	[IsDeleted] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.BranchHospitals] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Polyclinics]    Script Date: 13.03.2020 10:51:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Polyclinics](
	[Id] [int] NOT NULL,
	[PolyclinicName] [nvarchar](100) NULL,
	[BranchHospitalId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Polyclinics] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PolyclinicServices]    Script Date: 13.03.2020 10:51:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PolyclinicServices](
	[Id] [int] NOT NULL,
	[ServiceName] [nvarchar](100) NULL,
	[PolyclinicId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.PolyclinicServices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[BranchHospitals] ON 

INSERT [dbo].[BranchHospitals] ([Id], [BranchName], [IsDeleted], [Active]) VALUES (1, N'Poliklinik', 0, 1)
INSERT [dbo].[BranchHospitals] ([Id], [BranchName], [IsDeleted], [Active]) VALUES (2, N'Cerrahi Birimler', 0, 1)
INSERT [dbo].[BranchHospitals] ([Id], [BranchName], [IsDeleted], [Active]) VALUES (3, N'Dal Hizmet Birimler', 0, 1)
SET IDENTITY_INSERT [dbo].[BranchHospitals] OFF
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (1, N'Check-Up', 1, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (2, N'Alerji & İmmünoloji', 1, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (3, N'Algoloji (Ağrı Merkezi)', 1, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (4, N'Dahiliye (İç Hastalıkları)', 1, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (5, N'Dermotoloji', 1, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (6, N'Endokrinoloji', 1, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (7, N'Enfeksiyon & Mikrobiyoloji', 1, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (8, N'Gastroentereloji', 1, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (9, N'Genel cerrahi', 2, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (10, N'Kalp damar cerrahisi', 2, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (11, N'Beyin ve sinir cerrahisi', 2, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (12, N'Organ nakil merkezi', 2, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (13, N'Kadın Doğum', 2, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (15, N'Ağız ve Diş Sağlığı', 3, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (16, N'Göz Sağlığı', 3, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (17, N'Estetik & Güzellik', 3, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (18, N'Beslenme & Diyet', 3, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (20, N'Fizik Tedavi & Rehabilitasyon', 3, 0, 1)
INSERT [dbo].[Polyclinics] ([Id], [PolyclinicName], [BranchHospitalId], [IsDeleted], [Active]) VALUES (21, N'Diyaliz', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (1, N'19 -36 yaş', 1, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (2, N'36-40 yaş', 1, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (3, N'Genel Muayene', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (4, N'Akım Sitometrisi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (5, N'Bazofil Aktivasyon Testleri', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (6, N'Besin Yükleme Testi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (7, N'Bronş Provakasyon Testi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (8, N'Deri Testi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (9, N'Endoskopi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (10, N'Eozinofil Sayımı', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (11, N'Fiziksel Ürtiker testi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (12, N'İlaç Provakasyon Testi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (13, N'Nitrik Oksit Ölçümü', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (14, N'Otolog Serum Deri Testi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (15, N'Pletismografi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (16, N'Reverzibilite Testi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (17, N'Kantitatif Ig Düzeyleri', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (18, N'Serum Total IgE tayini', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (19, N'Alerjene Özgü IgE', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (20, N'Solunum Fonksiyon Testi', 2, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (21, N'Genel Muayene', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (22, N'Epidural ve Transforaminal enjeksiyon', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (23, N'Radyofrekans termokoagülasyon', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (24, N'Epidural lizis', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (25, N'Nörolitik bloklama', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (26, N'Sempatik bloklama', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (27, N'Morfin pompaları', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (28, N'Tetik nokta enjeksiyonu', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (29, N'Nükleoplasti', 3, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (30, N'Genel Muayene', 4, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (32, N'Gastroskopi', 4, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (33, N'Kolonoskopi', 4, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (34, N'ERCP ', 4, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (35, N'Karaciğer Testleri', 4, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (36, N'Anorektal manometri', 4, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (37, N'Genel Muayene', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (38, N'Deri Biyopsileri', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (39, N'Lezyon içi enjeksiyon', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (40, N'Kriyoterapi (dondurma)', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (41, N'Dermoskopi', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (42, N'Deri Patch testi', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (43, N'Paterji testi', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (44, N'Radyo frekans', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (45, N'Botoks', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (46, N'Kimyasal Peeling', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (47, N'Mezoterapi', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (48, N'V Lifting', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (49, N'Lazer Epilasyon', 5, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (50, N'Genel Muayene', 6, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (51, N'Büyüme faktör testleri', 6, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (52, N'Metabolizma testleri', 6, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (53, N'Mineral-Tuz Balansı', 6, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (54, N'Üreme Hormonları testi', 6, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (55, N'Kemik metabolizma testleri', 6, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (56, N'Androjen testleri', 6, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (57, N'Troid foksiyonları testi', 6, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (58, N'Down sendromu analizi', 6, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (59, N'Genel Muayene', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (60, N'Mikroskopi', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (61, N'Aerob Kültür', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (62, N'Anaerob kültür', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (63, N'Spesifik kültür', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (64, N'TBC kültür', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (65, N'Deri testi', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (66, N'Parazitoloji', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (67, N'Mikoloji', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (68, N'Konvansiyonel Seroloji', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (70, N'Moleküler PCR test', 7, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (71, N'Genel Muayene', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (72, N'Manometri', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (73, N'Anorektal manometri', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (74, N'Endeskopi', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (75, N'ERCP', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (76, N'Gastroskopi', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (77, N'Karaciğer testleri', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (79, N'Kolonoskopi', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (80, N'Ph-metri', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (81, N'Sigmoidoskopi', 8, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (82, N'Mide küçültme ameliyatı', 9, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (83, N'Laparoskopik kolon ameliyatı', 9, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (84, N'Robotik meme ameliyatı', 9, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (85, N'Sigmoidoskopi', 9, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (86, N'Spesmen grafisi', 9, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (87, N'Stereotaktik işaretleme', 9, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (88, N'Kalp biyopsisi', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (89, N'Kalp kateterizasyonu ve anjiografi (anjio)', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (90, N'Kalp pili', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (91, N'Kalp ve dolaşım sistemi sintigrafileri', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (92, N'Kardiyak MR', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (93, N'Karotid doppler testi', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (94, N'Kateter yolu ile aort kapak yerleştirilmesi (TAVI)', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (95, N'Koroner anjiyo CT (Çok kesitli BT anjiyografi/ Multislice kardiyak BT)', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (96, N'Koroner anjiyografi ve kalp kateterizasyonu', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (97, N'Koroner by-pass ameliyatları', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (98, N'Loop recorder', 10, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (99, N'Chiari malformasyonu', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (100, N'Elektromiyografi (EMG)', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (101, N'Fonksiyonel nöroşirürji ve ağrı cerrahisi', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (102, N'Gelişim anomalileri', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (103, N'Hidrosefali ameliyatları', 11, 0, 1)
GO
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (104, N'Kafa travmaları', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (105, N'Nöro-onkolojik cerrahi', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (106, N'Nörovasküler cerrahi', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (107, N'Omurga hastalıkları cerrahisi', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (108, N'Parkinson Hastalığında Beyin Pili Tedavisi', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (109, N'Periferik sinir hastalıkları cerrahisi', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (110, N'Tam endoskopik bel fıtığı ameliyatı', 11, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (111, N'Karaciğer nakli', 12, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (112, N'Böbrek nakli', 12, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (113, N'Kemik iliği nakli', 12, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (114, N'Pankreas nakli', 12, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (115, N'Kalp nakli', 12, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (116, N'Normal doğum', 13, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (117, N'Sezaryen doğum', 13, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (118, N'Epidural doğum', 13, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (119, N'Bleaching (Diş beyazlatma)', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (120, N'Çene eklemine artriosentez', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (121, N'Detertraj (Diş taşı temizliği)', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (122, N'Diş çekimi', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (123, N'Diş estetiği', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (124, N'Estetik diş hekimliği', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (125, N'Gömük 20 yaş dişleri', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (126, N'Gülüş tasarımı', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (127, N'İmplantoloji (Çene kemiğine vidalı diş uygulaması)', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (128, N'Kaybolan dişin yerine konan yapay apereyler', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (129, N'Kist operasyonları', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (130, N'Lingual ortodonti (dişlerin iç yüzeyinden)', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (132, N'Mukogingival operasyonlar', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (133, N'Retreatment (Kanal tedavisi söküm ve tekrarı)', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (134, N'Tam seramik restorasyonlar', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (135, N'Total ve parsiyel protezler', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (136, N'Yaprak porselenler (Laminate veneerler)', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (137, N'Zirkonyum porselen', 15, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (138, N'Aberrometre', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (139, N'Argon lazer uygulaması', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (140, N'Excimer lazer uygulaması', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (141, N'Göz anjiyografisi (Fundus floressein digital angiografi)', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (142, N'Göz tomografisi (Optik koherens tomografi / OCT)', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (143, N'Kornea topografisi', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (144, N'Optik sinir başı analizi (Optic nerve head analyser )', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (145, N'Pakimetri', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (146, N'Pneumatik tonometri', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (147, N'Renk Körlüğü Testi Nedir?', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (148, N'Sarı nokta kareli kağıt testi (Amsler grid)', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (149, N'Ultrasonik biyometri', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (150, N'Vitreoretinal cerrahi işlemler (Vitrektomi)', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (151, N'YAG lazer', 16, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (152, N'Burun estetiği', 17, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (153, N'Göğüs estetiği', 17, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (154, N'Boyun germe', 17, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (155, N'Saç ekimi', 17, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (156, N'Liposuction', 17, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (157, N'Botox', 17, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (158, N'Obezitede Tibbi Beslenme', 18, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (159, N'Diyabetes Mellitus’ta (Şeker Hastalığında) Tıbbi Beslenme', 18, 0, 1)
INSERT [dbo].[PolyclinicServices] ([Id], [ServiceName], [PolyclinicId], [IsDeleted], [Active]) VALUES (160, N'Karaciğer Hastalıklarında Tıbbi Beslenme', 18, 0, 1)
ALTER TABLE [dbo].[Polyclinics]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Polyclinics_dbo.BranchHospitals_BranchHospitalId] FOREIGN KEY([BranchHospitalId])
REFERENCES [dbo].[BranchHospitals] ([Id])
GO
ALTER TABLE [dbo].[Polyclinics] CHECK CONSTRAINT [FK_dbo.Polyclinics_dbo.BranchHospitals_BranchHospitalId]
GO
ALTER TABLE [dbo].[PolyclinicServices]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PolyclinicServices_dbo.Polyclinics_PolyclinicId] FOREIGN KEY([PolyclinicId])
REFERENCES [dbo].[Polyclinics] ([Id])
GO
ALTER TABLE [dbo].[PolyclinicServices] CHECK CONSTRAINT [FK_dbo.PolyclinicServices_dbo.Polyclinics_PolyclinicId]
GO
